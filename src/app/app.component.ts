import { Component, HostListener, ElementRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { User } from './user';
import { Subscription } from 'rxjs';
import { AuthenticationService } from './authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  /* animations: [
    navbarTransparent
  ] */
})

export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('musicF') musicF: ElementRef;

  private users: User[];
  currentUser: User;
  // state = 'transparent';
  navbarTransparent:boolean = true;
  route: string;
  isExpanded: boolean;
  random = 0;

  private fragment: string;
  private subscription: Subscription;

  ngOnInit() {
    this.activatedRoute.fragment.subscribe(fragment => { this.fragment = fragment; });

    this.authenticationService.initialize();

    this.router.events.subscribe((val) => {

      if (val instanceof NavigationEnd) {
        
        this.route = this.location.path();
        if (this.route !== '/mainPage' && this.route !== '') {
          this.navbarTransparent = false;
        }
        else {
          this.navbarTransparent = true;
        } 

        const tree = this.router.parseUrl(this.router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) { element.scrollIntoView(true); }
        }

        // used for google analytics
        (<any>window).ga('set', 'page', val.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });

    this.currentUser = this.authenticationService.currentUser;
    this.subscription = this.authenticationService.userChange.subscribe((value) => {
      this.currentUser = value;
    });

    this.isExpanded = false;
  }

  ngAfterViewInit(): void {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }

  constructor(
    public el: ElementRef,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private authenticationService: AuthenticationService) {
   }

  logout() {
    this.authenticationService.logout();
    this.authenticationService.backToMainPage();
  }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }

  @HostListener('window:scroll', ['$event'])
    checkScroll() {
      if (this.route === '/mainPage') {
        const componentPosition = 100;
        const scrollPosition = window.pageYOffset;

        if (scrollPosition >= componentPosition || this.isExpanded) {
          this.navbarTransparent = false;
        } else {
          this.navbarTransparent = true;
        }
      }
    }

  openMenu($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();  // <- that will stop propagation on lower layers
    this.isExpanded = !this.isExpanded;

    if (this.navbarTransparent) {
      this.navbarTransparent = false;
    }
  }

  stopPropagation($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();
  }

  @HostListener('document:click', ['$event']) clickedOutside($event){
    // here you can hide your menu
    this.isExpanded = false;
  }
}
