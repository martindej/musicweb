import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicFileComponent } from './music-file.component';

describe('MusicFileComponent', () => {
  let component: MusicFileComponent;
  let fixture: ComponentFixture<MusicFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
