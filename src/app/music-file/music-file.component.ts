import { Component, ElementRef, OnInit, Input, TemplateRef, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs';
// import { FacebookService, UIParams, UIResponse, InitParams } from 'ngx-facebook';
// import { ShareButtons } from '@ngx-share/buttons';

import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-music-file',
  templateUrl: './music-file.component.html',
  styleUrls: ['./music-file.component.css']
})
export class MusicFileComponent implements OnInit, OnDestroy {

  @Input() musicFile: MusicFile;
  @Input() topMusic: boolean;

  currentUser: User = new User(0, '', '', '', false, null, false, false, '', 0, false, true);
  private subscription: Subscription;

  currentMusicFilePlayer: MusicFile;
  currentMusicFilePlayerSubscription: Subscription;

  numberLikes: number;

  modalRef: BsModalRef;

  like = false;
  duration: number;
  playPercent: number;

  // sanitizedUrl;
  loaded: boolean;

  repoUrl = '';

  constructor(
    public el: ElementRef,
    private mainService: MainService,
    private musicService: MusicService,
    private sanitizer: DomSanitizer,
    private modalService: BsModalService,
    private authenticationService: AuthenticationService,
    /* public share: ShareButtons */) {

/*     const initParams: InitParams = {
      appId: '2084828878456913',
      xfbml: true,
      version: 'v2.8'
    };

    fb.init(initParams); */
  }

  ngOnInit() {

    this.currentMusicFilePlayerSubscription = this.mainService.currentMusicFile.subscribe(
      data => {
        this.currentMusicFilePlayer = data;
    });

    this.musicService.checkLike(this.musicFile.id).subscribe(
      res => {
        this.like = res;
      }
    );
    this.musicService.getNumberLikesByMusicId(this.musicFile.id).subscribe(
      res => {
        this.numberLikes = res;
      }
    );

    this.currentUser = this.authenticationService.currentUser;
    this.subscription = this.authenticationService.userChange.subscribe((value) => {
      this.currentUser = value;
    });

    this.repoUrl = 'https://murmure.tech/musicDetails/' + this.musicFile.id + '/infoScore';
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
    this.currentMusicFilePlayerSubscription.unsubscribe();
  }

  createBlob(source: string) {
    if (source === 'Xml') {
      this.musicService.loadXmlMusic(this.musicFile.id, true)
          .subscribe( res => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            this.downloadBlob(blob, this.musicFile.name + '.xml');
          });
    }
    else if (source === 'mXml') {
      this.musicService.loadXmlMusic(this.musicFile.id, false)
          .subscribe( res => {
            const blob = new Blob([res], { type: 'text/xml' });
            this.downloadBlob(blob, this.musicFile.name + '_modified.xml');
          });
    }
    else if (source === 'mp3') {
      this.musicService.loadMusic(this.musicFile.id)
          .subscribe( res => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            this.downloadBlob(blob, this.musicFile.name + '.mp3');
          });
    }
    else if (source === 'mid') {
      this.musicService.loadMidiMusic(this.musicFile.id)
          .subscribe( res => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            this.downloadBlob(blob, this.musicFile.name + '.mid');
          });
    }
    else {
      this.musicService.loadWavMusic(this.musicFile.id)
          .subscribe( res => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            this.downloadBlob(blob, this.musicFile.name + '.wav');
          });
    }

  }

  downloadBlob(blob, nameFile: string) {
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = nameFile;
      link.click();
  }

/*   shareOnFacebook() {

    const params: UIParams = {
      href: 'www.murmure.tech/musicDetails/' + this.musicFile.id + '/infoScore',
      method: 'share'
    };

    this.fb.ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));

  } */

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'customModal modal-sm' })
    );
  }

  decline(): void {
    this.modalRef.hide();
  }

  deleteMusic(): void {
    this.modalRef.hide();
    this.musicService.deleteMusic(this.musicFile).subscribe();
  }

  playAudio(): void {
    this.mainService.setFileMusic(this.musicFile);
  }

  likeMusic(): void {
    this.musicService.likeMusic(this.musicFile.id, this.musicFile.user_id).subscribe(
      res => {
        if (res === true) {
          this.like = true;
        }
      }
    );
  }

  dislikeMusic(): void {
    this.musicService.dislikeMusic(this.musicFile.id).subscribe(
      res => {
        if (res === true) {
          this.like = false;
        }
      }
    );
  }

  toggleVisible(): void {
    const visible = this.musicFile.visible_origin || this.musicFile.visible_modified || this.musicFile.visible_video;
    this.musicService.toggleVisible(this.musicFile.id, !visible).subscribe(
      res => {
        if (res === true) {
          this.musicFile.visible_origin = !visible;
          this.musicFile.visible_modified = !visible;
          this.musicFile.visible_video = !visible;
        }
      }
    );
  }

  take() {
    this.musicService.take(this.musicFile.id)
      .subscribe(
        data => {
          const music: MusicFile = new MusicFile(
            data.id,
            data.name,
            data.user_id,
            data.user_name,
            data.visible_origin,
            data.user_id_origin,
            data.user_name_origin,
            data.visible_modified,
            data.user_id_modified,
            data.user_name_modified,
            data.visible_video,
            data.user_id_video,
            data.user_name_video,
            'https://picsum.photos/640/640?image=' + data.pictureNumber,
            data.description,
            data.musicXmlFile,
            data.musicXmlFileModified,
            data.videoId,
            data.tempo,
            data.style,
            data.instruments,
            data.tonality,
            data.pictureNumber,
            data.creationDate);
          this.musicService.addMusicToList(music);
        });
  }
}
