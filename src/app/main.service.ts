import { Injectable } from '@angular/core';
import { of , BehaviorSubject ,  Observable } from 'rxjs';

import { MusicFile } from './MusicFile';
import { MidiInstrument } from './midiInstrument';
import { MIDIINSTRUMENTS } from './list-midiInstrument';

@Injectable()
export class MainService {

  private musicFile = new BehaviorSubject<MusicFile>(
    { id:0, 
      user_id:0, 
      user_name:"", 
      user_id_origin:0, 
      user_name_origin:"", 
      user_id_modified:0, 
      user_name_modified:"", 
      user_id_video:0, 
      user_name_video:"", 
      name :"",
      image:"https://picsum.photos/640/640/?random", 
      description:"",
      musicXml:"",
      musicXmlModified:"", 
      videoId:"", 
      tempo:0, 
      style:"", 
      instruments:"",
      tonality:"",
      visible_origin:false, 
      visible_modified:false,
      visible_video:false,
      picture_number:0, 
      creationDate:0
    });

  currentMusicFile = this.musicFile.asObservable();
  
  constructor() { }

  getInstrumentList(): Observable<MidiInstrument[]> {
    return of(MIDIINSTRUMENTS);
  }

  setFileMusic(musicFile:MusicFile) {
    this.musicFile.next(musicFile);
  }

}
