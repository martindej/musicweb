import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable, Subscription, Subject } from 'rxjs';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { AuthenticationService } from './authentication.service';
import { User } from './user';
import { StyleMusicFull } from './styleMusicFull';
import { StyleGroupInstrumentsFull } from './styleGroupInstruments';
import { StyleGroupDrumsFull } from './styleGroupDrums';
import { StyleGroupDrumsInsFull } from './styleGroupDrumsIns';
import { MesureTempsFort } from './mesureTempsFort';
import { StyleCoefDegres } from './styleCoefDegres';

@Injectable()
export class StyleService {

  private apiUrl = '/api/style';

  listStyles: StyleMusicFull[] = [];
  listStyleChange: Subject<StyleMusicFull[]> = new Subject<StyleMusicFull[]>();

  currentUser: User;
  private subscription: Subscription;

  constructor(
    private httpClient: HttpClient,
    private http: Http,
    private sanitizer: DomSanitizer,
    private authenticationService: AuthenticationService) {
    this.currentUser = authenticationService.currentUser;
    this.subscription = authenticationService.userChange.subscribe((value) => {
      this.currentUser = value;
    });
  }

  getSoundFont(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getListSoundFont');
  }

  getStylesMusic(): Observable<any> {
    const published: boolean = this.currentUser.superUser;
    return this.httpClient.get(this.apiUrl + '/getAll' + '/' + published);
  }

  deleteByName(name: string): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/delete' + '/' + name);
  }

  getStylesMusicLight(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getAllLight');
  }

  addNew(styleMusicFull: StyleMusicFull): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/addStyle', styleMusicFull, { headers });
  }

  addNewGroupInstruments(styleGroupInstruments: StyleGroupInstrumentsFull): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/addGroupInstruments', styleGroupInstruments, { headers });
  }

  addNewGroupDrums(styleGroupDrums: StyleGroupDrumsFull): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/addGroupDrums', styleGroupDrums, { headers });
  }

  addNewGroupDrumsIns(styleGroupDrumsIns: StyleGroupDrumsInsFull): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/addGroupDrumsIns', styleGroupDrumsIns, { headers });
  }

  addNewMesureTempsFort(mesureTempsFort: MesureTempsFort): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/addMesureTempsFort', mesureTempsFort, { headers });
  }

  addNewStyleCoefDegres(styleCoefDegres: StyleCoefDegres): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/addStyleCoefDegres', styleCoefDegres, { headers });
  }

  deleteGroupInstruments(group_id: number, style_id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteGroupInstruments/' + group_id + '/' + style_id);
  }

  deleteGroupDrums(group_drums_id: number, style_id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteGroupDrums/' + group_drums_id + '/' + style_id);
  }

  deleteGroupDrumsIns(group_drums_ins_id: number, style_id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteGroupDrumsIns/' + group_drums_ins_id + '/' + style_id);
  }

  deleteMesureTempsFort(group_id: number, style_id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteMesureTempsFort/' + group_id + '/' + style_id);
  }

  deleteStyleCoefDegres(degre_id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteStyleCoefDegres/' + degre_id);
  }

  setAllStyleGroupInstruments(data): StyleGroupInstrumentsFull[] {
    const styleGroupInstruments: StyleGroupInstrumentsFull[] = [];
    for (const s of data) {
      const group: StyleGroupInstrumentsFull = new StyleGroupInstrumentsFull(
        s.id,
        s.styleId,
        s.name,
        s.motherId,
        s.instruments,

        s.pourcentagePlayingMin,
        s.pourcentagePlayingMax,
        s.balanceBassMelodyMin,
        s.balanceBassMelodyMax,
        s.balanceMelodyMelodyBisMin,
        s.balanceMelodyMelodyBisMax,
        s.gBetweenNotesMin,
        s.gBetweenNotesMax,
        s.dispersionNotesMin,
        s.dispersionNotesMax,
        s.pourcentageSilMin,
        s.pourcentageSilMax,
        s.speedMax,
        s.speedMin,
        s.pSyncopesMin,
        s.pSyncopesMax,
        s.pSyncopettesMin,
        s.pSyncopettesMax,
        s.pSyncopesTempsFortsMin,
        s.pSyncopesTempsFortsMax,
        s.pContreTempsMin,
        s.pContreTempsMax,
        s.pDissonanceMin,
        s.pDissonanceMax,
        s.pIrregularitesMin,
        s.pIrregularitesMax,
        s.bBinaireTernaireMin,
        s.bBinaireTernaireMax,
        s.pChanceSupSameRythmMin,
        s.pChanceSupSameRythmMax,
        s.numberNotesSimultaneouslyMin,
        s.numberNotesSimultaneouslyMax,
        s.probabilityRythmMin,
        s.probabilityRythmMax,
        s.numberRythmMin,
        s.numberRythmMax,
        s.weightRythmMin,
        s.weightRythmMax,
        s.pChanceSameNoteInMotifMin,
        s.pChanceSameNoteInMotifMax,
        s.pChanceStartByTonaleAsBassMin,
        s.pChanceStartByTonaleAsBassMax,
        s.pChanceStayAroundNoteRefMin,
        s.pChanceStayAroundNoteRefMax,
        s.pChancePlayChordMin,
        s.pChancePlayChordMax
      );
      styleGroupInstruments.push(group);
    }

    return styleGroupInstruments;
  }

  setAllStyleGroupDrums(data): StyleGroupDrumsFull[] {
    const styleGroupDrums: StyleGroupDrumsFull[] = [];
    for (const s of data) {
      const group: StyleGroupDrumsFull = new StyleGroupDrumsFull(
        s.id,
        s.styleId,
        s.name,
        s.rhythm,
        s.pourcentagePlayingTempsFortMin,
        s.pourcentagePlayingTempsFortMax,
        s.pourcentagePlayingTempsFaibleMin,
        s.pourcentagePlayingTempsFaibleMax,
        s.pourcentagePlayingTemps1Min,
        s.pourcentagePlayingTemps1Max,
        s.pourcentagePlayingTemps2Min,
        s.pourcentagePlayingTemps2Max,
        s.pourcentagePlayingTemps3Min,
        s.pourcentagePlayingTemps3Max,
        s.pourcentagePlayingTemps4Min,
        s.pourcentagePlayingTemps4Max
      );

      if (s.drumsIns) {
        group.drumsIns = this.setAllStyleGroupDrumsIns(s.drumsIns);
      }

      styleGroupDrums.push(group);
    }

    return styleGroupDrums;
  }

  setAllStyleGroupDrumsIns(data): StyleGroupDrumsInsFull[] {
    const styleGroupDrumsIns: StyleGroupDrumsInsFull[] = [];
    for (const s of data) {
      const group: StyleGroupDrumsInsFull = new StyleGroupDrumsInsFull(
        s.id,
        s.styleId,
        s.groupDrumsId,
        s.instruments,
        s.name,
        s.rhythm,
        s.pourcentagePlayingTempsFortMin,
        s.pourcentagePlayingTempsFortMax,
        s.pourcentagePlayingTempsFaibleMin,
        s.pourcentagePlayingTempsFaibleMax,
        s.pourcentagePlayingTemps1Min,
        s.pourcentagePlayingTemps1Max,
        s.pourcentagePlayingTemps2Min,
        s.pourcentagePlayingTemps2Max,
        s.pourcentagePlayingTemps3Min,
        s.pourcentagePlayingTemps3Max,
        s.pourcentagePlayingTemps4Min,
        s.pourcentagePlayingTemps4Max
      );
      styleGroupDrumsIns.push(group);
    }

    return styleGroupDrumsIns;
  }

  setAllMesureTempsFort(data): MesureTempsFort[] {
    const mesureTempsForts: MesureTempsFort[] = [];
    for (const s of data) {
      const group: MesureTempsFort = new MesureTempsFort(
        s.id,
        s.styleId,
        s.mesure,
        s.tempsFort,
        s.etOu
      );
      mesureTempsForts.push(group);
    }

    return mesureTempsForts;
  }

  setAllStyleCoefDegres(data): StyleCoefDegres[] {
    const styleCoefDegres: StyleCoefDegres[] = [];
    for (const s of data) {
      const group: StyleCoefDegres = new StyleCoefDegres(
        s.id,
        s.c11,
        s.c12,
        s.c13,
        s.c14,
        s.c15,
        s.c16,
        s.c17,

        s.c21,
        s.c22,
        s.c23,
        s.c24,
        s.c25,
        s.c26,
        s.c27,

        s.c31,
        s.c32,
        s.c33,
        s.c34,
        s.c35,
        s.c36,
        s.c37,

        s.c41,
        s.c42,
        s.c43,
        s.c44,
        s.c45,
        s.c46,
        s.c47,

        s.c51,
        s.c52,
        s.c53,
        s.c54,
        s.c55,
        s.c56,
        s.c57,

        s.c61,
        s.c62,
        s.c63,
        s.c64,
        s.c65,
        s.c66,
        s.c67,

        s.c71,
        s.c72,
        s.c73,
        s.c74,
        s.c75,
        s.c76,
        s.c77,

        s.styleId,
        s.coefDegre
      );
      styleCoefDegres.push(group);
    }

    return styleCoefDegres;
  }

  setAllStyles(data) {
    this.listStyles = [];

    for (const s of data) {

      const music: StyleMusicFull = new StyleMusicFull(
        s.id,
        s.styleName,
        s.availableTon,
        s.rythmeMax,
        s.rythmeMin,
        s.nInstrumentMin,
        s.nInstrumentMax,
        s.nPercussionMin,
        s.nPercussionMax,
        s.complexityChordMin,
        s.complexityChordMax,
        s.numberChordMin,
        s.numberChordMax,
        s.soundFont,
        s.publish);

      if (s.styleCoefDegres) {
        music.styleCoefDegres = this.setAllStyleCoefDegres(s.styleCoefDegres);
      }
      if (s.mesureTempsFort) {
        music.mesureTempsForts = this.setAllMesureTempsFort(s.mesureTempsFort);
      }

      if (s.styleGroupIns) {
        music.styleGroupInstruments = this.setAllStyleGroupInstruments(s.styleGroupIns);
      }

      if (s.styleGroupDrums) {
        music.styleGroupDrums = this.setAllStyleGroupDrums(s.styleGroupDrums);
      }

      this.listStyles.push(music);
    }

    this.listStyleChange.next(this.listStyles);
  }

  pushFileToStorage(file: File, style_id: number): Observable<any> {

    const token = localStorage.getItem('auth_token');
    // let headers = new Headers({'Authorization': 'Bearer ' + token});
    const headers = new Headers();
    headers.append('Authorization', 'Bearer ' + token);
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const url = this.apiUrl + '/upload/' + this.currentUser.id + '/' + style_id;
    return this.http.post(url, formdata, { headers: headers }); // {headers}
  }

}
