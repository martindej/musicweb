import { Component, OnInit, ElementRef, ViewChild, Input, OnChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { User } from "../user";
import { AuthenticationService } from "../authentication.service"

@Component({
  selector: 'app-card-profile',
  templateUrl: './card-profile.component.html',
  styleUrls: ['./card-profile.component.css']
})
export class CardProfileComponent implements OnInit, OnChanges {

  @Input() publicUser:User;
  @Input() whiteBackground:boolean;
  // private publicUser: User = new User(0,"","","",false,null, false, false);
  // subscriptionUser:Subscription;
  private random:number;
  who:String;
  numberFollowers:number = 0;
  numberLikes:number = 0;
  numberCompositions:number = 0;

  constructor(
    private authenticationService: AuthenticationService, 
    private router:Router) {
    // this.publicUser = authenticationService.publicUser;
    // this.subscriptionUser = authenticationService.publicUserChange.subscribe((value) => { 
    //   this.publicUser = value; 
    // });
  }

  ngOnInit() {
    // this.getUserById(this.id);
  }

  ngOnChanges() {
    if(this.publicUser) {
      if(this.publicUser.player && this.publicUser.composer) {
        this.who = "Player & Composer";
      }
      else if (this.publicUser.player) {
        this.who = "Player";
      }
      else if (this.publicUser.composer) {
        this.who = "Composer";
      }
      else {
        this.who = "why do you care ? "
      }

      this.getNumberFollowers();
      this.getNumberLikes();
      this.getNumberCompositions();
    }
  }

  getNumberFollowers() {
    this.authenticationService.getNumberFollowers(this.publicUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberFollowers = data;
        }
    });
  }

  getNumberLikes() {
    this.authenticationService.getNumberLikes(this.publicUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberLikes = data;
        }
    });
  }

  getNumberCompositions() {
    this.authenticationService.getNumberCompositions(this.publicUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberCompositions = data;
        }
    });
  }

  goToProfile() {
    this.router.navigate(['/publicAccount/'+this.publicUser.id]);
  }

  // getUserById(id:number): void {
  //   this.authenticationService.getUserById(id).subscribe(
  //     res => {
  //       let data:any = res;

  //       if (data) {
  //         this.publicUser = data;
  //       }
  //     });
  // }

}