import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription, Subject } from 'rxjs';
import { User } from './user';


import { AuthenticationService } from './authentication.service';
import { SubscriptionUser } from './subscriptionUser';
import { Booster } from './booster';

@Injectable()
export class SubscriptionService {

  private apiUrl = '/api/subscription';

  currentUser: User;
  private subscription: Subscription;

  subUser: SubscriptionUser;
  subUserChange: Subject<SubscriptionUser> = new Subject<SubscriptionUser>();

  numberMusicsSubUser: number;
  numberMusicsSubUserChange: Subject<number> = new Subject<number>();

  numberMusicsBooster: number;
  numberMusicsBoosterChange: Subject<number> = new Subject<number>();

  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) {

    this.currentUser = authenticationService.currentUser;
      this.subscription = authenticationService.userChange.subscribe((value) => { 
        this.currentUser = value; 
      });
  }

  getAll(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getAll');
  }

  getAllBooster(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getAllBooster');
  }

  getSubscriptionUser(user_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getSubscriptionUserByUserId/'+user_id);
  }

  getAllSubscriptionUserByUserId(user_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getAllSubscriptionUserByUserId/'+user_id);
  }

  getSubscriptionOption(id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getSubscriptionOptionById/'+id);
  }

  getSubscriptionOptionByUserId(user_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getSubscriptionOptionIdByUserId/'+user_id);
  }

  getNumberMusicsSubscription(user_id:number): Observable<any>  {
    return this.httpClient.get(this.apiUrl + '/getNumberMusicsSubscription/'+user_id);
  }

  getNumberMusicsBooster(user_id:number): Observable<any>  {
    return this.httpClient.get(this.apiUrl + '/getNumberMusicsBooster/'+user_id);
  }

  getOrderId(): Observable<any>  {
    return this.httpClient.get(this.apiUrl + '/getOrderId');
  }

  getOrderIdIncremented(): Observable<any>  {
    return this.httpClient.get(this.apiUrl + '/getOrderIdIncremented');
  }

  setNumberMusicsSubscription(data:number) {
    this.numberMusicsSubUser = data;
    this.numberMusicsSubUserChange.next(this.numberMusicsSubUser);
  }

  setNumberMusicsBooster(data:number) {
    this.numberMusicsBooster = data;
    this.numberMusicsBoosterChange.next(this.numberMusicsBooster);
  }

  setSubUser(data:SubscriptionUser) {
    this.subUser = data;
    this.subUserChange.next(this.subUser);
  }

  incrementOrderId(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/incrementOrderId');
  }

  setSubscriptionAsAdmin(sub, user): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/setSubscriptionAsAdmin/'+ sub.id + '/' + user.id + '/' + this.currentUser.id);
  }

  deleteSubscriptionUserAsAdmin(subId): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteSubscriptionUserAsAdmin/'+ subId + '/' + this.currentUser.id);
  }

  updateMusicBoosterAsAdmin(numberMusic, user_id): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/updateMusicBoosterAsAdmin/' + numberMusic + '/' + user_id + '/' + this.currentUser.id );
  }
}
