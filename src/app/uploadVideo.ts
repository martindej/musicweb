export class UploadVideo {

  STATUS_POLLING_INTERVAL_MILLIS:number = 60 * 1000; // One minute.
  tags:Array<string>;
  categoryId:number;
  videoId:string;
  uploadStartTime:number;

  constructor(){
    this.tags = ['youtube-cors-upload'];
    this.categoryId = 22;
    this.videoId = '';

    this.uploadStartTime = 0;
  }

}