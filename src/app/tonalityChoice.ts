export class TonalityChoice {

    id: number;
    name: string;
    keyMajorMinor: string;
    description: string;
    image: string;

    constructor(id: number, name: string, keyMajorMinor: string, description: string, image: string){
      this.id = id;
      this.name = name;
      this.keyMajorMinor = keyMajorMinor;
      this.description = description;
      this.image = image;
    }
  }
