export class MidiInstrument {
  id: number;
  name: string;
  bassPart: boolean;
}
