import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { Parallax, ParallaxConfig } from 'ngx-parallax';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';
import { Autosize } from 'ng-autosize';
import { FacebookService } from 'ngx-facebook';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ShareModule } from '@ngx-share/core';

// import { StompService, StompState, StompConfig } from '@stomp/ng2-stompjs';
// import { Message } from '@stomp/stompjs';

import { AppComponent } from './app.component';
import { MainImageComponent } from './main-image/main-image.component';
import { CardComponent } from './card/card.component';
import { MusicFileComponent } from './music-file/music-file.component';
import { AppRoutingModule } from './/app-routing.module';
import { ComposeComponent } from './compose/compose.component';
import { MainPageComponent } from './main-page/main-page.component';
import { MainSettingsComponent } from './main-settings/main-settings.component';
import { MusicStyleComponent } from './music-style/music-style.component';
import { MyMusicComponent } from './my-music/my-music.component';
import { MyMusicListComponent } from './my-music-list/my-music-list.component';
import { SharedMusicsListComponent } from './shared-musics-list/shared-musics-list.component';
import { InstrumentsSettingsComponent } from './instruments-settings/instruments-settings.component';
import { SafePipe } from './safe.pipe';
import { MusicDetailsComponent } from './music-details/music-details.component';
import { MainService } from './main.service';
import { ServiceComponent } from './service/service.component';
import { ProfileComponent } from './profile/profile.component';
import { LimitToPipe } from './limit-to.pipe';
import { PlayerComponent } from './player/player.component';
import { AuthenticationService } from './authentication.service';
import { AuthguardService } from './authguard.service';
import { AlertService } from './alert.service';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './alert/alert.component';
import { RegisterComponent } from './register/register.component';
import { JwtInterceptor } from './jwt.interceptor';
import { UsernameValidator } from './validators/username';
import { EmailValidator } from './validators/email';
import { UsernameUpdateValidator } from './validators/usernameUpdate';
import { EmailUpdateValidator } from './validators/emailUpdate';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ContactComponent } from './contact/contact.component';
import { ContactService } from './contact.service';
import { ComposeService } from './compose.service';
import { MusicService } from './music.service';
import { CommentService } from './comment.service';
import { AccountComponent } from './account/account.component';
import { ProfileCardComponent } from './profile-card/profile-card.component';
import { AvatarDirective } from './avatar.directive';
import { AccountInformationsComponent } from './account-informations/account-informations.component';
import { AccountFormInformationsComponent } from './account-form-informations/account-form-informations.component';
import { AccountFormPasswordComponent } from './account-form-password/account-form-password.component';
import { PopoverModule, CollapseModule, BsDropdownModule, AlertModule, ModalModule, AccordionModule } from 'ngx-bootstrap';
import { MusicListFilterPipe } from './music-list-filter.pipe';
import { MainScoreComponent } from './main-score/main-score.component';
import { ModifiedScoreComponent } from './modified-score/modified-score.component';
import { MusicParametersComponent } from './music-parameters/music-parameters.component';
import { PublicAccountComponent } from './public-account/public-account.component';
import { CardProfileComponent } from './card-profile/card-profile.component';
import { MusicDetailsInfoComponent } from './music-details-info/music-details-info.component';
import { CommentComponent } from './comment/comment.component';
import { CardSubscriptionComponent } from './card-subscription/card-subscription.component';
import { SubscriptionService } from './subscription.service';
import { TonalityFilterPipe } from './tonality-filter.pipe';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { SortPipe } from './sort.pipe';
import { CardBoosterComponent } from './card-booster/card-booster.component';
import { AdministrationComponent } from './administration/administration.component';
import { AdminGuardService } from './admin-guard.service';
import { StyleFactoryComponent } from './style-factory/style-factory.component';
import { StyleService } from './style.service';
import { StyleGroupInstrumentsComponent } from './style-group-instruments/style-group-instruments.component';
import { RecoverComponent } from './recover/recover.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { YoutubeVideoService } from './youtube-video.service';
import { PaymentService } from './payment.service';
import { MesureTempsFortComponent } from './mesure-temps-fort/mesure-temps-fort.component';
import { StyleGroupDrumsComponent } from './style-group-drums/style-group-drums.component';
import { StyleGroupDrumsInsComponent } from './style-group-drums-ins/style-group-drums-ins.component';
import { ServicesCarouselComponent } from './services-carousel/services-carousel.component';
import { BubblesDirective } from './bubbles.directive';
import { CardTonalityComponent } from './card-tonality/card-tonality.component';
import { AnimateTitleDirective } from './animate-title.directive';
import { StyleCoefDegresComponent } from './style-coef-degres/style-coef-degres.component';
import { StyleAnalyseComponent } from './style-analyse/style-analyse.component';
import { ListUsersComponent } from './list-users/list-users.component';

// const stompConfig: StompConfig = {
//   // Which server?
//   // url: 'api/',
//   // url: () => {return new SockJS('api/socket')},
//   url: 'ws://localhost:8080/api/socket',

//   // Headers
//   // Typical keys: login, passcode, host
//   headers: {
//     login: 'guest',
//     passcode: 'guest'
//   },

//   // How often to heartbeat?
//   // Interval in milliseconds, set to 0 to disable
//   heartbeat_in: 0, // Typical value 0 - disabled
//   heartbeat_out: 20000, // Typical value 20000 - every 20 seconds
//   // Wait in milliseconds before attempting auto reconnect
//   // Set to 0 to disable
//   // Typical value 5000 (5 seconds)
//   reconnect_delay: 5000,

//   // Will log diagnostics on console
//   debug: true
// };

@NgModule({
  declarations: [
    AppComponent,
    MainImageComponent,
    CardComponent,
    MusicFileComponent,
    // Parallax,
    ComposeComponent,
    MainPageComponent,
    MainSettingsComponent,
    MusicStyleComponent,
    MyMusicComponent,
    MyMusicListComponent,
    SharedMusicsListComponent,
    InstrumentsSettingsComponent,
    SafePipe,
    MusicDetailsComponent,
    ServiceComponent,
    ProfileComponent,
    LimitToPipe,
    PlayerComponent,
    LoginComponent,
    AlertComponent,
    RegisterComponent,
    ConfirmationComponent,
    ContactComponent,
    AccountComponent,
    ProfileCardComponent,
    AvatarDirective,
    AccountInformationsComponent,
    AccountFormInformationsComponent,
    AccountFormPasswordComponent,
    MusicListFilterPipe,
    MainScoreComponent,
    ModifiedScoreComponent,
    MusicParametersComponent,
    PublicAccountComponent,
    CardProfileComponent,
    MusicDetailsInfoComponent,
    CommentComponent,
    Autosize,
    // CeiboShare,
    CardSubscriptionComponent,
    TonalityFilterPipe,
    SubscribeComponent,
    SortPipe,
    CardBoosterComponent,
    AdministrationComponent,
    StyleFactoryComponent,
    StyleGroupInstrumentsComponent,
    RecoverComponent,
    PrivacyComponent,
    TermsComponent,
    MesureTempsFortComponent,
    StyleGroupDrumsComponent,
    StyleGroupDrumsInsComponent,
    ServicesCarouselComponent,
    BubblesDirective,
    CardTonalityComponent,
    AnimateTitleDirective,
    StyleCoefDegresComponent,
    StyleAnalyseComponent,
    ListUsersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    // InlineSVGModule.forRoot({ baseUrl: '/musicApp/' }),
    InlineSVGModule.forRoot({ baseUrl: '/' }),
    PopoverModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    CarouselModule.forRoot(),
    NgxChartsModule,
    ShareModule
  ],
  providers: [
    MainService,
    AuthguardService,
    AdminGuardService,
    AlertService,
    MusicService,
    StyleService,
    CommentService,
    ComposeService,
    UsernameValidator,
    EmailValidator,
    UsernameUpdateValidator,
    EmailUpdateValidator,
    AuthenticationService,
    ContactService,
    YoutubeVideoService,
    FacebookService,
    PaymentService,
    // StompService,
    // {
    //   provide: StompConfig,
    //   useValue: stompConfig
    // },
    SubscriptionService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptor,
        multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
