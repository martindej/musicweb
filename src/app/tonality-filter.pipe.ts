import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tonalityFilter',
  pure: false 
})
export class TonalityFilterPipe implements PipeTransform {

  transform(value: any, args: any): any {

    if (!value || !args) {
      return value;
    }
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return value.filter(val => val.keyMajorMinor.indexOf(args) !== -1);
  }

}
