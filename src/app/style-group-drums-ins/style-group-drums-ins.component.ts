import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { StyleService } from '../style.service';
import { PERCUSSIONMIDIINSTRUMENTS } from '../list-midiPercussionInstruments';
import { AlertService} from '../alert.service';
import { MessageAlert } from '../messageAlert';
import { StyleGroupDrumsInsFull } from '../styleGroupDrumsIns';
import { RHYTHMS } from '../list-rhythm';

@Component({
  selector: 'app-style-group-drums-ins',
  templateUrl: './style-group-drums-ins.component.html',
  styleUrls: ['./style-group-drums-ins.component.css']
})
export class StyleGroupDrumsInsComponent implements OnInit {

  @Input() styleGroupDrumsIns: StyleGroupDrumsInsFull;

  listPercussionInstruments = PERCUSSIONMIDIINSTRUMENTS;
  listRhythm = RHYTHMS;

  submitAttempt: boolean;
  loading: boolean;

  random: number;
  loaded: boolean;

  styleGroupDrumsInsForm: FormGroup;

  constructor(
    private fBuilder: FormBuilder,
    private styleService: StyleService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {

    this.styleGroupDrumsInsForm = this.fBuilder.group({
      name: this.fBuilder.control(this.styleGroupDrumsIns.name, [Validators.required]),
      instruments: this.fBuilder.control(this.styleGroupDrumsIns.instruments, [Validators.required]),
      rhythm: this.fBuilder.control(this.styleGroupDrumsIns.rhythm, [Validators.required]),
      pourcentagePlayingTempsFortMin: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTempsFortMin, [Validators.required]),
      pourcentagePlayingTempsFortMax: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTempsFortMax, [Validators.required]),
      pourcentagePlayingTempsFaibleMin: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTempsFaibleMin, [Validators.required]),
      pourcentagePlayingTempsFaibleMax: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTempsFaibleMax, [Validators.required]),
      pourcentagePlayingTemps1Min: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps1Min, [Validators.required]),
      pourcentagePlayingTemps1Max: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps1Max, [Validators.required]),
      pourcentagePlayingTemps2Min: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps2Min, [Validators.required]),
      pourcentagePlayingTemps2Max: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps2Max, [Validators.required]),
      pourcentagePlayingTemps3Min: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps3Min, [Validators.required]),
      pourcentagePlayingTemps3Max: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps3Max, [Validators.required]),
      pourcentagePlayingTemps4Min: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps4Min, [Validators.required]),
      pourcentagePlayingTemps4Max: this.fBuilder.control(this.styleGroupDrumsIns.pourcentagePlayingTemps4Max, [Validators.required])

    });
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;

    if (!this.styleGroupDrumsInsForm.valid) {
        // this.signupSlider.slideTo(0);
    } else {
        const model: StyleGroupDrumsInsFull = this.styleGroupDrumsInsForm.value;
        model.id = this.styleGroupDrumsIns.id;
        model.styleId = this.styleGroupDrumsIns.styleId;
        model.groupDrumsId = this.styleGroupDrumsIns.groupDrumsId;
        // model.id = this.styleGroupIns.id;
        // User user = new User(0,model.username, model.password, )
        this.styleService.addNewGroupDrumsIns(model)
          .subscribe(
            data => {
              this.getStyles();
            },
            error => {
              const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

  delete() {
    this.styleService.deleteGroupDrumsIns(this.styleGroupDrumsIns.id, this.styleGroupDrumsIns.styleId)
      .subscribe(
        data => {
          this.getStyles();
        },
        error => {
          const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
          this.alertService.error(message);
        });
  }

  getStyles(): void {
    this.loading = true;
    this.styleService.getStylesMusic().subscribe(
      data => {
        this.styleService.setAllStyles(data);
        this.loading = false;
      }
    );
  }

}
