import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleGroupDrumsInsComponent } from './style-group-drums-ins.component';

describe('StyleGroupDrumsInsComponent', () => {
  let component: StyleGroupDrumsInsComponent;
  let fixture: ComponentFixture<StyleGroupDrumsInsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleGroupDrumsInsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleGroupDrumsInsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
