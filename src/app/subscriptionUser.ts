export class SubscriptionUser {

  id: number;
  subscriptionId: number;
  start_date: number;
  end_date: number;
  musicsPerDay: number;
  duration_days: number;
  advancedSettings: boolean;
  melodySettings: boolean;


  constructor(id: number, subscriptionId: number, musicsPerDay: number, start_date: number, end_date:number, duration_days: number, advancedSettings: boolean, melodySettings: boolean){
    this.id = id;
    this.subscriptionId = subscriptionId;
    this.musicsPerDay = musicsPerDay;
    this.start_date = start_date;
    this.end_date = end_date;
    this.duration_days = duration_days;
    this.advancedSettings = advancedSettings;
    this.melodySettings = melodySettings;
  }
}
