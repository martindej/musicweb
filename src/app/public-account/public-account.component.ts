import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AuthenticationService } from "../authentication.service"
import { User } from "../user";
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-public-account',
  templateUrl: './public-account.component.html',
  styleUrls: ['./public-account.component.css']
})
export class PublicAccountComponent implements OnInit {

  musicFiles:MusicFile[] = [];
  publicUser: User = new User(0,"","","",false,null, false, false,"",0,false, true);
  // subscriptionUser:Subscription;
  random:number;
  followed:boolean = false;

  listUsersFollowed: User[] = [];
  private subscriptionUsersFollowed:Subscription;

  numberFollowers:number = 0;
  numberLikes:number = 0;
  numberCompositions:number = 0;

  origin:string = "publicAccount";

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private authenticationService: AuthenticationService,
    private musicService: MusicService
  ) {}

  ngOnInit() {
    window.scroll(0, 0);
    const id = +this.route.snapshot.paramMap.get('id');
    this.getUserById(id);
    this.getMusics(id);
    this.following(id);

    if(this.publicUser && this.publicUser.id > 0) {
      this.getUsersFollowed();
    }
  }

  getNumberFollowers() {
    this.authenticationService.getNumberFollowers(this.publicUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberFollowers = data;
        }
    });
  }

  getNumberLikes() {
    this.authenticationService.getNumberLikes(this.publicUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberLikes = data;
        }
    });
  }

  getNumberCompositions() {
    this.authenticationService.getNumberCompositions(this.publicUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberCompositions = data;
        }
    });
  }

  getUsersFollowed() {
    this.subscriptionUsersFollowed = this.authenticationService.getListUsersFollowed(this.publicUser.id).subscribe((userList) => { 
      for(let m of userList) {
        let user:User = m;
        this.listUsersFollowed.push(m);
      }
    });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    if(this.subscriptionUsersFollowed) {
      this.subscriptionUsersFollowed.unsubscribe();
    }
  }

  ngOnChanges(changes) {
    this.getUsersFollowed();
  }

  toggleFollow() {
    this.authenticationService.toggleFollowUser(this.publicUser.id).subscribe(
      res => {
        let data:any = res;
        if(data === true) {
          this.followed = !this.followed;
        }
      });
  }

  following(id:number) {
    this.authenticationService.following(id).subscribe(
      res => {
        let data:any = res;
        if(data === true) {
          this.followed = true;
        }
        else {
          this.followed = false;
        }
      });
  }

  getUserById(id:number): void {
    this.authenticationService.getUserById(id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.publicUser = data;
          this.getUsersFollowed();
          this.getNumberFollowers();
          this.getNumberLikes();
          this.getNumberCompositions();
        }
      },
      error => {
        this.publicUser = new User(0,"a deleted user","","",false,null, false, false,"",0,false, true);
      });
  }

  getMusics(id:number): void {
    this.musicService.getPublicMusicsByUserId(id)
      .subscribe(
        data => {
          for(let m of data) {
            let music:MusicFile = new MusicFile(
              m.id, m.name, 
              m.user_id, 
              m.user_name, 
              m.visible_origin,
              m.user_id_origin,
              m.user_name_origin,
              m.visible_modified,
              m.user_id_modified,
              m.user_name_modified,
              m.visible_video,
              m.user_id_video,
              m.user_name_video, 
              "https://picsum.photos/640/640?image="+m.pictureNumber, 
              m.description,
              m.musicXmlFile,
              m.musicXmlFileModified,
              m.videoId, 
              m.tempo, 
              m.style, 
              m.instruments, 
              m.tonality, 
              m.pictureNumber,
              m.creationDate);
            this.musicFiles.push(music);
          }
        }
      );
  }

}
