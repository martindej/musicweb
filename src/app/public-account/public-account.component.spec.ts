import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicAccountComponent } from './public-account.component';

describe('PublicAccountComponent', () => {
  let component: PublicAccountComponent;
  let fixture: ComponentFixture<PublicAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
