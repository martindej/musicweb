import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { Parallax, ParallaxConfig } from 'ngx-parallax';

@Component({
  selector: 'app-main-image',
  templateUrl: './main-image.component.html',
  styleUrls: ['./main-image.component.css']
})
export class MainImageComponent implements OnInit {

  urlImage: string;

  constructor(private router: Router) { }

  ngOnInit() {
    const imgNumber = Math.floor(Math.random() * 4) + 1;
    this.urlImage = 'back' + imgNumber + '.jpg';
  }

  compose() {
     this.router.navigateByUrl('/myMusic/(myMusicsList//aux:compose)');
  }

}
