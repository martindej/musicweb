import { Card } from './card';

export const CARDS: Card[] = [
  { id: 1, title: 'You', image: '/assets/you.jpg', text: 'You are a musician or a music group looking for a new music to play, a composer in quest for inspiration or just someone curious.', resume: 'Everybody is welcome !' },
  { id: 2, title: 'Settings', image: '/assets/settings.jpg', text: 'Choose the music genre that you like, adapt the tempo and the swing, adapt if needed the musical instruments, tonality and drums', resume: 'There is always a genre for you !' },
  { id: 4, title: 'Compose', image: '/assets/compose.jpg', text: 'Hit the compose button and let the machine work for you.', resume: 'That can be long...' },
  { id: 5, title: 'Get your score', image: '/assets/score.jpg', text: 'You \'ll get the score in .xml or .midi format and the music as .wav or .mp3. You can display or play it from the website or download it.' , resume: 'You now have a new unique music !' },
  { id: 6, title: 'Modify your score', image: '/assets/modified.jpg', text: 'The music can be interesting but not exactly what you were expecting, so if you modify it on your favourite music editor software, you can then upload your new version.' , resume: 'Share your creation.' },
  { id: 7, title: 'Upload a video', image: '/assets/youtube.jpg', text: 'You are proud of the result and you want to share it ? why don\'t you make a video of your interpretation ?', resume: 'We are waiting for your best creations !' }
];
