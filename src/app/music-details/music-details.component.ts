import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';
import { YoutubeVideoService } from '../youtube-video.service';

@Component({
  selector: 'app-music-details',
  templateUrl: './music-details.component.html',
  styleUrls: ['./music-details.component.css']
})
export class MusicDetailsComponent implements OnInit, OnDestroy {

  @ViewChild('score') score: ElementRef;
  // musicFile:MusicFile;
  url: string;
  file: File;
  dataXml: string;
  openNav = false;

  musicDetails: MusicFile;
  private subscriptionMusic: Subscription;

  constructor(
    private route: ActivatedRoute,
    private musicService: MusicService,
    private youtubeVideoService: YoutubeVideoService ) {
    this.musicDetails = musicService.musicDetails;
    this.subscriptionMusic = musicService.musicDetailsChange.subscribe((value) => {
      this.musicDetails = value;
      // this.displayMainScore(this.musicDetails.musicXml);
    });

    this.youtubeVideoService.getToken().subscribe();
  }

  ngOnInit() {
    window.scroll(0, 0);
    const id = +this.route.snapshot.paramMap.get('id');
    this.getMusic(id);
  }

  ngOnDestroy() {
    this.subscriptionMusic.unsubscribe();
  }

  getMusic(id: number): void {
    this.musicService.getMusic(id).subscribe(
      data => {
        const music: MusicFile = new MusicFile(
          data.id,
          data.name,
          data.user_id,
          data.user_name,
          data.visible_origin,
          data.user_id_origin,
          data.user_name_origin,
          data.visible_modified,
          data.user_id_modified,
          data.user_name_modified,
          data.visible_video,
          data.user_id_video,
          data.user_name_video,
          'https://picsum.photos/640/640?image=' + data.pictureNumber,
          data.description,
          data.musicXmlFile,
          data.musicXmlFileModified,
          data.videoId,
          data.tempo,
          data.style,
          data.instruments,
          data.tonality,
          data.pictureNumber,
          data.creationDate);
        this.musicService.setMusicDetail(music);
      }
    );
  }
}
