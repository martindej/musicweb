import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay';

import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-main-score',
  templateUrl: './main-score.component.html',
  styleUrls: ['./main-score.component.css']
})
export class MainScoreComponent implements OnInit, OnDestroy {

  @ViewChild('score') score: ElementRef;

  musicDetails: MusicFile;
  private subscriptionMusic: Subscription;
  fileUrl;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private musicService: MusicService
  ) {}

  ngOnInit() {
    window.scroll(0, 0);
    this.musicDetails = this.musicService.musicDetails;
    if (this.musicDetails && this.musicDetails.id) {
      this.displayMainScore(this.musicDetails.musicXml);
      this.createBlob();
    }
    this.subscriptionMusic = this.musicService.musicDetailsChange.subscribe((value) => {
      this.musicDetails = value;
      this.displayMainScore(this.musicDetails.musicXml);
      this.createBlob();
    });
  }

  ngOnDestroy() {
    this.subscriptionMusic.unsubscribe();
  }

  createBlob() {
    const blob = new Blob([this.musicDetails.musicXml], { type: 'text/xml' }),
    url = window.URL;
    this.fileUrl = url.createObjectURL(blob);
  }

  displayMainScore(scoreLink: string): void {
    if (this.score.nativeElement.firstChild != null) {
      this.score.nativeElement.removeChild(this.score.nativeElement.firstChild);
    }

    //   context.clearRect(0, 0, canvas.width, canvas.height);
    const osmd = new OpenSheetMusicDisplay(this.score.nativeElement);
      osmd.load(scoreLink)
      .then(
        () => {
          osmd.render();
          // osmd.cursor.show();
        },
        (err) => console.log(err)
      );
  }
}
