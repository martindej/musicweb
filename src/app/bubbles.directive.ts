import { Directive, ElementRef, HostListener, Input, OnInit, Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[appBubbles]'
})
export class BubblesDirective implements OnInit {

  // @Input() defaultColor: string;
  // @Input('appHighlight') highlightColor: string;

  interval;
  bArray = [];
  sArray = [10, 12, 14, 16];

  constructor(private elementRef: ElementRef, private renderer: Renderer2, @Inject(DOCUMENT) private document) {}

  ngOnInit() {

    for (let i = 0; i < this.elementRef.nativeElement.clientWidth; i++) {
      this.bArray.push(i);
    }

  }

  createBubble() {
    // Get a random size, defined as variable so it can be used for both width and height
    const size = this.randomValue(this.sArray);
    const noteNumber = Math.floor(Math.random() * 7) + 1;

    // this.dataContainer.nativeElement.innerHTML = svg;

    const child = document.createElement('div');
    this.renderer.addClass(child, 'individual-bubble');
    this.renderer.addClass(child, 'individual-bubble-' + noteNumber);
    this.renderer.setStyle(child, 'left', this.randomValue(this.bArray) + 'px');
    this.renderer.setStyle(child, 'width', size + 'px');
    this.renderer.setStyle(child, 'height', 3 * size + 'px');

    this.renderer.appendChild(this.elementRef.nativeElement, child);

    setInterval(
      () => child.remove()
      , 3000);
  }

  randomValue(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.interval = setInterval(
      () => this.createBubble()
      , 350);
  }

  @HostListener('mouseleave') onMouseLeave() {
    clearInterval(this.interval);
  }
}
