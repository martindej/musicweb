import { Component, OnInit, OnChanges, ViewChild, ElementRef, OnDestroy, TemplateRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { User } from "../user";
import { AuthenticationService } from '../authentication.service';
import { SubscriptionService } from '../subscription.service';
import { AvatarDirective } from '../avatar.directive';
import { SubscriptionOption } from '../subscriptionOption';
import { SubscriptionUser } from '../subscriptionUser';
import { Booster } from '../booster';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, OnChanges, OnDestroy {

  currentUser: User = new User(0,"","","",false,null, false, false,"",0,false, true);
  private subscription:Subscription;

  listUsersFollowed: User[] = [];
  private subscriptionUsersFollowed:Subscription;

  // listSubscriptionOption: SubscriptionOption[] = [];
  // private subscriptionListSubscriptionOption: Subscription;

  subUser: SubscriptionUser;
  private subscriptionSubUser: Subscription;

  subOption: Subscription;
  private subscriptionSubOption: Subscription;

  numberMusicsSubOption: number;
  private subscriptionNumberMusicsSubOption: Subscription;

  // booster: Booster;
  // private subscriptionBooster: Subscription;
  modalRef: BsModalRef;
  
  numberMusicsBooster: number;
  private subscriptionNumberMusicsBooster: Subscription;

  random:number;
  userInfos:String = "info";
  loaded:boolean;

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };

  numberFollowers:number = 0;
  numberLikes:number = 0;
  numberCompositions:number = 0;

  origin:string = "account";

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private modalService: BsModalService,
    private subscriptionService: SubscriptionService) {
  }

  getNumberFollowers() {
    this.authenticationService.getNumberFollowers(this.currentUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberFollowers = data;
        }
    });
  }

  getNumberLikes() {
    this.authenticationService.getNumberLikes(this.currentUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberLikes = data;
        }
    });
  }

  getNumberCompositions() {
    this.authenticationService.getNumberCompositions(this.currentUser.id).subscribe(
      res => {
        let data:any = res;

        if (data) {
          this.numberCompositions = data;
        }
    });
  }

  modifyInfos() {
    this.userInfos = "form";
  }

  changePassword() {
    this.userInfos = "password";
  }

  deleteAccount() {
    this.authenticationService.deleteUser(this.currentUser.id).subscribe(
      data => { 
        if(data === true) {
          this.authenticationService.logout();
          this.authenticationService.backToMainPage();
        }
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'customModal modal-sm' })
    );
  }

  decline(): void {
    this.modalRef.hide();
  }

  selectFile(event) {
    const file = event.target.files.item(0)
 
    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
      this.upload();
    } else {
      alert('invalid format!');
    }
  }

  upload() {
    this.progress.percentage = 0;
 
    this.currentFileUpload = this.selectedFiles.item(0)
    this.authenticationService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!');
      }
      this.random = Math.random();
    })
 
    this.selectedFiles = undefined
  }

  getUsersFollowed() {
    this.subscriptionUsersFollowed = this.authenticationService.getListUsersFollowed(this.currentUser.id).subscribe((userList) => { 
      for(let m of userList) {
        let user:User = m;
        this.listUsersFollowed.push(m);
      }
    });
  }

  // getSubscriptionsOption() {
  //   this.subscriptionListSubscriptionOption = this.subscriptionService.getAll().subscribe(
  //     (subscriptionOptionList) => { 
  //       for(let m of subscriptionOptionList) {
  //         let sub:SubscriptionOption = m;
  //         this.listSubscriptionOption.push(sub);
  //       }
  //     });
  // }

  getSubUser() {
    this.subscriptionSubUser = this.subscriptionService.getSubscriptionUser(this.currentUser.id).subscribe(
      sub => { 
        this.subUser = sub;
        this.getNumberMusicsSubscription(this.subUser.id);
        this.getSubOption(this.subUser.subscriptionId);
      });
  }

  getSubOption(id) {
    this.subscriptionSubOption = this.subscriptionService.getSubscriptionOption(id).subscribe(
      sub => { 
        this.subOption = sub;
      });
  }

  getNumberMusicsSubscription(sub_id:number) {
    this.subscriptionNumberMusicsSubOption = this.subscriptionService.getNumberMusicsSubscription(this.currentUser.id).subscribe(
      sub => { 
        let data:any = sub;
        if(typeof data === "number") {
          this.numberMusicsSubOption = data;
        }
        
      });
  }

  // getBooster() {
  //   this.subscriptionBooster = this.subscriptionService.getBooster(this.currentUser.id).subscribe(
  //     boo => { 
  //       this.booster = boo;
  //     });
  // }

  getNumberMusicsBooster() {
    this.subscriptionNumberMusicsBooster = this.subscriptionService.getNumberMusicsBooster(this.currentUser.id).subscribe(
      boo => { 
        let data:any = boo;
        if(typeof data === "number") {
          this.numberMusicsBooster = data;
        }
      });
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
    if(this.subscriptionUsersFollowed) {
      this.subscriptionUsersFollowed.unsubscribe();
    }
    // if(this.subscriptionListSubscriptionOption) {
    //   this.subscriptionListSubscriptionOption.unsubscribe();
    // }

    this.subscriptionSubUser.unsubscribe();
    this.subscriptionNumberMusicsSubOption.unsubscribe();
    // this.subscriptionBooster.unsubscribe();
    this.subscriptionNumberMusicsBooster.unsubscribe();
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.currentUser = this.authenticationService.currentUser;
    this.subscription = this.authenticationService.userChange.subscribe((value) => { 
      this.currentUser = value; 
    });

    if(this.currentUser && this.currentUser.id > 0) {
      this.getUsersFollowed();
    }

    this.getNumberFollowers();
    this.getNumberLikes();
    this.getNumberCompositions();

    // this.getBooster();
    this.getNumberMusicsBooster();
    this.getSubUser();
  }

  ngOnChanges(changes) {
    this.getUsersFollowed();
    this.getNumberFollowers();
    this.getNumberLikes();
    this.getNumberCompositions();

    // this.getBooster();
    this.getNumberMusicsBooster();
    this.getSubUser();
  }
}
