import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicParametersComponent } from './music-parameters.component';

describe('MusicParametersComponent', () => {
  let component: MusicParametersComponent;
  let fixture: ComponentFixture<MusicParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
