import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { User } from "../user";
import { AuthenticationService } from '../authentication.service';
import { AlertService} from '../alert.service';
import { PasswordValidator } from '../validators/password';
import { MessageAlert } from '../messageAlert';


@Component({
  selector: 'app-account-form-password',
  templateUrl: './account-form-password.component.html',
  styleUrls: ['./account-form-password.component.css']
})
export class AccountFormPasswordComponent implements OnInit {

  @Input() getUserFormInfos: String;
  @Output() getUserFormInfosChange = new EventEmitter<String>();
  currentUser: User;
  private subscription:Subscription;
  userForm: FormGroup;

  private random:number;
  private userFormInfosDisabled:boolean = true;

  loading = false;
  submitAttempt:boolean = false;
  newPassword = 'newPassword';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private fBuilder:FormBuilder) {

      this.currentUser = authenticationService.currentUser;
      this.subscription = authenticationService.userChange.subscribe((value) => { 
        this.currentUser = value; 
      });

      this.userForm = fBuilder.group({
        password: fBuilder.control(null, Validators.required),
        newPassword: fBuilder.control(null, Validators.required),
        repeatNewPassword: fBuilder.control(null, [Validators.required, PasswordValidator(this.newPassword)])
      });
  }

  ngOnInit() {
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;
 
    if(!this.userForm.valid){
        // this.signupSlider.slideTo(0);
    }
    else {
        // let model: User = new User(this.currentUser.id,this.userForm.value.username,this.currentUser.password, this.userForm.value.email, false, this.instrumentsToString(), this.userForm.value.player, this.userForm.value.composer);
        // User user = new User(0,model.username, model.password, )
        this.authenticationService.changePassword(this.currentUser.id, this.userForm.value.password, this.userForm.value.newPassword)
          .subscribe(
            data => {
              let message:MessageAlert = new MessageAlert("password modified successfully", 5000, true, 'success');
              this.alertService.success(message);
              this.getUserFormInfosChange.emit("info");
            },
            error => {
              let message:MessageAlert = new MessageAlert("error, please try again later", 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

}
