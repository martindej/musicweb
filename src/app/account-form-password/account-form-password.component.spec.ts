import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountFormPasswordComponent } from './account-form-password.component';

describe('AccountFormPasswordComponent', () => {
  let component: AccountFormPasswordComponent;
  let fixture: ComponentFixture<AccountFormPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountFormPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFormPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
