import { StyleGroupDrumsInsFull } from './styleGroupDrumsIns';

export class StyleGroupDrumsFull {

    id: number;
    styleId: number;
    drumsIns: StyleGroupDrumsInsFull[];
    name: string;
    rhythm: number;

    pourcentagePlayingTempsFortMin: number;
    pourcentagePlayingTempsFortMax: number;
    pourcentagePlayingTempsFaibleMin: number;
    pourcentagePlayingTempsFaibleMax: number;
    pourcentagePlayingTemps1Min: number;
    pourcentagePlayingTemps1Max: number;
    pourcentagePlayingTemps2Min: number;
    pourcentagePlayingTemps2Max: number;
    pourcentagePlayingTemps3Min: number;
    pourcentagePlayingTemps3Max: number;
    pourcentagePlayingTemps4Min: number;
    pourcentagePlayingTemps4Max: number;

    constructor(id: number,
            styleId: number,
            name: string,
            rhythm: number,
            pourcentagePlayingTempsFortMin: number,
            pourcentagePlayingTempsFortMax: number,
            pourcentagePlayingTempsFaibleMin: number,
            pourcentagePlayingTempsFaibleMax: number,
            pourcentagePlayingTemps1Min: number,
            pourcentagePlayingTemps1Max: number,
            pourcentagePlayingTemps2Min: number,
            pourcentagePlayingTemps2Max: number,
            pourcentagePlayingTemps3Min: number,
            pourcentagePlayingTemps3Max: number,
            pourcentagePlayingTemps4Min: number,
            pourcentagePlayingTemps4Max: number) {
        this.id = id;
        this.styleId = styleId;
        this.name = name;
        this.drumsIns = [];
        this.rhythm = rhythm;
        this.pourcentagePlayingTempsFortMin = pourcentagePlayingTempsFortMin;
        this.pourcentagePlayingTempsFortMax = pourcentagePlayingTempsFortMax;
        this.pourcentagePlayingTempsFaibleMin = pourcentagePlayingTempsFaibleMin;
        this.pourcentagePlayingTempsFaibleMax = pourcentagePlayingTempsFaibleMax;
        this.pourcentagePlayingTemps1Min = pourcentagePlayingTemps1Min;
        this.pourcentagePlayingTemps1Max = pourcentagePlayingTemps1Max;
        this.pourcentagePlayingTemps2Min = pourcentagePlayingTemps2Min;
        this.pourcentagePlayingTemps2Max = pourcentagePlayingTemps2Max;
        this.pourcentagePlayingTemps3Min = pourcentagePlayingTemps3Min;
        this.pourcentagePlayingTemps3Max = pourcentagePlayingTemps3Max;
        this.pourcentagePlayingTemps4Min = pourcentagePlayingTemps4Min;
        this.pourcentagePlayingTemps4Max = pourcentagePlayingTemps4Max;
      }
   }
