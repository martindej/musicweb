import { Component, OnInit, Input, HostListener, ElementRef} from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { ProfileCard } from '../profileCard';
import { slideIn } from '../animation';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.css']
})
export class ProfileCardComponent implements OnInit {

  @Input() card: ProfileCard;
  visibility:string = 'visibleRed';

  constructor(public el: ElementRef) { }

  ngOnInit() {
  }

  changeStyle($event){
    this.visibility = $event.type == 'mouseover' ? 'visibleWhite' : 'visibleRed';
  }

}