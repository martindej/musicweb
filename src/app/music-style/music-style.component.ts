import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { StyleMusicFull } from '../styleMusicFull';
import { ComposeService } from '../compose.service';

@Component({
  selector: 'app-music-style',
  templateUrl: './music-style.component.html',
  styleUrls: ['./music-style.component.css']
})
export class MusicStyleComponent implements OnInit, OnDestroy {

  @Input() musicStyle: StyleMusicFull;

  private subscriptionStyle;
  style: number;
  random: number;

  constructor(private composeService: ComposeService) { }

  ngOnInit() {
    this.style = this.composeService.currentStyle;
    this.subscriptionStyle = this.composeService.style.subscribe(
      style => {
        this.style = style;
    });
  }

  ngOnDestroy() {
    this.subscriptionStyle.unsubscribe();
  }

}
