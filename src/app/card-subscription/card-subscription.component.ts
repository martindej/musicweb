import { Component, OnInit, Input,OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { User } from "../user";
import { AuthenticationService } from "../authentication.service";
import * as sha1 from 'js-sha1';

import { SubscriptionOption } from '../subscriptionOption';
import { SubscriptionService } from '../subscription.service';

@Component({
	selector: 'app-card-subscription',
	templateUrl: './card-subscription.component.html',
	styleUrls: ['./card-subscription.component.css']
})
export class CardSubscriptionComponent implements OnInit,OnDestroy {

	@Input() subscriptionOption: SubscriptionOption;
	@Input() orderId:string;
	@Input() selected:boolean;
	@Input() account:boolean;

	certificate:string = "mhHeU5pTE66HYe2y";

	subscriptionForm: FormGroup;
	model = {
  		vads_action_mode: "INTERACTIVE",
  		vads_amount:"",
  		vads_ctx_mode:"PRODUCTION",
  		vads_currency:"978",
  		vads_page_action:"PAYMENT",
  		vads_payment_config:"SINGLE",
  		vads_site_id:"30388450",
  		vads_trans_date:"",
  		vads_trans_id:"",
  		vads_version:"V2",
  		vads_cust_email:"",
  		vads_cust_id:"",
  		vads_order_id:"",
  		vads_language:"en",
  		vads_product_amountN:"",
  		signature:""
  	};

	currentUser: User = new User(0,"","","",false,null, false, false,"",0,false, true);
	private subscription:Subscription;

	constructor(
		private fBuilder: FormBuilder, 
		private subscriptionService: SubscriptionService,
		private authenticationService: AuthenticationService) { 

		this.currentUser = this.authenticationService.currentUser;
		this.subscription = this.authenticationService.userChange.subscribe((value) => { 
			this.currentUser = value; 
			this.setUserDetails();
		});
	}

	ngOnInit() {

		let date = new Date();
		let datestring = date.getUTCFullYear() + ("0"+(date.getUTCMonth()+1)).slice(-2) + ("0" + date.getUTCDate()).slice(-2)
				+ ("0" + date.getUTCHours()).slice(-2) + ("0" + date.getUTCMinutes()).slice(-2) + ("0" + date.getUTCSeconds()).slice(-2);
		let price = this.subscriptionOption.pricePerYear*100;

		this.model.vads_amount = price.toString();
		this.model.vads_trans_date = datestring;
		this.model.vads_cust_id = this.currentUser.id.toString();
		this.model.vads_trans_id = this.orderId;
		this.model.vads_order_id = this.subscriptionOption.id.toString();
		this.model.vads_product_amountN = this.subscriptionOption.durationInDays.toString();

		this.setUserDetails();
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	setUserDetails() {
		if(this.currentUser.id !== 0) {
			this.model.vads_cust_email = this.currentUser.email;
			this.model.vads_cust_id = this.currentUser.id.toString();
		}
		this.setSignature();
	}

	setSignature() {
		this.model.signature = this.generateSignature();
	}

	generateSignature() {

		let signature = 
			this.model.vads_action_mode + "+"+
			this.model.vads_amount + "+"+
			this.model.vads_ctx_mode + "+"+
			this.model.vads_currency + "+"+ 
			this.model.vads_cust_email + "+"+ 
			this.model.vads_cust_id + "+"+ 
			this.model.vads_language + "+"+ 
			this.model.vads_order_id + "+"+ 
			this.model.vads_page_action + "+"+ 
			this.model.vads_payment_config + "+"+ 
			this.model.vads_product_amountN + "+"+ 
			this.model.vads_site_id + "+"+ 
			this.model.vads_trans_date + "+"+ 
			this.model.vads_trans_id + "+"+ 
			this.model.vads_version + "+"+ 
			this.certificate;

		return sha1(signature);
	}
}
