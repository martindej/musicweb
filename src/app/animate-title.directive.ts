import { Directive, ElementRef, Renderer2, Inject, Input, OnInit, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[appAnimateTitle]'
})
export class AnimateTitleDirective implements OnInit {

  @Input('appAnimateTitle') text: string;

  triggered = false;

  constructor(private el: ElementRef, private renderer: Renderer2, @Inject(DOCUMENT) private document) {
  //   // use a promise to output text layers into DOM first
  //   let outputLayers = new Promise(function (resolve, reject) {
  //     document.getElementById('text').innerHTML = createLetterContainers(createLetterLayers(createLetterArray(text))).join('');
  //     resolve();
  //   });

  // // then adjust width and height of each letter
  //   let spans = Array.prototype.slice.call(document.getElementsByTagName('span'));
  //   outputLayers.then(function () {
  //     return spans.map(function (span) {
  //       setTimeout(function () {
  //         span.parentElement.style.width = span.offsetWidth + 'px';
  //         span.parentElement.style.height = span.offsetHeight + 'px';
  //       }, 250);
  //     });
  //   }).then(function () {
  //     // then slide letters into view one at a time
  //     var time = 250;
  //     return spans.map(function (span) {
  //       time += 75;
  //       setTimeout(function () {
  //         span.parentElement.style.top = '0px';
  //       }, time);
  //     });
  //   });

  }

  ngOnInit(): void {
    this.createLetters();
    this.ajustSize();
    // this.animate();
  }

  @HostListener('window:scroll', ['$event'])
    checkScroll() {

      // const scrollPosition = window.pageYOffset;
      const pageHeight = window.innerHeight;
      const componentPosition = this.el.nativeElement.getBoundingClientRect().top;

      if ((componentPosition > 100 ) && (componentPosition < (pageHeight - 100))) {
        if (!this.triggered) {
          this.animate();
          this.triggered = true;
        }
      }
      else {
        this.triggered = false;
      }
    }

  ajustSize() {

    const childrens = this.el.nativeElement.children;

    for (let i = 0; i < childrens.length; i++) {

      const children = this.el.nativeElement.children[i];
      const span = this.el.nativeElement.children[i].children[0];

      //ajust size
      const ran = Math.floor(Math.random() * 9);
      this.renderer.setStyle(children, 'width', span.offsetWidth + 'px');
      this.renderer.setStyle(children, 'height', span.offsetHeight + 'px');
      this.renderer.addClass(children, 'deg' + ran);
    }
  }

  animate() {
    const childrens = this.el.nativeElement.children;
    let time = 250;

    for (let i = 0; i < childrens.length; i++) {

      const children = this.el.nativeElement.children[i];
      // const span = this.el.nativeElement.children[i].children[0];

      //ajust size
      // this.renderer.setStyle(children, 'width', span.offsetWidth + 'px');
      // this.renderer.setStyle(children, 'height', span.offsetHeight + 'px');

      setTimeout(
        () => {
          this.renderer.setStyle(children, 'top', '-50px');
      }, time);

      setTimeout(
        () => {
          this.renderer.setStyle(children, 'top', '0px');
      }, time + 500);

      time += 75;
    }
  }

  createLetters() {

    // loop for each letter
    this.text.split('').map(
      letter => {

        const child = document.createElement('div');
        this.renderer.addClass(child, 'wrapper');
        //specify # of layers per letter
        for (let i = 1; i <= 2; i++) {

          const span = document.createElement('span');

          // if letter is a space
          if (letter === ' ') {
            this.renderer.addClass(span, 'space');
          } else {
            const text = this.renderer.createText(letter);
            this.renderer.addClass(span, 'letter-' + i);
            this.renderer.appendChild(span, text);
          }

          this.renderer.appendChild(child, span);
        }

        this.renderer.appendChild(this.el.nativeElement, child);
    });
  }

}
