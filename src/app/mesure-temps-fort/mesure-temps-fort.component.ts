import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { TEMPSFORTS } from '../listTempsForts';
import { DURATIONCHORDS } from '../listDurationChord';
import { StyleService } from '../style.service';
import { AlertService} from '../alert.service';
import { MesureTempsFort } from '../mesureTempsFort';
import { MessageAlert } from '../messageAlert';

@Component({
  selector: 'app-mesure-temps-fort',
  templateUrl: './mesure-temps-fort.component.html',
  styleUrls: ['./mesure-temps-fort.component.css']
})
export class MesureTempsFortComponent implements OnInit {

  @Input() mesureTempsFort: MesureTempsFort;

  listDurationChord = DURATIONCHORDS;
  listTempsForts = TEMPSFORTS;

  loading: Boolean = false;

  styleMesureTempsFortForm: FormGroup;

  constructor(
    private fBuilder: FormBuilder,
    private styleService: StyleService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.styleMesureTempsFortForm = this.fBuilder.group({
      mesure: this.fBuilder.control(this.mesureTempsFort.mesure, [Validators.required]),
      tempsFort: this.fBuilder.control(this.mesureTempsFort.tempsFort, [Validators.required]),
      etOu: this.fBuilder.control(this.mesureTempsFort.etOu, [Validators.required])
    });
  }

  save() {
    this.loading = true;

    if (!this.styleMesureTempsFortForm.valid) {
        // this.signupSlider.slideTo(0);
    } else {
        const model: MesureTempsFort = this.styleMesureTempsFortForm.value;
        model.id = this.mesureTempsFort.id;
        model.styleId = this.mesureTempsFort.styleId;

        this.styleService.addNewMesureTempsFort(model)
          .subscribe(
            data => {
              this.getStyles();
            },
            error => {
              const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

  delete() {
    this.styleService.deleteMesureTempsFort(this.mesureTempsFort.id, this.mesureTempsFort.styleId)
      .subscribe(
        data => {
          this.getStyles();
        },
        error => {
          const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
          this.alertService.error(message);
        });
  }

  getStyles(): void {
    this.loading = true;
    this.styleService.getStylesMusic().subscribe(
      data => {
        this.styleService.setAllStyles(data);
        this.loading = false;
      }
    );
  }

}
