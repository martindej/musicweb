import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesureTempsFortComponent } from './mesure-temps-fort.component';

describe('MesureTempsFortComponent', () => {
  let component: MesureTempsFortComponent;
  let fixture: ComponentFixture<MesureTempsFortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesureTempsFortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesureTempsFortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
