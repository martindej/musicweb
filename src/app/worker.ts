export class Worker {
    id: number;
    name: string;
    job: string;
    text: string;
    textResume: string;
    image: string;
    linkedInLink: string;
  }
