import { Injectable } from '@angular/core';
import { Observable, Subject ,  BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { MusicParameters } from './musicParameters';
import { MidiInstrument } from './midiInstrument';
import { MIDIINSTRUMENTS } from './list-midiInstrument';

@Injectable()
export class ComposeService {

  private apiUrl = '/api/music';

  currentStyle: number;
  style: Subject<number> = new Subject<number>();

  private listInstrumentsInternal: Array<MidiInstrument> = [];
  private listInstruments = new BehaviorSubject<Array<MidiInstrument>>([]);
  currentListInstruments = this.listInstruments.asObservable();

  currentListInstrumentsId: Array<number>;
  listInstrumentsId = new BehaviorSubject<Array<number>>([]);

  private musicParameters = new BehaviorSubject<MusicParameters>(new MusicParameters(100, 40, 150, 50, 0, 100, '', 'A major', false));
  currentMusicParameters = this.musicParameters.asObservable();

  private listIdImagesInternal: Array<number> = [];
  private listIdImages = new BehaviorSubject<number[]>([]);
  currentListIdImages = this.listIdImages.asObservable();

  randomRefreshCompose: Subject<number> = new Subject<number>();

  currentIdImage: number;
  idImage: Subject<number> = new Subject<number>();

  constructor(private httpClient: HttpClient) { }

  setCurrentStyle(style: number) {
    this.currentStyle = style;
    this.style.next(style);
  }

  newRandomRefreshCompose() {
    this.randomRefreshCompose.next(Math.random());
  }

  addToCurrentListInstruments(instrument: MidiInstrument) {
    this.listInstrumentsInternal.push(instrument);
    this.listInstruments.next(this.listInstrumentsInternal);

    this.currentListInstrumentsId.push(instrument.id);
    this.listInstrumentsId.next(this.currentListInstrumentsId);
  }

  removeFromCurrentListInstruments(instrument: MidiInstrument) {

    let index = this.listInstrumentsInternal.indexOf(instrument, 0);

    if (index > -1) {
      this.listInstrumentsInternal.splice(index, 1);
    }
    this.listInstruments.next(this.listInstrumentsInternal);

    index = this.currentListInstrumentsId.indexOf(instrument.id, 0);

    if (index > -1) {
      this.currentListInstrumentsId.splice(index, 1);
    }
    this.listInstrumentsId.next(this.currentListInstrumentsId);
  }

  setcurrentListInstruments(listInstruments: Array<MidiInstrument>) {
    this.listInstrumentsInternal = listInstruments;
    this.listInstruments.next(this.listInstrumentsInternal);
  }

  setCurrentListIdImages(listId: Array<number>) {
    this.listIdImagesInternal = listId;
    this.listIdImages.next(this.listIdImagesInternal);

    if (!this.currentIdImage) {
      this.setRandomIdImage();
    }
  }

  setTonality(tonality) {
    const mParameters = this.musicParameters.value;
    mParameters.setTonality(tonality);
    this.musicParameters.next(mParameters);
  }

  setPercussion(percussion) {
    const mParameters = this.musicParameters.value;
    mParameters.setPercussion(percussion);
    this.musicParameters.next(mParameters);
  }

  getInstrumentById(id: number) {
    for (const entry of MIDIINSTRUMENTS) {
      if (entry.id === id) {
        return entry;
      }
    }
  }

  getMusicParameters(style: number): Observable<any> {

    let styleFinal;
    if (style === 0 || style === undefined) {
      styleFinal = this.currentStyle;
    }
    else {
      styleFinal = style;
    }

    return this.httpClient.get(this.apiUrl + '/getFirstParametersMusic/' + styleFinal);
  }

  setMusicParameters(data: any) {
    // this.tempo.next(data.tempo);
    // this.musicTitle.next(data.musicTitle);

    this.musicParameters.next(new MusicParameters(data.tempo, data.tempoMin, data.tempoMax, data.swing, data.swingMin, data.swingMax, data.musicTitle, data.tonality, data.percussion));

    this.setRandomIdImage();

    this.currentListInstrumentsId = data.listInstruments;
    this.listInstrumentsId.next(this.currentListInstrumentsId);

    const listInst: Array<MidiInstrument> = [];
    for (const inst of data.listInstruments) {
      listInst.push(this.getInstrumentById(inst));
    }

    this.setcurrentListInstruments(listInst);
  }

  getListIdImages(): Observable<any> {
    return this.httpClient.get('https://picsum.photos/list');
  }

  setListIdImages(data) {

    const listIdIm: Array<number> = [];
    for (const image of data) {
      listIdIm.push(image.id);
    }

    this.setCurrentListIdImages(listIdIm);
  }

  setRandomIdImage() {
    if (this.listIdImagesInternal.length > 0) {
      const rand = Math.floor(Math.random() * this.listIdImagesInternal.length);
      this.currentIdImage = this.listIdImagesInternal[rand];
      this.idImage.next(this.listIdImagesInternal[rand]);
    }
  }

}
