
import {empty as observableEmpty,  Observable, Subscription, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { User } from './user';
import { Http, Headers } from '@angular/http';



import { AuthenticationService } from './authentication.service';
import { MusicFile } from './MusicFile';
import { MusicDetails } from './musicDetails';
import { ComposeService } from './compose.service';
import { MusicParameters } from './musicParameters';

@Injectable()
export class MusicService {

  listMusic: MusicFile[] = [];
  // styleFilter:string[] = [];

  filter:{name: string, like: boolean, visibility:boolean,  style:string[],instruments:string[]} = {"name": "", "like":false, "visibility":false, "style":[],"instruments":[]};
  currentUser: User;
  private subscription: Subscription;
  private apiUrl = '/api/music';
  musicDetails: MusicFile;

  listMusicChange: Subject<MusicFile[]> = new Subject<MusicFile[]>();
  filterChange:Subject<{name: string, like: boolean, visibility:boolean, style:string[],instruments:string[]}> = new Subject<{"name": "", "like":null, "visibility":null, style:string[],instruments:string[]}>();
  musicDetailsChange: Subject<MusicFile> = new Subject<MusicFile>();

  musicParameters: MusicParameters;
  private subscriptionMusicParameters: Subscription;

  listSelectedInstruments: Array<number> = [];
  private subscriptionListInstruments;

  idImage:number;
  private subscriptionIdImage;
  
  constructor(
    private httpClient: HttpClient, 
    private http: Http, 
    private authenticationService: AuthenticationService,
    private composeService: ComposeService) {

      this.currentUser = authenticationService.currentUser;
      this.subscription = authenticationService.userChange.subscribe((value) => { 
        this.currentUser = value; 
      });

      this.subscriptionMusicParameters = this.composeService.currentMusicParameters.subscribe(
        musicParameters => {
          this.musicParameters = musicParameters;
      });

      this.subscriptionListInstruments = this.composeService.listInstrumentsId.subscribe(
        listSelectedInstruments => {
          this.listSelectedInstruments = listSelectedInstruments;
      });

      this.idImage = this.composeService.currentIdImage;
      this.subscriptionIdImage = this.composeService.idImage.subscribe(
        idImage => {
          this.idImage = idImage;
      });
    }

  getStylesMusic(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getStyles');
  }

  getNumberLikesByMusicId(music_id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getNumberLikesByMusicId/' + music_id );
  }

  updateModifiedScore(file: File, musicId: number): Observable<any> {

    const token = localStorage.getItem('auth_token');
    const headers = new Headers();
    headers.append('Authorization', 'Bearer ' + token);
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const url = this.apiUrl + '/uploadModifiedScore/' + musicId + '/' + this.currentUser.id;
    return this.http.post(url, formdata, {headers: headers}); // {headers}
  }

  setMusicVisibilityOrigin(visible:boolean) {
    this.musicDetails.visible_origin = visible;
    this.musicDetailsChange.next(this.musicDetails);
  }

  setMusicVisibilityModified(visible:boolean) {
    this.musicDetails.visible_modified = visible;
    this.musicDetailsChange.next(this.musicDetails);
  }

  setMusicVisibilityVideo(visible:boolean) {
    this.musicDetails.visible_video = visible;
    this.musicDetailsChange.next(this.musicDetails);
  }

  setVideoIdLocaly(videoId:string) {
    this.musicDetails.videoId = videoId;
    this.musicDetailsChange.next(this.musicDetails);
  }

  toggleVisibilityFilter(visibility:boolean) {
    this.filter.visibility = visibility;
    this.filterChange.next(this.filter);
  }

  toggleLikeFilter(like:boolean) {
    this.filter.like = like;
    this.filterChange.next(this.filter);
  }

  toggleNameFilter(name:string) {
    this.filter.name = name;
    this.filterChange.next(this.filter);
  }

  toggleStyleFilter(style:string) {

    if(style.indexOf("AllStyles") > -1) {
      this.filter.style = [];
    }
    else if(this.filter.style.indexOf(style.toLowerCase()) > -1) {
      this.filter.style.splice(this.filter.style.indexOf(style.toLowerCase()),1);
    } 
    else {
      this.filter.style.push(style.toLowerCase());
    }
    
    this.filterChange.next(this.filter);
  }

  toggleInstrumentsFilter(instrument:string) {

    if(instrument.indexOf("AllInstruments") > -1) {
      this.filter.instruments = [];
    }
    else if(this.filter.instruments.indexOf(instrument.toLowerCase()) > -1) {
      this.filter.instruments.splice(this.filter.instruments.indexOf(instrument.toLowerCase()),1);
    } 
    else {
      this.filter.instruments.push(instrument.toLowerCase());
    }
    
    this.filterChange.next(this.filter);
  }

  changeMusicDetails(musicDetails:MusicDetails):Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/changeMusicDetails', musicDetails, {headers});
  }

  generateMusic(style: number): Observable<any> {

    const musicParameters = {
      tempo: this.musicParameters.tempo,
      swing: this.musicParameters.swing,
      style: style,
      listInstruments: this.listSelectedInstruments,
      musicTitle: this.musicParameters.musicTitle,
      idImage: this.idImage,
      tonality: this.musicParameters.tonality,
      percussions: this.musicParameters.percussion
    };
    // const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const req = new HttpRequest('POST', this.apiUrl + '/compose/' + this.currentUser.id, musicParameters, {
      reportProgress: true
    });

    return this.httpClient.request(req);
  }

  getAllAnalyseParameters(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getAllAnalyseParameters/' + this.currentUser.id);
  }

  analyseStyle(style: number, parameter: string): Observable<any> {

    const musicParameters = {
      style: style
    };
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/analyse/' + this.currentUser.id + '/' + parameter, musicParameters, {headers});
  }

  setVideoId(musicId, videoId): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/setVideoId/' + musicId + '/' + videoId + '/' + this.currentUser.id);
  }

  take(music_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/take/' + this.currentUser.id +'/'+this.currentUser.username+'/'+music_id);
  }

  loadMusic(id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getMp3/'+id, { responseType: 'blob' });
  }

  loadWavMusic(id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getWav/'+id, { responseType: 'blob' });
  }

  loadTestMusic(randomTopMusic): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getDemoMp3/' + randomTopMusic, { responseType: 'blob' });
  }

  loadTestSheetMusic(randomTopMusic): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getDemoXml/' + randomTopMusic, { responseType: 'blob' });
  }

  loadXmlMusic(id: number, original:boolean): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getXml/'+id +'/'+original, { responseType: 'blob' });
  }

  loadMidiMusic(id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getMidi/'+id, { responseType: 'blob' });
  }

  getMusic(id: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getMusicById/'+id);
  }

  setMusicDetail(music:MusicFile) {
    this.musicDetails = music;
    this.musicDetailsChange.next(this.musicDetails);
  }

  getTopMusics(): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getTopMusics');
  }

  getPublicMusicsByUserId(id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getPublicMusicsByUserId/'+ id);
  }

  getPublicMusics(page:number): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/getPublicMusics/'+ page, this.filter, {headers});
  }

  getAllMusics(page:number): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/getAllMusics/'+ this.currentUser.id +'/'+ page, this.filter, {headers});
  }

  setAllMusics(data) {
    this.listMusic = [];
    for(let m of data) {

      let music:MusicFile = new MusicFile(
        m.id, m.name,
        m.user_id,
        m.user_name,
        m.visible_origin,
        m.user_id_origin,
        m.user_name_origin,
        m.visible_modified,
        m.user_id_modified,
        m.user_name_modified,
        m.visible_video,
        m.user_id_video,
        m.user_name_video,
        "https://picsum.photos/640/640?image=" + m.pictureNumber,
        m.description,
        m.musicXmlFile,
        m.musicXmlFileModified,
        m.videoId,
        m.tempo,
        m.style,
        m.instruments,
        m.tonality,
        m.pictureNumber,
        m.creationDate);
      this.listMusic.push(music);
    }
    this.listMusicChange.next(this.listMusic);
  }

  addAllMusics(data) {
    for(const m of data) {

      const music:MusicFile = new MusicFile(
        m.id, m.name,
        m.user_id,
        m.user_name,
        m.visible_origin,
        m.user_id_origin,
        m.user_name_origin,
        m.visible_modified,
        m.user_id_modified,
        m.user_name_modified,
        m.visible_video,
        m.user_id_video,
        m.user_name_video,
        'https://picsum.photos/640/640?image=' + m.pictureNumber,
        m.description,
        m.musicXmlFile,
        m.musicXmlFileModified,
        m.videoId,
        m.tempo,
        m.style,
        m.instruments,
        m.tonality,
        m.pictureNumber,
        m.creationDate);
      this.listMusic.push(music);
    }
    this.listMusicChange.next(this.listMusic);
  }

  deleteMusic(music:MusicFile): Observable<any> {
    return this.httpClient
      .get(this.apiUrl + '/deleteMusic/'+ music.id + '/' + this.currentUser.id).pipe(
      map( res => {
        const data: any = res;
        const val: number = parseInt(data);

        if (val > 0) {
          const index = this.listMusic.indexOf(music, 0);
          if (index > -1) {
            this.listMusic.splice(index, 1);
          }
          this.listMusicChange.next(this.listMusic);
        }

        return res;
      },
      err => {
        return err;
      }));
  }

  checkLike(idMusic: number): Observable<any> {

    const like = this.currentUser && this.currentUser.id
    ? this.checkLikeWithUserId(idMusic)     // !! gives boolean true or false
    : observableEmpty();

    return like;
  }

  checkLikeWithUserId(idMusic: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/hasLike/' + idMusic + '/' + this.currentUser.id);
  }

  likeMusic(idMusic: number, idOwner: number) {
    return this.httpClient.get(this.apiUrl + '/like/' + idMusic + '/' + this.currentUser.id + '/' + idOwner);
  }

  dislikeMusic(idMusic: number) {
    return this.httpClient.get(this.apiUrl + '/dislike/' + idMusic + '/' + this.currentUser.id);
  }

  toggleVisible(idMusic: number, visible: boolean) {
    return this.httpClient.get(this.apiUrl + '/toggleVisible/' + idMusic + '/' + visible + '/' + this.currentUser.id);
  }

  toggleVisibleOrigin(idMusic: number, visible: boolean) {
    return this.httpClient.get(this.apiUrl + '/toggleVisibleOrigin/' + idMusic + '/' + visible + '/' + this.currentUser.id);
  }

  toggleVisibleModified(idMusic: number, visible: boolean) {
    return this.httpClient.get(this.apiUrl + '/toggleVisibleModified/' + idMusic + '/' + visible + '/' + this.currentUser.id);
  }

  toggleVisibleVideo(idMusic: number, visible: boolean) {
    return this.httpClient.get(this.apiUrl + '/toggleVisibleVideo/' + idMusic + '/' + visible + '/' + this.currentUser.id);
  }

  addMusicToList(music: MusicFile) {
    if (this.getMusicByIdInList(music.id) == null) {
      this.listMusic.unshift(music);
      this.listMusicChange.next(this.listMusic);
    }
  }

  getMusicByIdInList(id: number): MusicFile {
    for (const m of this.listMusic) {
      if (m.id === id) {
        return m;
      }
    }
    return null;
  }

  // getMusicXmlById(id:number):Observable<any> {
  //   return this.httpClient.get(this.apiUrl + '/xml/'+id);
  // }
}
