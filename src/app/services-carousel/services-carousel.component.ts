import { Component, OnInit } from '@angular/core';
import { CARDS } from '../list-card';

@Component({
  selector: 'app-services-carousel',
  templateUrl: './services-carousel.component.html',
  styleUrls: ['./services-carousel.component.css']
})
export class ServicesCarouselComponent implements OnInit {

  cards = CARDS;
  showIndicator = false;
  noPause = false;

  constructor() { }

  ngOnInit() {
  }

}
