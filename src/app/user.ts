export class User {

  id: number;
  username: string;
  password: string;
  email: string;
  emailChecked:boolean;
  instruments:String;
  player:boolean;
  composer:boolean;
  description:string;
  subscriptionId:number;
  superUser:boolean;
  newsletter:boolean;

  constructor(id: number, username: string, password: string, email: string, emailChecked:boolean, instruments:String, player:boolean, composer:boolean, description:string, subscriptionId:number, superUser:boolean,newsletter:boolean){
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
    this.emailChecked = emailChecked;
    this.instruments = instruments;
    this.player = player;
    this.composer = composer;
    this.description = description;
    this.subscriptionId = subscriptionId;
    this.superUser = superUser;
    this.newsletter = newsletter;
  }
}