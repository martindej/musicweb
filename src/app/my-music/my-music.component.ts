
import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Subscription , Subject } from 'rxjs';
import { Router } from '@angular/router';




import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { StyleService } from '../style.service';
import { AlertService } from '../alert.service';
import { ComposeService } from '../compose.service';
import { StyleMusicFull } from '../styleMusicFull';
import { MidiInstrument } from '../midiInstrument';
import { MusicFile } from '../MusicFile';
import { MessageAlert } from '../messageAlert';

@Component({
  selector: 'app-my-music',
  templateUrl: './my-music.component.html',
  styleUrls: ['./my-music.component.css']
})
export class MyMusicComponent implements OnInit, OnDestroy {

  @ViewChild('toolbar') toolbar: ElementRef;
  @ViewChild('filters') filters: ElementRef;
  @ViewChild('gallery') gallery: ElementRef;

  stylesMusic: StyleMusicFull[] = [];
  midiInstruments: MidiInstrument[];
  stylesVisible: boolean;
  fixedFilterNav: boolean;
  marginTopFilter: number;
  illustration = false;

  private subscriptionIdImage;
  idImage: number;
  urlImage = 'https://picsum.photos/640/640?image=0';

  nameSearch = new Subject<string>();

  private subscriptionMusic;
  musicFile: MusicFile;

  filter: {name: string, like: boolean, visibility: boolean,  style: string[], instruments: string[]} = {'name': '', 'like': false, 'visibility': false, 'style': [], 'instruments': []};
  private subscriptionFilter: Subscription;

  constructor(
     private mainService: MainService,
     private musicService: MusicService,
     private styleService: StyleService,
     private alertService: AlertService,
     private composeService: ComposeService,
     private router: Router) { }

  ngOnInit() {
    window.scroll(0, 0);
    this.getListStyles();
    this.getInstrumentList();
    this.stylesVisible = false;

    this.idImage = this.composeService.currentIdImage;
    if (!this.idImage) {
      this.getListIdImages();
    }
    else {
      this.urlImage = 'https://picsum.photos/640/640?image=' + this.idImage;
    }
    this.subscriptionIdImage = this.composeService.idImage.subscribe(
      idImage => {
        this.idImage = idImage;
        this.urlImage = 'https://picsum.photos/640/640?image=' + idImage;
    });

    this.filter = this.musicService.filter;
    this.subscriptionFilter = this.musicService.filterChange.subscribe((value) => {
      this.filter = value;
    });

    this.nameSearch.pipe(
      debounceTime(2000),
      distinctUntilChanged(),)
      .subscribe(term => this.filterByName(term));

    this.subscriptionMusic = this.mainService.currentMusicFile.subscribe(
      data => {
        this.musicFile = data;
        this.urlImage = 'https://picsum.photos/640/640?image=' + this.musicFile.picture_number;
    });
  }

  ngOnDestroy() {
    this.subscriptionMusic.unsubscribe();
    this.subscriptionFilter.unsubscribe();
    this.subscriptionIdImage.unsubscribe();
  }

  @HostListener('window:scroll', ['$event'])
    checkScroll() {
      const scrollPosition = window.pageYOffset;
      const toolbarPosition = this.toolbar.nativeElement.offsetTop;
      const filtersHeight = this.filters.nativeElement.offsetHeight;
      const galleryBottom = this.gallery.nativeElement.offsetHeight;


      this.marginTopFilter = scrollPosition - toolbarPosition + 100;

      if (this.marginTopFilter + filtersHeight > galleryBottom) {
        this.marginTopFilter = galleryBottom - filtersHeight;
      }

      if (scrollPosition >= toolbarPosition) {
        this.fixedFilterNav = true;
      } else {
        this.fixedFilterNav = false;
      }
    }

  getFilterMarginTop() {
    if (this.fixedFilterNav) {
      return this.marginTopFilter;
    }
    else {
      return 0;
    }
  }

  getInstrumentList(): void {
    this.mainService.getInstrumentList()
      .subscribe(midiInstruments => this.midiInstruments = midiInstruments);
  }

  getListIdImages(): void {
    this.composeService.getListIdImages()
      .subscribe(listId => this.composeService.setListIdImages(listId));
  }

  getListStyles() {

    this.styleService.getStylesMusic()
        .subscribe(
          data => {
            this.stylesMusic = data;
          },
          error => {
            const message: MessageAlert = new MessageAlert('Error fetching list of styles', 5000, true, 'danger');
            this.alertService.success(message, true);
        });
  }

  filterByName(name: string) {
    this.musicService.toggleNameFilter(name);
  }

  filterByLike(like: boolean) {
    this.musicService.toggleLikeFilter(like);
  }

  filterByVisibility(visibility: boolean) {
    this.musicService.toggleVisibilityFilter(visibility);
  }

  filterByStyle(style: string) {
    this.musicService.toggleStyleFilter(style);
  }

  filterByInstruments(instrument: string) {
    this.musicService.toggleInstrumentsFilter(instrument);
  }

  getCurrentStateMusic(): string {
    if (this.router.url.indexOf('myMusicsList') !== -1) {
      return 'My music';
    }
    else {
      return 'Shared music';
    }
  }

  isCompositionState(): boolean {
    return this.router.url.indexOf('compose') !== -1;
  }

}
