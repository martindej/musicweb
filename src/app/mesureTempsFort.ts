export class MesureTempsFort {

  id: number;
  styleId: number;
  mesure: number;
  tempsFort: number[];
  etOu: boolean;

  constructor(
    id: number,
    styleId: number,
    mesure: number,
    tempsFort: number[],
    etOu: boolean) {
      this.id = id;
      this.styleId = styleId;
      this.mesure = mesure;
      this.tempsFort = tempsFort;
      this.etOu = etOu;
  }
}
