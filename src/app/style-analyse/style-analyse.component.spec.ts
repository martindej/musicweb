import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleAnalyseComponent } from './style-analyse.component';

describe('StyleAnalyseComponent', () => {
  let component: StyleAnalyseComponent;
  let fixture: ComponentFixture<StyleAnalyseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleAnalyseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleAnalyseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
