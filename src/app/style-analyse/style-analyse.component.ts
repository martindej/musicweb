import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
// import * as shape from 'd3-shape';

import { StyleMusicFull } from '../styleMusicFull';
import { MusicService } from '../music.service';
import { AlertService } from '../alert.service';
import { StyleService } from '../style.service';

@Component({
  selector: 'app-style-analyse',
  templateUrl: './style-analyse.component.html',
  styleUrls: ['./style-analyse.component.css']
})
export class StyleAnalyseComponent implements OnInit, OnDestroy {

  stylesMusic: StyleMusicFull[] = [];
  private subscriptionStyleMusic: Subscription;
  data;
  selectedStyle: StyleMusicFull;

  allParameters: string[];
  selectedParameter: string;

  allOutput: string[] = [];
  x;
  y;

  // view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Number';
  showYAxisLabel = true;
  yAxisLabel = 'Color Value';
  timeline = false;
  // curve: any = shape.bas;

   // line, area
  autoScale = true;

  //pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  colorScheme = {
    domain: [
      '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886'
    ]
  };

  multi: any[] = [
    {
      name: 'Cyan',
      series: [
        {
          name: 5,
          value: 2650
        },
        {
          name: 10,
          value: 2800      },
        {
          name: 15,
          value: 2000
        }
      ]
    },
    {
      name: 'Yellow',
      series: [
        {
          name: 5,
          value: 2500
        },
        {
          name: 10,
          value: 3100
        },
        {
          name: 15,
          value: 2350
        }
      ]
    }
  ];


  constructor(
    private styleService: StyleService,
    private musicService: MusicService,
    private alertService: AlertService) {

      this.stylesMusic = this.styleService.listStyles;
      this.subscriptionStyleMusic = this.styleService.listStyleChange.subscribe((value) => {
        this.stylesMusic = value;
      });
  }

  ngOnInit() {
    this.getParametersList();
  }

  ngOnDestroy() {
    this.subscriptionStyleMusic.unsubscribe();
  }

  buildData() {
    const map = this.data.mapAnalyseMusic;

    console.log(typeof(map));
    const listIns = map[0].listAnalyseGroupInstrument;
    const insRef = listIns[0];

    let entry = {};
    let bigEntry = {};
    const final = [];

    for (const prop in insRef) {
      if (insRef.hasOwnProperty(prop)) {

        for (const indexIns in listIns) {
          if (listIns.hasOwnProperty(indexIns)) {
            const values = [];
            for (const indexMap in map) {
              if (map.hasOwnProperty(indexMap)) {
                const ins = map[indexMap].listAnalyseGroupInstrument[indexIns];
                entry = {
                  name: indexMap,
                  value: ins[prop]
                };

                values.push(entry);
              }
            }

            bigEntry = {
              name : prop + map[0].listAnalyseGroupInstrument[indexIns].instrument,
              series : values
            };

            final.push(bigEntry);
          }

        }
      }
    }

    //  the data must be immutable. This will make the component register the change and trigger update.
    this.multi = [...final];

  }

  getAllOutput() {

    this.allOutput = [];
    const params = this.data.mapAnalyseMusic[0].listAnalyseGroupInstrument[0];

    for (const indexParam in params) {
      if (params.hasOwnProperty(indexParam)) {
        this.allOutput.push(indexParam);
      }
    }
  }

  // buildMap() {
  //   const map = this.data.mapAnalyseMusic;

  //   let entry = {};

  //   for (const indexMap in map) {
  //     if (map.hasOwnProperty(indexMap)) {
  //       const ins = map[indexMap].listAnalyseGroupInstrument[0];

  //       entry = {
  //         name: indexMap,
  //         value: ins[this.parameterY]
  //       };

  //       values.push(entry);
  //     }
  //   }
  // }

  onSelect(event) {
    console.log(event);
  }

  buildSimpleData(parameterX: string, parameterY: string) {
    const map = this.data.mapAnalyseMusic;
    const listIns = map[0].listAnalyseGroupInstrument;
    const insRef = listIns[0];

    let entry = {};
    let bigEntry = {};
    const final = [];

    this.xAxisLabel = parameterX + ' in style';
    this.yAxisLabel = parameterY + ' in output';

    for (const indexIns in listIns) {
      if (listIns.hasOwnProperty(indexIns)) {
        const values = [];
        for (const indexMap in map) {
          if (map.hasOwnProperty(indexMap)) {
            const ins = map[indexMap].listAnalyseGroupInstrument[indexIns];
            entry = {
              name: indexMap,
              value: ins[parameterY]
            };

            values.push(entry);
          }
        }

        bigEntry = {
          name : parameterY + ' ' + map[0].listAnalyseGroupInstrument[indexIns].instrument,
          series : values
        };

        final.push(bigEntry);
      }

    }

    this.multi = [...final];

  }

  analyse() {

    if (this.selectedParameter && this.selectedStyle) {
      this.musicService.analyseStyle(this.selectedStyle.id, this.selectedParameter).subscribe(
        data => {
          console.log(data);
          this.data = data;
          this.getAllOutput();
          // this.buildSimpleData(this.selectedParameter, this.allOutput[0]);
          console.log(this.multi);
        }
      );
    }
  }

  getParametersList() {
    this.musicService.getAllAnalyseParameters().subscribe(
      data => {
        console.log(data);
        this.allParameters = data;
      }
    );
  }

}
