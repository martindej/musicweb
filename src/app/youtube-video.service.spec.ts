import { TestBed, inject } from '@angular/core/testing';

import { YoutubeVideoService } from './youtube-video.service';

describe('YoutubeVideoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YoutubeVideoService]
    });
  });

  it('should be created', inject([YoutubeVideoService], (service: YoutubeVideoService) => {
    expect(service).toBeTruthy();
  }));
});
