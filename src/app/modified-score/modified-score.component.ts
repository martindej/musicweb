import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';

import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-modified-score',
  templateUrl: './modified-score.component.html',
  styleUrls: ['./modified-score.component.css']
})
export class ModifiedScoreComponent implements OnInit, OnDestroy {

  @ViewChild('score') score: ElementRef;

  musicDetails: MusicFile;
  private subscriptionMusic: Subscription;

  private random: number;
  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };
  fileUrl: string;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private musicService: MusicService
  ) {

  }

  ngOnDestroy() {
    this.subscriptionMusic.unsubscribe();
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.musicDetails = this.musicService.musicDetails;
    if (this.musicDetails && this.musicDetails.id) {
      this.displayMainScore(this.musicDetails.musicXmlModified);
    }
    this.subscriptionMusic = this.musicService.musicDetailsChange.subscribe((value) => {
      this.musicDetails = value;
      this.displayMainScore(this.musicDetails.musicXmlModified);
    });
  }

  displayMainScore(scoreLink: string): void {
    if (this.score.nativeElement.firstChild != null) {
      this.score.nativeElement.removeChild(this.score.nativeElement.firstChild);
    }

    //   context.clearRect(0, 0, canvas.width, canvas.height);
    const osmd = new OpenSheetMusicDisplay(this.score.nativeElement);
      osmd.load(scoreLink)
      .then(
        () => {
          osmd.render();
          // osmd.cursor.show();
        },
        (err) => console.log(err)
      );
  }

  selectFile(event) {
    const file = event.target.files.item(0);

    if (file.type.match('xml.*')) {
      this.selectedFiles = event.target.files;
      this.upload();
    } else {
      alert('invalid format!');
    }
  }


  ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.musicService.updateModifiedScore(this.currentFileUpload, this.musicDetails.id).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      }
      else if (event.ok) {
        console.log('File is completely uploaded!');
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          this.musicDetails.musicXmlModified = this.ab2str(fileReader.result);
          this.displayMainScore(this.musicDetails.musicXmlModified);
          this.selectedFiles = undefined;
        };
        fileReader.readAsText(this.selectedFiles.item(0));

      }
      else {
        this.selectedFiles = undefined;
      }
      this.random = Math.random();
    });


  }
}
