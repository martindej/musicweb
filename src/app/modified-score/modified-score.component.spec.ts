import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiedScoreComponent } from './modified-score.component';

describe('ModifiedScoreComponent', () => {
  let component: ModifiedScoreComponent;
  let fixture: ComponentFixture<ModifiedScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiedScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiedScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
