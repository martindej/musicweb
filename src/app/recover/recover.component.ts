import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
 
import { AlertService} from '../alert.service';
import { AuthenticationService } from '../authentication.service';
import { EmailValidator } from  '../validators/email';
import { MessageAlert } from '../messageAlert';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.css']
})
export class RecoverComponent implements OnInit {

  @ViewChild('email') emailElement: ElementRef;

  loading = false;
  submitAttempt:boolean = false;

  userForm: FormGroup;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private fBuilder:FormBuilder,
    public emailValidator: EmailValidator) {

      this.userForm = fBuilder.group({
        email: fBuilder.control(null, [Validators.required, Validators.email])
      });
  }

  ngOnInit() {
    window.scroll(0, 0);
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;
 
    if(!this.userForm.valid){
        // this.signupSlider.slideTo(0);
    }
    else {
        this.authenticationService.resetPassword(this.userForm.value.email)
          .subscribe(
            data => {
              // set success message and pass true paramater to persist the message after redirecting to the login page
              let message:MessageAlert = new MessageAlert("A mail has just been sent with a new password, don't forget to change it from your account", 5000, true, 'success');
              this.alertService.success(message, true);
              this.router.navigate(['/login']);
              this.loading = false;
            },
            error => {
              let message:MessageAlert = new MessageAlert("A problem occured, try again later", 5000, true, 'danger');
              this.alertService.success(message, true);
              this.loading = false;
            });
    }
  }

}
