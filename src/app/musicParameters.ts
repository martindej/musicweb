export class MusicParameters {

    tempo:number;
	tempoMin:number;
	tempoMax:number;
	
	swing:number;
	swingMin:number;
	swingMax:number;

    tonality:string;
    percussion:boolean;
	
	musicTitle:string;
  
    constructor(tempo:number, tempoMin:number, tempoMax:number, swing:number, swingMin:number, swingMax:number, musicTitle:string, tonality:string, percussion:boolean) {
    	this.tempo = tempo;
    	this.tempoMax = tempoMax;
    	this.tempoMin = tempoMin;

    	this.swing = swing;
    	this.swingMax = swingMax;
    	this.swingMin = swingMin;

        this.tonality = tonality;
        this.percussion = percussion;

    	this.musicTitle = musicTitle;
    }

    setTonality(tonality) {
        this.tonality = tonality;
    }

    setPercussion(percussion) {
        this.percussion = percussion;
    }
}