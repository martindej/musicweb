import { DurationChord } from './durationChord';

export const DURATIONCHORDS: DurationChord[] = [
  // { id: 1, name: 'One'},
  { id: 0, name: 'Two'},
  { id: 1, name: 'Three'},
  { id: 2, name: 'Four'}
  // { id: 5, name: 'Five'}
];