
import {throwError as observableThrowError,  Observable, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/mergeMap';

import {catchError} from 'rxjs/operators';
import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse } from '@angular/common/http';


import { AuthenticationService } from './authentication.service';
 
@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    isRefreshingToken: boolean = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(private injector: Injector) {} 

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }})
    }
 
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        
        if(req.headers.get('Authorization') == null) {
          const token = localStorage.getItem("auth_token");

          return Observable.create((observer: any) => {
              setTimeout(() => {
                  const authenticationService = this.injector.get(AuthenticationService);
                  observer.next(token)
                  observer.complete()
              })
          })
          .mergeMap((Authorization: string) => {
              let authReq = req

              if (Authorization) {
                  authReq = this.addToken(req, token);
              }

              return next.handle(authReq).pipe(
                  catchError(error => {
                      if (error instanceof HttpErrorResponse) {
                          switch ((<HttpErrorResponse>error).status) {
                              case 400:
                                  return this.handle400Error(error);
                              case 401:
                                  return this.handle401Error(req, next);
                          }
                      } else {
                          return observableThrowError(error);
                      }
                  }));
          })
        } else {
          return next.handle(req);
        }
    }

    handle400Error(error) {
        console.log("error 400 = "+ error)
        if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
            // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
            return Observable.create((observer: any) => {
                setTimeout(() => {
                    const authenticationService = this.injector.get(AuthenticationService);
                    authenticationService.logout();
                    authenticationService.backToMainPage();
                })
            })
        }
 
        return observableThrowError(error);
    }

    handle401Error(req: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;
 
            // Reset here so that the following requests wait until the token
            // comes back from the refreshToken call.
            this.tokenSubject.next(null);

            return Observable.create((observer: any) => {
                setTimeout(() => {
                    const authenticationService = this.injector.get(AuthenticationService);
                    return authenticationService.refreshToken().subscribe(
                          data => {

                              this.isRefreshingToken = false;
                              if(data) {
                                  authenticationService.setSession(data);
                              }
                              else {
                                  authenticationService.logout();
                                  authenticationService.backToMainPage();
                              }
                              return observableThrowError("");
                          })
                })
            })
 
            
        } else {
            // return this.tokenSubject
            //     .filter(token => token != null)
            //     .take(1)
            //     .switchMap(token => {
            //         return next.handle(this.addToken(this.getNewRequest(req), token));
            //     });
        }
    }
}