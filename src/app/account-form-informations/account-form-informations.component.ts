import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { User } from "../user";
import { AuthenticationService } from '../authentication.service';
import { AvatarDirective } from '../avatar.directive';
import { UsernameUpdateValidator } from  '../validators/usernameUpdate';
import { EmailUpdateValidator } from  '../validators/emailUpdate';
import { PasswordValidator } from '../validators/password';
import { AlertService} from '../alert.service';
import { MidiInstrument } from '../midiInstrument';
import { MIDIINSTRUMENTS} from '../list-midiInstrument';
import { MessageAlert } from '../messageAlert';

@Component({
  selector: 'app-account-form-informations',
  templateUrl: './account-form-informations.component.html',
  styleUrls: ['./account-form-informations.component.css']
})
export class AccountFormInformationsComponent implements OnInit {

  @Input() getUserFormInfos: String;
  @Output() getUserFormInfosChange = new EventEmitter<String>();
  currentUser: User;
  private subscription:Subscription;
  userForm: FormGroup;

  private random:number;
  private userFormInfosDisabled:boolean = true;

  listInstruments = MIDIINSTRUMENTS;
  listSelectedInstruments: Array<MidiInstrument> = [];
  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 }

  loading = false;
  submitAttempt:boolean = false;


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private fBuilder:FormBuilder,
    public usernameValidator: UsernameUpdateValidator,
    public emailValidator: EmailUpdateValidator) {

      this.currentUser = authenticationService.currentUser;
      this.subscription = authenticationService.userChange.subscribe((value) => { 
        this.currentUser = value; 
      });

      this.userForm = fBuilder.group({
        username: fBuilder.control(this.currentUser.username, [Validators.required, Validators.pattern('[a-zA-Z]*')], usernameValidator.checkUsername.bind(usernameValidator)),
        email: fBuilder.control(this.currentUser.email, [Validators.required, Validators.email], emailValidator.checkEmail.bind(emailValidator)),
        instruments: fBuilder.control(this.currentUser.instruments),
        player: fBuilder.control(this.currentUser.player),
        composer: fBuilder.control(this.currentUser.composer),
        description: fBuilder.control(this.currentUser.description),
        newsletter: fBuilder.control(this.currentUser.newsletter)
      });

      this.listSelectedInstruments = this.listInstrumentsFromString();
  }

  ngOnInit() {
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;
 
    if(!this.userForm.valid){
        // this.signupSlider.slideTo(0);
    }
    else {
        let model: User = new User(this.currentUser.id,this.userForm.value.username,this.currentUser.password, this.userForm.value.email, false, this.instrumentsToString(), this.userForm.value.player, this.userForm.value.composer, this.userForm.value.description,0,false, this.userForm.value.newsletter);
        // User user = new User(0,model.username, model.password, )
        this.authenticationService.updateUser(model)
          .subscribe(
            data => {
              this.authenticationService.login(model.username).subscribe(
                data => {
                  let message:MessageAlert = new MessageAlert("account modified successfully", 5000, true, 'success');
                  // set success message and pass true paramater to persist the message after redirecting to the login page
                   this.alertService.success(message, true);
                   this.getUserFormInfosChange.emit("info");
                  // this.router.navigate(['/login']);
                },
                error => {
                  let message:MessageAlert = new MessageAlert("a problem occured, please try again later", 5000, true, 'danger');
                  this.alertService.error(message);
                  this.loading = false;
                });;
              // set success message and pass true paramater to persist the message after redirecting to the login page
              // this.alertService.success('Registration successful', true);
              // this.router.navigate(['/login']);
            },
            error => {
              let message:MessageAlert = new MessageAlert("a problem occured, please try again later", 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

  listInstrumentsFromString():Array<MidiInstrument> {
  
    let instru:Array<MidiInstrument> = [];

    if(this.currentUser && this.currentUser.instruments) {
      
      let instrument:MidiInstrument;
      let arr = this.currentUser.instruments.split(",");
  
      for (var i = 0; i < arr.length; i++) {
        instrument = this.getInstrumentById(parseInt(arr[i]));
  
        if(instrument) {
          instru.push(instrument);
        }
      }
    }
    return instru;
  }

  instrumentsToString():String {

    let toString:String;

    for (let entry of this.listSelectedInstruments) {
      if(!toString || toString.length == 0) {
        toString = String(entry.id);
      }
      else {
        toString = toString +","+entry.id;
      }
    }

    return toString;
  }

  addToList(instrument: MidiInstrument): void {
  //  console.log(id);
  //  let instrument = this.getInstrumentById(id);
    this.listSelectedInstruments.push(instrument);
  }

  getInstrumentById(id:number):MidiInstrument {
    for (let entry of this.listInstruments) {
      if(entry.id == id) {
        return entry;
      }
    }
  }

  deleteFromList(instrument: MidiInstrument): void {
    
    let index = this.listSelectedInstruments.indexOf(instrument, 0);

    if (index > -1) {
      this.listSelectedInstruments.splice(index, 1);
    }
  }

}