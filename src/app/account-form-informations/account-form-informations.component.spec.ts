import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountFormInformationsComponent } from './account-form-informations.component';

describe('AccountFormInformationsComponent', () => {
  let component: AccountFormInformationsComponent;
  let fixture: ComponentFixture<AccountFormInformationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountFormInformationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFormInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
