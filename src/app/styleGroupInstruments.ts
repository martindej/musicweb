export class StyleGroupInstrumentsFull {

  id: number;
  styleId: number;
  instruments: number[];
  name: string;
  motherId: number;

  pourcentagePlayingMin: number;
  pourcentagePlayingMax: number;
  balanceBassMelodyMin: number;
  balanceBassMelodyMax: number;
  balanceMelodyMelodyBisMin: number;
  balanceMelodyMelodyBisMax: number;
  gBetweenNotesMin: number;
  gBetweenNotesMax: number;
  dispersionNotesMin: number;
  dispersionNotesMax: number;
  pourcentageSilMin: number;
  pourcentageSilMax: number;
  speedMax: number;
  speedMin: number;
  pSyncopesMin: number;
  pSyncopesMax: number;
  pSyncopettesMin: number;
  pSyncopettesMax: number;
  pSyncopesTempsFortsMin: number;
  pSyncopesTempsFortsMax: number;
  pContreTempsMin: number;
  pContreTempsMax: number;
  pDissonanceMin: number;
  pDissonanceMax: number;
  pIrregularitesMin: number;
  pIrregularitesMax: number;
  bBinaireTernaireMin: number;
  bBinaireTernaireMax: number;
  pChanceSupSameRythmMin: number;
  pChanceSupSameRythmMax: number;
  numberNotesSimultaneouslyMin: number;
  numberNotesSimultaneouslyMax: number;
  probabilityRythmMin: number;
  probabilityRythmMax: number;
  numberRythmMin: number;
  numberRythmMax: number;
  weightRythmMin: number;
  weightRythmMax: number;
  pChanceSameNoteInMotifMin: number;
  pChanceSameNoteInMotifMax: number;
  pChanceStartByTonaleAsBassMin: number;
  pChanceStartByTonaleAsBassMax: number;
  pChanceStayAroundNoteRefMin: number;
  pChanceStayAroundNoteRefMax: number;
  pChancePlayChordMin: number;
  pChancePlayChordMax: number;

    constructor(id: number,
                styleId: number,
                name: string,
                motherId: number,
                instruments: number[],
                pourcentagePlayingMin: number,
                pourcentagePlayingMax: number,
                balanceBassMelodyMin: number,
                balanceBassMelodyMax: number,
                balanceMelodyMelodyBisMin: number,
                balanceMelodyMelodyBisMax: number,
                gBetweenNotesMin: number,
                gBetweenNotesMax: number,
                dispersionNotesMin: number,
                dispersionNotesMax: number,
                pourcentageSilMin: number,
                pourcentageSilMax: number,
                speedMax: number,
                speedMin: number,
                pSyncopesMin: number,
                pSyncopesMax: number,
                pSyncopettesMin: number,
                pSyncopettesMax: number,
                pSyncopesTempsFortsMin: number,
                pSyncopesTempsFortsMax: number,
                pContreTempsMin: number,
                pContreTempsMax: number,
                pDissonanceMin: number,
                pDissonanceMax: number,
                pIrregularitesMin: number,
                pIrregularitesMax: number,
                bBinaireTernaireMin: number,
                bBinaireTernaireMax: number,
                pChanceSupSameRythmMin: number,
                pChanceSupSameRythmMax: number,
                numberNotesSimultaneouslyMin: number,
                numberNotesSimultaneouslyMax: number,
                probabilityRythmMin: number,
                probabilityRythmMax: number,
                numberRythmMin: number,
                numberRythmMax: number,
                weightRythmMin: number,
                weightRythmMax: number,
                pChanceSameNoteInMotifMin: number,
                pChanceSameNoteInMotifMax: number,
                pChanceStartByTonaleAsBassMin: number,
                pChanceStartByTonaleAsBassMax: number,
                pChanceStayAroundNoteRefMin: number,
                pChanceStayAroundNoteRefMax: number,
                pChancePlayChordMin: number,
                pChancePlayChordMax: number) {
      this.id = id;
      this.styleId = styleId;
      this.name = name;
      this.motherId = motherId;
      this.instruments = instruments;

      this.pourcentagePlayingMin = pourcentagePlayingMin;
      this.pourcentagePlayingMax = pourcentagePlayingMax;
      this.balanceBassMelodyMin = balanceBassMelodyMin;
      this.balanceBassMelodyMax = balanceBassMelodyMax;
      this.balanceMelodyMelodyBisMin = balanceMelodyMelodyBisMin;
      this.balanceMelodyMelodyBisMax = balanceMelodyMelodyBisMax;
      this.gBetweenNotesMin =  gBetweenNotesMin;
      this.gBetweenNotesMax = gBetweenNotesMax;
      this.dispersionNotesMin = dispersionNotesMin;
      this.dispersionNotesMax = dispersionNotesMax;
      this.pourcentageSilMin =  pourcentageSilMin;
      this.pourcentageSilMax = pourcentageSilMax;
      this.speedMax =  speedMax;
      this.speedMin =  speedMin;
      this.pSyncopesMin =  pSyncopesMin;
      this.pSyncopesMax = pSyncopesMax;
      this.pSyncopettesMin =  pSyncopettesMin;
      this.pSyncopettesMax = pSyncopettesMax;
      this.pSyncopesTempsFortsMin =  pSyncopesTempsFortsMin;
      this.pSyncopesTempsFortsMax =  pSyncopesTempsFortsMax;
      this.pContreTempsMin =  pContreTempsMin;
      this.pContreTempsMax = pContreTempsMax;
      this.pDissonanceMin = pDissonanceMin;
      this.pDissonanceMax = pDissonanceMax;
      this.pIrregularitesMin = pIrregularitesMin;
      this.pIrregularitesMax = pIrregularitesMax;
      this.bBinaireTernaireMin = bBinaireTernaireMin;
      this.bBinaireTernaireMax = bBinaireTernaireMax;
      this.pChanceSupSameRythmMin = pChanceSupSameRythmMin;
      this.pChanceSupSameRythmMax = pChanceSupSameRythmMax;
      this.numberNotesSimultaneouslyMin = numberNotesSimultaneouslyMin;
      this.numberNotesSimultaneouslyMax = numberNotesSimultaneouslyMax;
      this.probabilityRythmMin = probabilityRythmMin;
      this.probabilityRythmMax = probabilityRythmMax;
      this.numberRythmMin = numberRythmMin;
      this.numberRythmMax = numberRythmMax;
      this.weightRythmMin = weightRythmMin;
      this.weightRythmMax = weightRythmMax;
      this.pChanceSameNoteInMotifMin = pChanceSameNoteInMotifMin;
      this.pChanceSameNoteInMotifMax = pChanceSameNoteInMotifMax;
      this.pChanceStartByTonaleAsBassMin = pChanceStartByTonaleAsBassMin;
      this.pChanceStartByTonaleAsBassMax = pChanceStartByTonaleAsBassMax;
      this.pChanceStayAroundNoteRefMin = pChanceStayAroundNoteRefMin;
      this.pChanceStayAroundNoteRefMax  = pChanceStayAroundNoteRefMax;
      this.pChancePlayChordMin = pChancePlayChordMin;
      this.pChancePlayChordMax = pChancePlayChordMax;
    }
 }
