import { trigger, state, style, transition, animate } from '@angular/animations';

export const navbarTransparent = trigger('navbarTransparent', [
    state('transparent', style({
      background: 'rgba(0,0,0,0)'
    })),
    state('opaque',   style({
        background: '#f5f6fa'
    })),
    transition('transparent => opaque', animate('500ms ease-out')),
    transition('opaque => transparent', animate('500ms ease-in')),
  ]);
