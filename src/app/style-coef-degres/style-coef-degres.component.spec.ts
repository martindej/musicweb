import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleCoefDegresComponent } from './style-coef-degres.component';

describe('StyleCoefDegresComponent', () => {
  let component: StyleCoefDegresComponent;
  let fixture: ComponentFixture<StyleCoefDegresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleCoefDegresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleCoefDegresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
