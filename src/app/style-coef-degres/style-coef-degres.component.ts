import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { StyleCoefDegres } from '../styleCoefDegres';
import { StyleService } from '../style.service';
import { AlertService} from '../alert.service';
import { MessageAlert } from '../messageAlert';

@Component({
  selector: 'app-style-coef-degres',
  templateUrl: './style-coef-degres.component.html',
  styleUrls: ['./style-coef-degres.component.css']
})
export class StyleCoefDegresComponent implements OnInit {

  @Input() styleCoefDegres: StyleCoefDegres;

  loading: Boolean = false;

  styleCoefDegresForm: FormGroup;

  constructor(
    private fBuilder: FormBuilder,
    private styleService: StyleService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.styleCoefDegresForm = this.fBuilder.group({
      c11: this.fBuilder.control(this.styleCoefDegres.c11, [Validators.required]),
      c12: this.fBuilder.control(this.styleCoefDegres.c12, [Validators.required]),
      c13: this.fBuilder.control(this.styleCoefDegres.c13, [Validators.required]),
      c14: this.fBuilder.control(this.styleCoefDegres.c14, [Validators.required]),
      c15: this.fBuilder.control(this.styleCoefDegres.c15, [Validators.required]),
      c16: this.fBuilder.control(this.styleCoefDegres.c16, [Validators.required]),
      c17: this.fBuilder.control(this.styleCoefDegres.c17, [Validators.required]),

      c21: this.fBuilder.control(this.styleCoefDegres.c21, [Validators.required]),
      c22: this.fBuilder.control(this.styleCoefDegres.c22, [Validators.required]),
      c23: this.fBuilder.control(this.styleCoefDegres.c23, [Validators.required]),
      c24: this.fBuilder.control(this.styleCoefDegres.c24, [Validators.required]),
      c25: this.fBuilder.control(this.styleCoefDegres.c25, [Validators.required]),
      c26: this.fBuilder.control(this.styleCoefDegres.c26, [Validators.required]),
      c27: this.fBuilder.control(this.styleCoefDegres.c27, [Validators.required]),

      c31: this.fBuilder.control(this.styleCoefDegres.c31, [Validators.required]),
      c32: this.fBuilder.control(this.styleCoefDegres.c32, [Validators.required]),
      c33: this.fBuilder.control(this.styleCoefDegres.c33, [Validators.required]),
      c34: this.fBuilder.control(this.styleCoefDegres.c34, [Validators.required]),
      c35: this.fBuilder.control(this.styleCoefDegres.c35, [Validators.required]),
      c36: this.fBuilder.control(this.styleCoefDegres.c36, [Validators.required]),
      c37: this.fBuilder.control(this.styleCoefDegres.c37, [Validators.required]),

      c41: this.fBuilder.control(this.styleCoefDegres.c41, [Validators.required]),
      c42: this.fBuilder.control(this.styleCoefDegres.c42, [Validators.required]),
      c43: this.fBuilder.control(this.styleCoefDegres.c43, [Validators.required]),
      c44: this.fBuilder.control(this.styleCoefDegres.c44, [Validators.required]),
      c45: this.fBuilder.control(this.styleCoefDegres.c45, [Validators.required]),
      c46: this.fBuilder.control(this.styleCoefDegres.c46, [Validators.required]),
      c47: this.fBuilder.control(this.styleCoefDegres.c47, [Validators.required]),

      c51: this.fBuilder.control(this.styleCoefDegres.c51, [Validators.required]),
      c52: this.fBuilder.control(this.styleCoefDegres.c52, [Validators.required]),
      c53: this.fBuilder.control(this.styleCoefDegres.c53, [Validators.required]),
      c54: this.fBuilder.control(this.styleCoefDegres.c54, [Validators.required]),
      c55: this.fBuilder.control(this.styleCoefDegres.c55, [Validators.required]),
      c56: this.fBuilder.control(this.styleCoefDegres.c56, [Validators.required]),
      c57: this.fBuilder.control(this.styleCoefDegres.c57, [Validators.required]),

      c61: this.fBuilder.control(this.styleCoefDegres.c61, [Validators.required]),
      c62: this.fBuilder.control(this.styleCoefDegres.c62, [Validators.required]),
      c63: this.fBuilder.control(this.styleCoefDegres.c63, [Validators.required]),
      c64: this.fBuilder.control(this.styleCoefDegres.c64, [Validators.required]),
      c65: this.fBuilder.control(this.styleCoefDegres.c65, [Validators.required]),
      c66: this.fBuilder.control(this.styleCoefDegres.c66, [Validators.required]),
      c67: this.fBuilder.control(this.styleCoefDegres.c67, [Validators.required]),

      c71: this.fBuilder.control(this.styleCoefDegres.c71, [Validators.required]),
      c72: this.fBuilder.control(this.styleCoefDegres.c72, [Validators.required]),
      c73: this.fBuilder.control(this.styleCoefDegres.c73, [Validators.required]),
      c74: this.fBuilder.control(this.styleCoefDegres.c74, [Validators.required]),
      c75: this.fBuilder.control(this.styleCoefDegres.c75, [Validators.required]),
      c76: this.fBuilder.control(this.styleCoefDegres.c76, [Validators.required]),
      c77: this.fBuilder.control(this.styleCoefDegres.c77, [Validators.required]),

      coefDegre: this.fBuilder.control(this.styleCoefDegres.coefDegre, [Validators.required])

    });
  }

  save() {
    this.loading = true;

    if (!this.styleCoefDegresForm.valid) {
        // this.signupSlider.slideTo(0);
    } else {
        const model: StyleCoefDegres = this.styleCoefDegresForm.value;
        model.id = this.styleCoefDegres.id;
        model.styleId = this.styleCoefDegres.styleId;

        this.styleService.addNewStyleCoefDegres(model)
          .subscribe(
            data => {
              this.getStyles();
            },
            error => {
              const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

  delete() {
    this.styleService.deleteStyleCoefDegres(this.styleCoefDegres.id)
      .subscribe(
        data => {
          this.getStyles();
        },
        error => {
          const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
          this.alertService.error(message);
        });
  }

  getStyles(): void {
    this.loading = true;
    this.styleService.getStylesMusic().subscribe(
      data => {
        this.styleService.setAllStyles(data);
        this.loading = false;
      }
    );
  }

}
