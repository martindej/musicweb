export class ProfileCard {
    username: string;
    image:string;
    text:string;
    numberFollowers:number;
  }