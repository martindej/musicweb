import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders} from '@angular/common/http';
import { Observable, Subscription, Subject } from 'rxjs';
// import { Http, Response, RequestOptions, Headers } from "@angular/http";

import { User } from './user';
import { CommentMusic } from './CommentMusic';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class CommentService {

  listComments:CommentMusic[] = [];
  listCommentChange: Subject<CommentMusic[]> = new Subject<CommentMusic[]>();

  private apiUrl = '/api/comment';
  currentUser: User;
  private subscription:Subscription;

  constructor(
    private httpClient: HttpClient, 
    // private http: Http,
    private authenticationService: AuthenticationService) {
      this.currentUser = authenticationService.currentUser;
      this.subscription = authenticationService.userChange.subscribe((value) => { 
        this.currentUser = value; 
      });
    }

  getCommentsByMusicId(music_id:number, page:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getCommentsByMusicId/' + music_id + '/' + page);
  }

  getNumberLikes(comment_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getNumberLikesByCommentId/' + comment_id);
  }

  getNumberDislikes(comment_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getNumberDislikesByCommentId/' + comment_id);
  }

  getNumberCommentsByMusicId(music_id:number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/getNumberCommentsByMusicId/' + music_id);
  }

  setAllComments(data) {
    this.listComments = [];
    for(let m of data) {
      let commentMusic:CommentMusic = new CommentMusic(m.id, m.user_id, m.user_name,m.music_id, m.date, m.message);
      this.listComments.push(commentMusic);
    }
    this.listCommentChange.next(this.listComments);
  }

  addAllComments(data) {
    for(let m of data) {
      let commentMusic:CommentMusic = new CommentMusic(m.id, m.user_id, m.user_name,m.music_id, m.date, m.message);
      this.listComments.push(commentMusic);
    }
    this.listCommentChange.next(this.listComments);
  }

  deleteComment(comment:CommentMusic): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/deleteComment/'+comment.id+'/'+this.currentUser.id);
  }

  deleteInLocalList(comment:CommentMusic) {
    var index = this.listComments.indexOf(comment, 0);
    if (index > -1) {
      this.listComments.splice(index, 1);
    }
    this.listCommentChange.next(this.listComments);
  }

  send(commentMusic: CommentMusic): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    return this.httpClient.post(this.apiUrl + '/create', commentMusic, {headers});
  }

  hasLike(idComment:number, idUser:number):Observable<any> {
    return this.httpClient.get(this.apiUrl + '/hasLike/' + idComment + '/' + idUser);
  }

  hasDislike(idComment:number, idUser:number):Observable<any> {
    return this.httpClient.get(this.apiUrl + '/hasDislike/' + idComment + '/' + idUser);
  }

  removeEntryLike(idComment:number, idUser:number):Observable<any> {
    return this.httpClient.get(this.apiUrl + '/removeEntryLike/' + idComment + '/' + idUser);
  }

  like(idComment:number, idOwner: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/like/'+idComment+'/'+this.currentUser.id + '/' + idOwner);
  }

  dislike(idComment:number, idOwner: number): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/dislike/'+idComment+'/'+this.currentUser.id + '/' + idOwner);
  }

  signalComment(idComment:number, idOwner: number): Observable<any>  {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/signalComment/'+idComment+'/'+this.currentUser.id + '/' + idOwner, {headers});
  }

}
