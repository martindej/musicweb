import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert.service';
import { MessageAlert } from '../messageAlert';
 
@Component({
    moduleId: module.id,
    selector: 'alertDiv',
    styleUrls: ['./alert.component.css'],
    templateUrl: 'alert.component.html'
})
 
export class AlertComponent {
    message:MessageAlert;
    dismissible = true;
 
    constructor(private alertService: AlertService) { }
 
    ngOnInit() {
        this.alertService.getMessage().subscribe(message => { this.message = message; });
    }
}
