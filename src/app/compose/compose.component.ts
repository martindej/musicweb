import { Component, OnInit, ElementRef, TemplateRef, ViewChild, OnDestroy, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';

import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { SubscriptionService } from '../subscription.service';
import { SubscriptionUser } from '../subscriptionUser';
import { MusicService } from '../music.service';
import { AlertService } from '../alert.service';
import { ComposeService } from '../compose.service';
import { MusicFile } from '../MusicFile';
import { MidiInstrument } from '../midiInstrument';
import { flip } from '../flipAnimation';
import { MessageAlert } from '../messageAlert';
import { MusicParameters } from '../musicParameters';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.css'],
  animations: [ flip ]
})

export class ComposeComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('row') row: ElementRef;
  @ViewChild('dropdownButton') dropdownButton: ElementRef;

  @ViewChild('modalTemplate') tpl: TemplateRef<any>;

  private subscriptionStyle;
  private subscriptionListInstruments;
  // private subscriptionMusicTitle;
  style = 0;
  // musicTitle:String;
  lisInstruments: MidiInstrument[];
  flip = 'start';
  loading = false;
  random = 0;
  panel = 'menu';

  musicParameters: MusicParameters;
  private subscriptionMusicParameters;

  currentUser: User = new User(0, '', '', '', false, null, false, false, '', 0, false, true);
  private subscription: Subscription;

  subUser: SubscriptionUser;
  private subscriptionSubUser: Subscription;

  numberMusicsSubUser: number;
  private subscriptionNumberMusicsSubUser: Subscription;

  numberMusicsBooster: number;
  private subscriptionNumberMusicsBooster: Subscription;

  idImage: number;
  private subscriptionIdImage: Subscription;

  modalRef: BsModalRef;

  constructor(
    private musicService: MusicService,
    private alertService: AlertService,
    private composeService: ComposeService,
    private authenticationService: AuthenticationService,
    private subscriptionService: SubscriptionService,
    private modalService: BsModalService,
    private router: Router
  ) {}

  ngOnDestroy() {
    this.subscriptionStyle.unsubscribe();
    this.subscriptionListInstruments.unsubscribe();
    this.subscriptionSubUser.unsubscribe();
    this.subscriptionNumberMusicsSubUser.unsubscribe();
    this.subscriptionNumberMusicsBooster.unsubscribe();
    this.subscriptionMusicParameters.unsubscribe();
  }

  ngOnInit() {

    this.idImage = this.composeService.currentIdImage;
    this.subscriptionIdImage = this.composeService.idImage.subscribe(
      idImage => {
        this.idImage = idImage;
    });

    this.style = this.composeService.currentStyle;
    this.subscriptionStyle = this.composeService.style.subscribe(
      style => {
        this.style = style;
    });

    this.subscriptionMusicParameters = this.composeService.currentMusicParameters.subscribe(
      musicParameters => {
        this.musicParameters = musicParameters;
    });

    this.subscriptionListInstruments = this.composeService.currentListInstruments.subscribe(
      lisInstruments => {
        this.lisInstruments = lisInstruments;
    });

    this.currentUser = this.authenticationService.currentUser;
    this.subscription = this.authenticationService.userChange.subscribe((value) => {
      this.currentUser = value;
    });

    this.subUser = this.subscriptionService.subUser;
    this.subscriptionSubUser = this.subscriptionService.subUserChange.subscribe(
      data => {
        this.subUser = data;
        this.getNumberMusicsSubscription(this.subUser.id);
    });

    this.numberMusicsBooster = this.subscriptionService.numberMusicsBooster;
    this.subscriptionNumberMusicsBooster = this.subscriptionService.numberMusicsBoosterChange.subscribe(
      data => {
        this.numberMusicsBooster = data;
      });

    this.numberMusicsSubUser = this.subscriptionService.numberMusicsSubUser;
    this.subscriptionNumberMusicsSubUser = this.subscriptionService.numberMusicsSubUserChange.subscribe(
      data => {
        this.numberMusicsSubUser = data;
      });

    this.getNumberMusicsBooster();
    this.getSubUser();
    this.composeService.newRandomRefreshCompose();
  }

  ngOnChanges(changes) {
    this.getNumberMusicsBooster();
    this.getSubUser();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'customModal modal-sm' })
    );
  }

  decline(): void {
    this.modalRef.hide();
  }

  getRowWidth(): number {
    return this.row.nativeElement.offsetWidth;
  }

  getLeftPositionCompareToButton(): number {
    return  - (this.dropdownButton.nativeElement.offsetLeft + this.row.nativeElement.offsetLeft);
  }

  getSubUser() {
    this.subscriptionService.getSubscriptionUser(this.currentUser.id).subscribe(
      sub => {
        this.subscriptionService.setSubUser(sub);
      });
  }

  getNumberMusicsSubscription(sub_id: number) {
    this.subscriptionService.getNumberMusicsSubscription(this.currentUser.id).subscribe(
      sub => {
        const data: any = sub;
        if (typeof data === 'number') {
          this.subscriptionService.setNumberMusicsSubscription(data);
        }

      });
  }

  getNumberMusicsBooster() {
    this.subscriptionService.getNumberMusicsBooster(this.currentUser.id).subscribe(
      boo => {
        const data: any = boo;
        if (typeof data === 'number') {
          this.subscriptionService.setNumberMusicsBooster(data);
        }
      });
  }

  toggleFlip() {
    this.flip = (this.flip === 'start') ? 'end' : 'start';
  }

  flipState(flip: string) {
    this.flip = flip;
  }

  generateMusic(style: number) {

    if (this.numberMusicsBooster === 0 && this.numberMusicsSubUser === 0) {
      this.openModal(this.tpl);
    }
    else if (!this.loading) {
      this.loading = true;
      this.musicService.generateMusic(style)
        .subscribe(
          event => {
            if (event.type === HttpEventType.DownloadProgress) {
              const percentDone = Math.round(100 * event.loaded / event.total);
              console.log(`File is ${percentDone}% downloaded.`);
            }
            else if (event instanceof HttpResponse) {

              const data = event.body;
              const music: MusicFile = new MusicFile(
                data.id,
                data.name,
                data.user_id,
                data.user_name,
                data.visible_origin,
                data.user_id_origin,
                data.user_name_origin,
                data.visible_modified,
                data.user_id_modified,
                data.user_name_modified,
                data.visible_video,
                data.user_id_video,
                data.user_name_video,
                'https://picsum.photos/640/640?image=' + data.pictureNumber,
                data.description,
                data.musicXmlFile,
                data.musicXmlFileModified,
                data.videoId,
                data.tempo,
                data.style,
                data.instruments,
                data.tonality,
                data.pictureNumber,
                data.creationDate);
              this.musicService.addMusicToList(music);
              this.loading = false;
              const message: MessageAlert = new MessageAlert('You got a new music', 5000, true, 'success');
              this.alertService.success(message);
              // this.getBooster();
              this.getNumberMusicsBooster();
              this.getSubUser();
              this.composeService.newRandomRefreshCompose();
            }
          }
          ,
          error => {
            this.loading = false;
            const message: MessageAlert = new MessageAlert('A problem occured generating the music, please try again later !', 5000, true, 'danger');
            this.alertService.error(message);
            this.composeService.newRandomRefreshCompose();
          });
    }

  }

  getCurrentStateMusic(): string {
    if (this.router.url.indexOf('mainSettings') !== -1) {
      return 'General';
    }
    else {
      return 'Advanced';
    }
  }
}
