import { Component, OnInit, Input } from '@angular/core';

import { CommentMusic } from '../CommentMusic';
import { CommentService } from '../comment.service';
import { User } from '../user';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() commentMusic: CommentMusic;
  @Input() currentUser: User;
  time:string;
  likeIcon:boolean = false;
  dislikeIcon:boolean = false;

  numberLikes:number = 0;
  numberDislikes:number = 0;
  random:number = 0;

  constructor(private commentService: CommentService) { }

  ngOnInit() {
    this.calculateDifDate();
    this.getNumberLikes();
    this.showLikeIcons();
  }

  getNumberLikes() {
    this.commentService.getNumberLikes(this.commentMusic.id)
    .subscribe(
      data => {
        this.numberLikes = data;
      }
    );

  this.commentService.getNumberDislikes(this.commentMusic.id)
    .subscribe(
      data => {
        this.numberDislikes = data;
      }
    );
  }

  showLikeIcons() {

    this.commentService.hasLike(this.commentMusic.id, this.currentUser.id)
      .subscribe(
        data => {
          this.likeIcon = data;
        }
      );

    this.commentService.hasDislike(this.commentMusic.id, this.currentUser.id)
      .subscribe(
        data => {
          this.dislikeIcon = data;
        }
      );
  }

  delete() {
    this.commentService.deleteComment(this.commentMusic)
      .subscribe(
        data => {
          if (data > 0) {
            this.commentService.deleteInLocalList(this.commentMusic);
          }
        }
      );
  }

  removeEntryLike() {
    this.commentService.removeEntryLike(this.commentMusic.id, this.commentMusic.user_id).subscribe(
      data => {
        this.showLikeIcons();
        this.getNumberLikes();
      }
    )
  }

  like() {
    this.commentService.like(this.commentMusic.id, this.commentMusic.user_id).subscribe(
      data => {
        this.showLikeIcons();
        this.getNumberLikes();
      }
    )
  }

  dislike() {
    this.commentService.dislike(this.commentMusic.id, this.commentMusic.user_id).subscribe(
      data => {
        this.showLikeIcons();
        this.getNumberLikes();
      }
    )
  }

  signalComment() {
    this.commentService.signalComment(this.commentMusic.id, this.commentMusic.user_id).subscribe(
      data => {
        console.log(data);
      });
  }

  calculateDifDate() {

    var diff = Math.floor(Date.now() - this.commentMusic.date);
    var second = 1000;

    var seconds = Math.floor(diff/second);
    var minutes = Math.floor(diff/(second*60));
    var hours = Math.floor(diff/(second*3600));
    var days = Math.floor(diff/(second*3600*24));
    var months = Math.floor(days/31);
    var years = Math.floor(months/12);

    if(years > 0) {
      this.time = years + " years ago";
    }
    else if(months > 0) {
      this.time = months + " months ago"
    }
    else if(days > 0) {
      this.time = days + " days ago"
    }
    else if(hours > 0) {
      this.time = hours + " hours ago"
    }
    else if(minutes > 0) {
      this.time = minutes + " minutes ago"
    }
    else if(seconds > 0) {
      this.time = seconds + " seconds ago"
    }
  }

}
