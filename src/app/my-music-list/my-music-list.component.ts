import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-my-music-list',
  templateUrl: './my-music-list.component.html',
  styleUrls: ['./my-music-list.component.css']
})
export class MyMusicListComponent implements OnInit, OnDestroy {

  listMusic:MusicFile[] = [];
  private subscription:Subscription;
  loading:boolean;
  page:number;
  moreVisible:boolean = true;

  filter:{name: string, like: boolean, visibility:boolean,  style:string[],instruments:string[]} = {"name": "", "like":false, "visibility":false, "style":[],"instruments":[]};
  private subscriptionFilter:Subscription;

  constructor(private mainService: MainService, private musicService: MusicService) {}

  ngOnInit() {
    this.page = 0;
    this.listMusic = this.musicService.listMusic;
    this.subscription = this.musicService.listMusicChange.subscribe((value) => { 
      this.listMusic = value; 
      if(this.listMusic.length > 0) {
        this.mainService.setFileMusic(this.listMusic[0]);
      }
    });

    this.filter = this.musicService.filter;
    this.subscriptionFilter = this.musicService.filterChange.subscribe((value) => { 
      this.filter = value; 
      this.page = 0;
      this.moreVisible = true;
      this.getMusics();
    });

    this.getMusics();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscriptionFilter.unsubscribe();
  }

  addMore(): void {
    this.loading = true;
    this.musicService.getAllMusics(this.page).subscribe(
      data => {
        if(data.length >= 20) {
          this.page++;
        }
        else {
          this.moreVisible = false;
        }
        this.musicService.addAllMusics(data);
        this.loading = false;
      }
    );
  }

  getMusics(): void {
    this.loading = true;
    this.musicService.getAllMusics(this.page).subscribe(
      data => {
        if(data.length >= 20) {
          this.page++;
        }
        else {
          this.moreVisible = false;
        }
        this.musicService.setAllMusics(data);
        this.loading = false;
      }
    );
  }

}
