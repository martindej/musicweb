export class Booster {

    id: number;
    name: string;
    numberMusics: number;
    price: number;
    generalSettings: boolean;
	  advancedSettings: boolean;
	  melodySettings: boolean;
  
    constructor(id: number, name: string, numberMusics: number, price: number, generalSettings: boolean, advancedSettings: boolean, melodySettings: boolean) {
      this.id = id;
      this.name = name;
      this.numberMusics = numberMusics;
      this.price = price;
      this.generalSettings = generalSettings;
      this.advancedSettings = advancedSettings;
	    this.melodySettings = melodySettings;
    }
  }