import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../authentication.service';
 
@Injectable()
export class UsernameValidator {

    debouncer: any;

    constructor(public authenticationService: AuthenticationService){

    }
 
  checkUsername(control: FormControl): any {

    // return Observable.timer(500).switchMap(()=>{
    //     return this.authenticationService.validateUsername(control.value)
    //       .mapTo(null)
    //       .catch(err=>Observable.of({availability: true}));
    //   });

    return new Promise(resolve => {
 
        // we use this debouncer wait one second before performing a request
        this.debouncer = setTimeout(() => {
            this.authenticationService.validateUsername(control.value)
            .subscribe(
                data => {
                    if(data === true) {
                        resolve({'usernameInUse': true});
                    }
                    else {
                        resolve(null);
                    }
                    
                },
                error => {
                    resolve({'usernameInUse': true});
                });
        }, 1000);  
    });
  }
 
}