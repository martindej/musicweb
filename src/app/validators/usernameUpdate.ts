import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { User } from "../user";
import { AuthenticationService } from '../authentication.service';
 
@Injectable()
export class UsernameUpdateValidator {

    debouncer: any;
    currentUser: User;
    private subscription:Subscription;

    constructor(private authenticationService: AuthenticationService) {
    
          this.currentUser = authenticationService.currentUser;
          this.subscription = authenticationService.userChange.subscribe((value) => { 
            this.currentUser = value; 
          });
      }
 
  checkUsername(control: FormControl): any {

    // return Observable.timer(500).switchMap(()=>{
    //     return this.authenticationService.validateUsername(control.value)
    //       .mapTo(null)
    //       .catch(err=>Observable.of({availability: true}));
    //   });

    return new Promise(resolve => {
 
        // we use this debouncer wait one second before performing a request

        if(this.currentUser.username != control.value) {
            this.debouncer = setTimeout(() => {
                this.authenticationService.validateUsername(control.value)
                .subscribe(
                    data => {
                        if(data === true) {
                            resolve({'usernameInUse': true});
                        }
                        else {
                            resolve(null);
                        }
                        
                    },
                    error => {
                        resolve({'usernameInUse': true});
                    });
            }, 1000);  
        }
        else {
            resolve(null);
        }
        
    });
  }
 
}