// import { Observable, Observer, Subscription } from 'rxjs/Rx';

// type AsyncValidatorFactory = (service: (value: any) => Observable<any | null>) => AsyncValidatorFn;

// const asyncValidatorFactory: AsyncValidatorFactory = (service: (value: any) => Observable<any | null>): AsyncValidatorFn => {
//     let subscription: Subscription = Subscription.EMPTY;
//     return (input: AbstractControl) => {
//         subscription.unsubscribe();
//         return Observable.create((observer: Observer<any | null>) => {
//             subscription = Observable.timer(400).flatMap(() => service(input.value)).subscribe(observer);
//             return () => subscription.unsubscribe();
//         });
//     };
// };