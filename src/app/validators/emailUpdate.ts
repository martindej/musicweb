import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { User } from "../user";
import { AuthenticationService } from '../authentication.service';
 
@Injectable()
export class EmailUpdateValidator {

    debouncer: any;
    currentUser: User;
    private subscription:Subscription;

    constructor(private authenticationService: AuthenticationService) {
    
          this.currentUser = authenticationService.currentUser;
          this.subscription = authenticationService.userChange.subscribe((value) => { 
            this.currentUser = value; 
          });
      }
 
  checkEmail(control: FormControl): any {

    return new Promise(resolve => {
 
        // we use this debouncer wait one second before performing a request
        if(this.currentUser.email != control.value) {
            this.debouncer = setTimeout(() => {
                this.authenticationService.validateEmail(control.value)
                .subscribe(
                    data => {
                        if(data === true) {
                            resolve({'emailInUse': true});
                        }
                        else {
                            resolve(null);
                        }
                        
                    },
                    error => {
                        resolve({'emailInUse': true});
                    });
            }, 1000);  
        }
        else {
            resolve(null);
        }
    });
  }
 
}