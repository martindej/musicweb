import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';

import { AuthenticationService } from '../authentication.service';
 
@Injectable()
export class EmailValidator {

    debouncer: any;

    constructor(public authenticationService: AuthenticationService){

    }
 
  checkEmail(control: FormControl): any {

    return new Promise(resolve => {
 
        // we use this debouncer wait one second before performing a request
        this.debouncer = setTimeout(() => {
            this.authenticationService.validateEmail(control.value)
            .subscribe(
                data => {
                    if(data === true) {
                        resolve({'emailInUse': true});
                    }
                    else {
                        resolve(null);
                    }
                    
                },
                error => {
                    resolve({'emailInUse': true});
                });
        }, 1000);  
    });
  }
 
}