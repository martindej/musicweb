import { Component, OnInit, Input, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Worker } from '../worker';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input() worker: Worker;
  // @ViewChild('element') element: ElementRef;

  // mainWidth;
  // seeMore = false;
  // right: string;
  // left: string;

  constructor() { }

  ngOnInit() {
    // this.checkLengthText();
    // this.mainWidth = this.element.nativeElement.clientWidth;
  }

  // @HostListener('mousemove', ['$event']) onMousemove(event: MouseEvent) { this.adjustPositionImg(event); }
  // @HostListener('mouseleave', ['$event']) onMouseleave(event: MouseEvent) { this.standardPositionImg(); }

  // checkLengthText() {
  //   if (this.worker.text.length > 150) {
  //     this.seeMore = true;
  //   }
  // }

  // adjustPositionImg(event: MouseEvent) {
  //   // 160 = 2 * 20 pour une marge sur le coté + 120 pour la largeur de l'image
  //   const rightValue = Math.floor((event.offsetX / this.mainWidth) * (this.mainWidth - 160) + 20);
  //   this.right = rightValue + 'px';
  //   this.left = 'auto';
  // }

  // standardPositionImg() {
  //   this.left = '50%';
  //   this.right = 'auto';
  // }

}
