import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  success:boolean = false;

  constructor(
    private authenticationService:AuthenticationService,
    private route: ActivatedRoute,
    private location: Location,) { }

  ngOnInit() {
    let username = this.route.snapshot.paramMap.get('username');
    let identifier = this.route.snapshot.paramMap.get('identifier');

    this.authenticationService.confirmEmail(username, identifier)
      .subscribe(
        data => {
          this.success = true;
        },
        error => {
          this.success = false;
        });
  }

}
