import { Component, OnInit, OnChanges, Input, HostListener, ElementRef} from '@angular/core';

import { MidiInstrument } from '../midiInstrument';
import { MIDIINSTRUMENTS} from '../list-midiInstrument';
import { User } from '../user';
import { SubscriptionOption } from '../subscriptionOption';
import { SubscriptionUser } from '../subscriptionUser';

@Component({
  selector: 'app-account-informations',
  templateUrl: './account-informations.component.html',
  styleUrls: ['./account-informations.component.css']
})
export class AccountInformationsComponent implements OnInit, OnChanges {

  @Input() currentUser: User;
  @Input() origin: string;
  @Input() subscriptionOption: SubscriptionOption;
  @Input() subscriptionUser: SubscriptionUser;

  who:String;
  instruments:String;
  listInstruments = MIDIINSTRUMENTS;

  constructor() { }

  ngOnInit() {
    
  }

  ngOnChanges() {
    if(this.currentUser) {
      if(this.currentUser.player && this.currentUser.composer) {
        this.who = "Player & Composer";
      }
      else if (this.currentUser.player) {
        this.who = "Player";
      }
      else if (this.currentUser.composer) {
        this.who = "Composer";
      }
      else {
        this.who = "why do you care ? "
      }
  
      this.listInstrumentsFromString();
    }
  }

  listInstrumentsFromString() {

    if(this.currentUser && this.currentUser.instruments) {
      
      let instrument:MidiInstrument;
      let arr = this.currentUser.instruments.split(",");
  
      for (var i = 0; i < arr.length; i++) {
        instrument = this.getInstrumentById(parseInt(arr[i]));
  
        if(instrument) {
          if(this.instruments) {
            this.instruments = this.instruments +", "+instrument.name;
          }
          else {
            this.instruments = instrument.name;
          }
          
        }
      }

      if(!this.instruments) {
        this.instruments = "Nothing yet";
      }
    }
    else {
      this.instruments = "Nothing yet";
    }
  }

  getInstrumentById(id:number):MidiInstrument {
    for (let entry of this.listInstruments) {
      if(entry.id == id) {
        return entry;
      }
    }
  }

}
