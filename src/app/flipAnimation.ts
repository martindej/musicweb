import { trigger, transition, state, style, animate, keyframes } from '@angular/animations';
  
  export const flip = trigger('flip' , [
    state('start', style({ transform: 'rotateY(0)' })),
    state('end', style({ transform: 'rotateY(179.9deg)' })),

    transition('start => end', animate('3s ease-in-out', keyframes([
        style({transform: 'scale(1.05)', offset: 0 }),
        // style({'box-shadow': '0px 0px 10px 3px white', offset: 0 }),
        style({transform: 'rotateY(179.9deg)', offset: 0.4 })
      ])
    )),
    transition('end => start', animate('3s ease-in-out', keyframes([
        style({transform: 'scale(1.05)', offset: 0 }),
        // style({'box-shadow': '0px 0px 10px 3px white', offset: 0 }),
        style({transform: 'rotateY(-179.9deg)', offset: 0.4 })
      ])
    ))
  ]);