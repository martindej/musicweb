import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicDetailsInfoComponent } from './music-details-info.component';

describe('MusicDetailsInfoComponent', () => {
  let component: MusicDetailsInfoComponent;
  let fixture: ComponentFixture<MusicDetailsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicDetailsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicDetailsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
