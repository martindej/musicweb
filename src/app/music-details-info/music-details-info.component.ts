
import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { Subscription, Subject } from 'rxjs';
import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay';
import { ActivatedRoute } from '@angular/router';
// import { BsModalService } from 'ngx-bootstrap/modal';
// import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AuthenticationService } from '../authentication.service';
import { MusicService } from '../music.service';
import { CommentService } from '../comment.service';
import { MusicFile } from '../MusicFile';
import { CommentMusic } from '../CommentMusic';
import { User } from '../user';
import { MusicDetails } from '../musicDetails';
import { YoutubeVideoService } from '../youtube-video.service';

@Component({
  selector: 'app-music-details-info',
  templateUrl: './music-details-info.component.html',
  styleUrls: ['./music-details-info.component.css']
})
export class MusicDetailsInfoComponent implements OnInit, OnDestroy {

  @ViewChild('scoreOriginal') scoreOriginal: ElementRef;
  @ViewChild('scoreModified') scoreModified: ElementRef;
  @ViewChild('textAreaComment') textAreaComment: ElementRef;
  musicDetails: MusicFile;
  private subscriptionMusic: Subscription;

  commentForm: FormGroup;
  submitAttempt = false;
  loading = false;
  loadingDescription = false;
  numberComments = 0;

  description: string;

  descriptionChange = new Subject<string>();
  titleChange = new Subject<string>();

  currentUser: User;
  private subscriptionUser: Subscription;

  commentsMusic: CommentMusic[] = [];
  private subscriptionCommentMusic: Subscription;

  fileUrlMainScore;
  fileUrlModifiedScore;

  random: number;
  selectedFiles: FileList;
  selectedVideoFiles: FileList;
  currentFileUpload: File;
  currentVideoUpload: File;
  progress: { percentage: number } = { percentage: 0 };

  loadingComments: boolean;
  page: number;
  moreVisible = true;
  loaded: boolean;

  numberLikes: number;

  // modalRef: BsModalRef;
  // option:string = 'original';

  constructor(
    private fBuilder: FormBuilder,
    private musicService: MusicService,
    private commentService: CommentService,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private youtubeVideoService: YoutubeVideoService) {

      this.commentForm = fBuilder.group({
        message: fBuilder.control(null, Validators.required)
      });

      this.commentsMusic = commentService.listComments;
      this.subscriptionCommentMusic = commentService.listCommentChange.subscribe((value) => {
        this.commentsMusic = value;
      });

      this.titleChange.pipe(
        debounceTime(2000),
        distinctUntilChanged(),)
        .subscribe((title) => this.changeMusicDetails(title, this.musicDetails.description));

      this.descriptionChange.pipe(
        debounceTime(2000),
        distinctUntilChanged(),)
        .subscribe((description) => this.changeMusicDetails(this.musicDetails.name, description));
    }

  ngOnDestroy() {
    this.subscriptionMusic.unsubscribe();
    this.subscriptionUser.unsubscribe();
    this.subscriptionCommentMusic.unsubscribe();
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.page = 0;
    const id = +this.route.parent.snapshot.paramMap.get('id');

    this.musicDetails = this.musicService.musicDetails;
    this.subscriptionMusic = this.musicService.musicDetailsChange.subscribe((value) => {
      this.musicDetails = value;
      this.getNumberLikes();
      this.getNumberComments();
      this.displaySmallScores();
      this.getCommentsByMusicId(this.musicDetails.id);
      this.createBlobs();
    });

    this.currentUser = this.authenticationService.currentUser;
    this.subscriptionUser = this.authenticationService.userChange.subscribe((value) => {
      this.currentUser = value;
    });

    if (this.musicDetails && this.musicDetails.id === id) {
      this.getNumberLikes();
      this.getNumberComments();
      this.displaySmallScores();
      this.getCommentsByMusicId(this.musicDetails.id);
      this.createBlobs();
    }
  }

  getNumberLikes() {
    this.musicService.getNumberLikesByMusicId(this.musicDetails.id)
      .subscribe(
        data => {
          this.numberLikes = data;
      });
  }

  getNumberComments() {
    this.commentService.getNumberCommentsByMusicId(this.musicDetails.id)
      .subscribe(
        data => {
          this.numberComments = data;
      });
  }

  createBlobs() {
    let blob = new Blob([this.musicDetails.musicXml], { type: 'text/xml' }),
    url = window.URL;
    this.fileUrlMainScore = url.createObjectURL(blob);

    blob = new Blob([this.musicDetails.musicXmlModified], { type: 'text/xml' }),
    this.fileUrlModifiedScore = url.createObjectURL(blob);
  }

  // getUrlScore() {
  //   if(this.option === "original") {
  //     return this.fileUrlMainScore;
  //   }
  //   else if (this.option === "modified") {
  //     return this.fileUrlModifiedScore;
  //   }
  //   else {
  //     return '';
  //   }
  // }

  displaySmallScores() {

    this.displayScore(this.scoreOriginal, this.musicDetails.musicXml);

    if (this.musicDetails.musicXmlModified) {
      this.displayScore(this.scoreModified, this.musicDetails.musicXmlModified);
    }
  }

  displayScore(score: ElementRef, scoreLink: string): void {
    // OSMD = new opensheetmusicdisplay.OSMD(canvas, false, value);
    //         OSMD.setLogLevel('info');

    if (score.nativeElement.firstChild != null) {
      score.nativeElement.removeChild(score.nativeElement.firstChild);
    }

    const osmd = new OpenSheetMusicDisplay(score.nativeElement);
    osmd.load(scoreLink)
    .then(
      () => {
          osmd.zoom = 0.3;
          osmd.render();
      },
      (err) => console.log(err)
    );
  }

  getCommentsByMusicId(music_id: number): void {
    this.loadingComments = true;
    this.commentService.getCommentsByMusicId(music_id, this.page)
      .subscribe(
        data => {
          if (data.length >= 20) {
            this.page++;
          }
          else {
            this.moreVisible = false;
          }
          this.commentService.setAllComments(data);
          this.loadingComments = false;
        }
      );
  }

  addMore(): void {
    this.loadingComments = true;
    this.commentService.getCommentsByMusicId(this.musicDetails.id, this.page)
      .subscribe(
        data => {
          if (data.length >= 20) {
            this.page++;
          }
          else {
            this.moreVisible = false;
          }
          this.commentService.addAllComments(data);
          this.loadingComments = false;
        }
      );
  }

  changeMusicDetails(name: string, description: string) {
    this.loadingDescription = true;
    const musicDetails: MusicDetails = new MusicDetails(name, description, this.currentUser.id, this.musicDetails.id);

    this.musicService.changeMusicDetails(musicDetails)
      .subscribe(
        data => {
          this.loadingDescription = false;
        },
        error => {
          this.loadingDescription = false;
        });
  }

  send() {
    this.submitAttempt = true;
    this.loading = true;

    const commentMusic: CommentMusic = new CommentMusic(0, this.currentUser.id, this.currentUser.username, this.musicDetails.id, Date.now(), this.commentForm.value.message);

    if (!this.commentForm.valid){
        // this.signupSlider.slideTo(0);
    }
    else {
        // User user = new User(0,model.username, model.password, )
        this.commentService.send(commentMusic)
          .subscribe(
            data => {
              this.commentForm.reset();
              // this.commentForm.controls['message'].setValue(" ");
              this.commentsMusic.unshift(commentMusic);
              this.numberComments = this.numberComments + 1;
              this.loading = false;
            },
            error => {
              this.commentForm.reset();
              this.loading = false;
            });
    }
  }

  toggleVisibleOrigin(): void {

    const visible = this.musicDetails.visible_origin;
      this.musicService.toggleVisibleOrigin(this.musicDetails.id, !visible).subscribe(
        res => {
          if (res === true) {
            this.musicService.setMusicVisibilityOrigin(!visible);
          }
        }
      );
  }

  toggleVisibleModified(): void {

    const visible = this.musicDetails.visible_modified;
      this.musicService.toggleVisibleModified(this.musicDetails.id, !visible).subscribe(
        res => {
          if (res === true) {
            this.musicService.setMusicVisibilityModified(!visible);
          }
        }
      );
  }

  toggleVisibleVideo(): void {

    const visible = this.musicDetails.visible_video;
      this.musicService.toggleVisibleVideo(this.musicDetails.id, !visible).subscribe(
        res => {
          if (res === true) {
            this.musicService.setMusicVisibilityVideo(!visible);
          }
        }
      );
  }

  selectFile(event) {
    const file = event.target.files.item(0);

    if (file.type.match('xml.*')) {
      this.selectedFiles = event.target.files;
      this.upload();
    } else {
      alert('invalid format!');
    }
  }

  ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.musicService
      .updateModifiedScore(this.currentFileUpload, this.musicDetails.id)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        }
        else if (event.ok) {
          console.log('File is completely uploaded!');
          const fileReader = new FileReader();
          fileReader.onload = (e) => {
            this.musicDetails.musicXmlModified = this.ab2str(fileReader.result);
            this.displayScore(this.scoreModified, this.musicDetails.musicXmlModified);
            this.selectedFiles = undefined;
          };
          fileReader.readAsText(this.selectedFiles.item(0));

        }
        else {
          this.selectedFiles = undefined;
        }
        this.random = Math.random();
      });
  }

  selectVideo(event) {
    const file = event.target.files.item(0);

    if (file.type.match('video/*')) {
      this.selectedVideoFiles = event.target.files;
      this.uploadVideo();
    } else {
      alert('invalid format!');
    }
  }

  updateVideo(id: string, title: string, description: string, tags: string, categoryId: string, privacyStatus: string) {
    this.youtubeVideoService.updateFile(id, title, description, categoryId, tags, privacyStatus).subscribe(
      data => {
        /* console.log(data); */
      });
  }

  uploadVideo() {

    this.currentVideoUpload = this.selectedVideoFiles.item(0);
    this.youtubeVideoService.uploadFile(this.currentVideoUpload)
      .subscribe( data => {
        if (data.type === HttpEventType.UploadProgress) {
          console.log('progress : ' + data);
        }
        else if (data.id) {
          console.log('File is completely uploaded!');
          console.log('end : ' + event);
          this.musicService.setVideoId(this.musicDetails.id, data.id).subscribe(
            () => {
              this.musicService.setVideoIdLocaly(data.id);
            }
          );
          this.updateVideo(data.id, this.musicDetails.name, this.musicDetails.description, '', '22', 'private');
        }
        else {
          this.selectedVideoFiles = undefined;
        }
      });
  }

  getListVideos() {
    this.youtubeVideoService.getPlaylist();
    // .subscribe(
    //   data => {
    //     console.log(data);
    //   });
  }

  // openModal(template: TemplateRef<any>, option) {
  //   this.option = option;
  //   this.modalRef = this.modalService.show(
  //     template,
  //     Object.assign({}, { class: 'customModal modal-sm' })
  //   );
  // }


}
