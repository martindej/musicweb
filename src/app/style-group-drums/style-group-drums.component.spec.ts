import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleGroupDrumsComponent } from './style-group-drums.component';

describe('StyleGroupDrumsComponent', () => {
  let component: StyleGroupDrumsComponent;
  let fixture: ComponentFixture<StyleGroupDrumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleGroupDrumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleGroupDrumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
