import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { StyleService } from '../style.service';
import { PERCUSSIONMIDIINSTRUMENTS } from '../list-midiPercussionInstruments';
import { AlertService} from '../alert.service';
import { MessageAlert } from '../messageAlert';
import { StyleGroupDrumsFull } from '../styleGroupDrums';
import { StyleGroupDrumsInsFull } from '../styleGroupDrumsIns';
import { RHYTHMS } from '../list-rhythm';

@Component({
  selector: 'app-style-group-drums',
  templateUrl: './style-group-drums.component.html',
  styleUrls: ['./style-group-drums.component.css']
})
export class StyleGroupDrumsComponent implements OnInit {

  @Input() styleGroupDrums: StyleGroupDrumsFull;

  listPercussionInstruments = PERCUSSIONMIDIINSTRUMENTS;
  listRhythm = RHYTHMS;

  submitAttempt: boolean;
  loading: boolean;

  random: number;
  loaded: boolean;

  styleGroupDrumsForm: FormGroup;

  constructor(
    private fBuilder: FormBuilder,
    private styleService: StyleService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {

    this.styleGroupDrumsForm = this.fBuilder.group({
      name: this.fBuilder.control(this.styleGroupDrums.name, [Validators.required]),
      rhythm: this.fBuilder.control(this.styleGroupDrums.rhythm, [Validators.required]),
      pourcentagePlayingTempsFortMin: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTempsFortMin, [Validators.required]),
      pourcentagePlayingTempsFortMax: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTempsFortMax, [Validators.required]),
      pourcentagePlayingTempsFaibleMin: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTempsFaibleMin, [Validators.required]),
      pourcentagePlayingTempsFaibleMax: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTempsFaibleMax, [Validators.required]),
      pourcentagePlayingTemps1Min: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps1Min, [Validators.required]),
      pourcentagePlayingTemps1Max: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps1Max, [Validators.required]),
      pourcentagePlayingTemps2Min: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps2Min, [Validators.required]),
      pourcentagePlayingTemps2Max: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps2Max, [Validators.required]),
      pourcentagePlayingTemps3Min: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps3Min, [Validators.required]),
      pourcentagePlayingTemps3Max: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps3Max, [Validators.required]),
      pourcentagePlayingTemps4Min: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps4Min, [Validators.required]),
      pourcentagePlayingTemps4Max: this.fBuilder.control(this.styleGroupDrums.pourcentagePlayingTemps4Max, [Validators.required])

    });
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;

    if (!this.styleGroupDrumsForm.valid) {
        // this.signupSlider.slideTo(0);
    } else {
        const model: StyleGroupDrumsFull = this.styleGroupDrumsForm.value;
        model.id = this.styleGroupDrums.id;
        model.styleId = this.styleGroupDrums.styleId;
        // model.id = this.styleGroupIns.id;
        // User user = new User(0,model.username, model.password, )
        this.styleService.addNewGroupDrums(model)
          .subscribe(
            data => {
              this.getStyles();
            },
            error => {
              const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

  addGroupDrumsIns() {
    const styleGroupDrumsIns: StyleGroupDrumsInsFull = new StyleGroupDrumsInsFull(0,this.styleGroupDrums.styleId,this.styleGroupDrums.id,[0],"group drums ins",0,0,100,0,100,0,100,0,100,0,100,0,100);
    this.styleGroupDrums.drumsIns.push(styleGroupDrumsIns);
  }

  delete() {
    this.styleService.deleteGroupDrums(this.styleGroupDrums.id, this.styleGroupDrums.styleId)
      .subscribe(
        data => {
          this.getStyles();
        },
        error => {
          const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
          this.alertService.error(message);
        });
  }

  getStyles(): void {
    this.loading = true;
    this.styleService.getStylesMusic().subscribe(
      data => {
        this.styleService.setAllStyles(data);
        this.loading = false;
      }
    );
  }

}
