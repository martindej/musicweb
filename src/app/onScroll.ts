import { trigger,state,style,transition,animate,keyframes, query, stagger,animateChild } from '@angular/animations';
import { slideIn } from './animation';

export const onScroll = trigger('onScroll', [
    state('hide', style({
      opacity: 0,
      
    })),
    state('show',   style({
        opacity: 1
    })),
    
    transition('hide => show', [
        query('@slideIn', [
            stagger(30, [
                animateChild()
            ]),
        ], { optional: true })
    ]),
  ]);