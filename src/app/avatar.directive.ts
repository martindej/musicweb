import {Directive, OnInit, Input, OnDestroy} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders} from '@angular/common/http';

// import {BROWSER_SANITIZATION_PROVIDERS, DomSanitizationService} from '@angular/platform-browser';

@Directive({
  selector: '[appAvatar]',
  host: {
    '[src]': 'sanitizedImageData'
  }
})
export class AvatarDirective implements OnInit, OnDestroy {

  imageData: any;
  sanitizedImageData: any;
  @Input('appAvatar') profileId: any;
  @Input('random') random: number;
  @Input('urlData') urlData: string;

  subscription: Subscription;

  constructor(
    private httpClient: HttpClient,
    private sanitizer: DomSanitizer) { }

  ngOnChanges(changes) {
    this.loadAvatar();
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  ngOnInit() {        
    // this.loadAvatar();
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  loadAvatar() {
    if (this.profileId && this.urlData) {
      this.subscription = this.httpClient.get(this.urlData + this.profileId + "/"+ this.getRandomInt(99999),{ responseType: 'text' } )
      // .map(image => image.text())
      .subscribe(
        data => {
          this.imageData = 'data:image/png;base64,' + data;
          this.sanitizedImageData = this.sanitizer.bypassSecurityTrustResourceUrl(this.imageData);
        }
      );
    }
  }
}