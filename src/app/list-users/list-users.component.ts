import { Component, OnInit , ElementRef , TemplateRef } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { SubscriptionService } from '../subscription.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  allUsers = [];
  allSubscription = [];
  allSubscriptionUser = [];

  numberMusic = new FormControl('');

  currentUser;

  modalRef: BsModalRef;

  constructor(
    private authenticationService:AuthenticationService,
    private subscriptionService:SubscriptionService,
    private modalService: BsModalService) { }

  ngOnInit() {

    this.subscriptionService.getAll().subscribe(
      data => {
        this.allSubscription = data;
        this.authenticationService.getAllUsers().subscribe(
          data => {
            this.allUsers = data;
            this.getAllSubscriptionUser();
          }
        )
      }
    )
    
  }

  openModal(template: TemplateRef<any>, user) {

    this.currentUser = user;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'customModal modal-sm' })
    );
  }

  getAllSubscriptionUser() {
    for (let user of this.allUsers) {
      this.getSubscriptionUserById(user.id);
      this.getNumberMusicsBooster(user.id);
    }
  }

  getSubscriptionUserById(userId: number) {
    this.subscriptionService.getSubscriptionUser(userId).subscribe(
      data => {
        for (let user of this.allUsers) {
          if(user.id === data.userId) {
            user.subscriptionUser = data;

            for(let sub of this.allSubscription) {
              if(user.subscriptionUser.subscriptionId === sub.id) {
                user.subscription = sub;
              }

            }
          }
        }
      }
    )
  }

  getNumberMusicsBooster(user_id: number) {
    this.subscriptionService.getNumberMusicsBooster(user_id).subscribe(
      boo => {
        const data: any = boo;
        if (typeof data === 'number') {
          for (let user of this.allUsers) {
            if(user.id === user_id) {
              user.musicBooster = data;
            }
          }
        }
      });
  }

  getAllSubscriptionUserByUserId(userId: number) {
    this.subscriptionService.getAllSubscriptionUserByUserId(userId).subscribe(
      data => {
        this.allSubscriptionUser = data;
      }
    )
  }

  selectSubscription(sub) {
    this.subscriptionService.setSubscriptionAsAdmin(sub, this.currentUser).subscribe();
  }

  deleteSubUser(subId) {
    this.subscriptionService.deleteSubscriptionUserAsAdmin(subId).subscribe();
  }

  updateNumberMusic() {
    this.subscriptionService.updateMusicBoosterAsAdmin(this.numberMusic.value, this.currentUser.id).subscribe();
  }

}
