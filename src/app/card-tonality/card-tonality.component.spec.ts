import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTonalityComponent } from './card-tonality.component';

describe('CardTonalityComponent', () => {
  let component: CardTonalityComponent;
  let fixture: ComponentFixture<CardTonalityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardTonalityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTonalityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
