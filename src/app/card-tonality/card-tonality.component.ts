import { Component, OnInit, Input } from '@angular/core';
import { TonalityChoice } from '../tonalityChoice';
import { TONALITYCHOICES } from '../listTonalityChoice';

@Component({
  selector: 'app-card-tonality',
  templateUrl: './card-tonality.component.html',
  styleUrls: ['./card-tonality.component.css']
})
export class CardTonalityComponent implements OnInit {

  @Input() tonality: TonalityChoice;
  @Input() forwardSlide: boolean;

  tonalityList: TonalityChoice[] = TONALITYCHOICES;

  allTonVisible: TonalityChoice[];

  constructor() { }

  ngOnInit() {

    const size = this.tonalityList.length;
    const indexOfTon = this.tonalityList.indexOf(this.tonality);

    if (indexOfTon === 0) {
      this.allTonVisible = this.tonalityList.slice(size - 1);
      this.allTonVisible.push(this.tonalityList[0]);
      this.allTonVisible.push(this.tonalityList[1]);
    }
    else if (indexOfTon === size - 1) {
      this.allTonVisible = this.tonalityList.slice(size - 2);
      this.allTonVisible.push(this.tonalityList[0]);
    }
    else {
      this.allTonVisible = this.tonalityList.slice(indexOfTon - 1, indexOfTon + 2);
    }

  }

}
