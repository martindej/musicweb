import { Component, HostListener, ElementRef, ViewChild, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import WaveSurfer from 'wavesurfer.js';
import { OpenSheetMusicDisplay } from 'opensheetmusicdisplay';

import { WORKERS } from '../list-workers';
import { MusicFile } from '../MusicFile';
import { slideIn } from '../animation';
import { onScroll } from '../onScroll';
import { MusicService } from '../music.service';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
  animations: [
    slideIn,
    onScroll
  ]
})

export class MainPageComponent implements OnInit, AfterViewInit {

  listWorkers = WORKERS;
  musicFiles: MusicFile[] = [];
  wavesurfer: WaveSurfer;
  playVisible = true;
  scrollPosition: number;
  randomTopMusic: number;

  reader = new FileReader();

  @ViewChild('musicF') musicF: ElementRef;
  @ViewChild('score') score: ElementRef;

  mFilesState = false;

  ngOnInit() {
    this.randomTopMusic = Math.floor(Math.random() * 8);
    this.getTopMusics();
  }

  ngAfterViewInit() {
    this.loadTestSheetMusic();
    this.initAudio();
    this.loadAudio();
  }

  initAudio() {

    this.wavesurfer = WaveSurfer.create({
      container: '#waveform'
    });

    this.wavesurfer.on('pause', () => {
      this.playVisible = true;
    });

    this.wavesurfer.on('play', () => {
      this.playVisible = false;
    });

  }

  loadTestSheetMusic() {

    this.musicService.loadTestSheetMusic(this.randomTopMusic).subscribe(
      res => {
        // Start reading the blob as text.
        this.reader.readAsText(res);
      }
    );

    // This fires after the blob has been read/loaded.
    this.reader.addEventListener('loadend', (e) => {
      const text: any = e.srcElement;
      const sheet = text.result;
      this.displayMainScore(sheet);
    });
  }

  loadAudio(): void {

    this.musicService.loadTestMusic(this.randomTopMusic).subscribe(
      res => {
        this.wavesurfer.loadBlob(res);
      }
    );
  }

  playAudio(): void {
    this.wavesurfer.playPause();
  }

  getTopMusics(): void {
    this.musicService.getTopMusics()
      .subscribe(
        res => {
          const data: any = res;

          for (const m of data) {
            const music: MusicFile = new MusicFile(
              m.id, m.name,
              m.user_id,
              m.user_name,
              m.visible_origin,
              m.user_id_origin,
              m.user_name_origin,
              m.visible_modified,
              m.user_id_modified,
              m.user_name_modified,
              m.visible_video,
              m.user_id_video,
              m.user_name_video,
              'https://picsum.photos/640/640?image=' + m.pictureNumber,
              m.description,
              m.musicXmlFile,
              m.musicXmlFileModified,
              m.videoId,
              m.tempo,
              m.style,
              m.instruments,
              m.tonality,
              m.pictureNumber,
              m.creationDate);
            this.musicFiles.push(music);
          }
        }
      );
  }

  constructor(
    public el: ElementRef,
    private musicService: MusicService) { }

  @HostListener('window:scroll', ['$event'])
    checkScroll() {
      const musicFPosition = this.musicF.nativeElement.offsetTop;
      this.scrollPosition = window.pageYOffset;

      if (this.scrollPosition + window.innerHeight >= musicFPosition) {
        this.mFilesState = true;
      }

    }

  animate(target) {
    // const scrollPosition = window.pageYOffset;
    const pageHeight = window.innerHeight;
    const componentPosition = target.getBoundingClientRect().top;

    if ((componentPosition > 100 ) && (componentPosition < (pageHeight - 100))) {
      return true;
    }
    else {
      return false;
    }
  }

  opacity(target) {
    // const scrollPosition = window.pageYOffset;
    const pageHeight = window.innerHeight;
    const componentPosition = target.getBoundingClientRect().top;

    if ((componentPosition > 100 ) && (componentPosition < (pageHeight - 100))) {
      return 1;
    }
    else {
      return 0;
    }
  }

  displayMainScore(scoreLink: string): void {
    if (this.score.nativeElement.firstChild != null) {
      this.score.nativeElement.removeChild(this.score.nativeElement.firstChild);
    }

    //   context.clearRect(0, 0, canvas.width, canvas.height);
    const osmd = new OpenSheetMusicDisplay(this.score.nativeElement);
      osmd.load(scoreLink)
      .then(
        () => {
          osmd.zoom = 0.35;
          osmd.render();
          // osmd.cursor.show();
        },
        (err) => console.log(err)
      );
  }
}
