export class CommentMusic {

    id: number;
    user_id: number;
    user_name:string;
    music_id: number;
    date: number;
    message: string;
  
    constructor(id: number, user_id: number, user_name:string, music_id: number, date: number, message:string){
      this.id = id;
      this.user_id = user_id;
      this.user_name = user_name;
      this.music_id = music_id;
      this.date = date;
      this.message = message;
    }
  }