export class MusicFile {
  id: number;
  name:string;
  description:string;
  user_id:number;
  user_name:string;

  visible_origin:boolean;
  user_id_origin:number;
  user_name_origin:string;

  visible_modified:boolean;
  user_id_modified:number;
  user_name_modified:string;

  visible_video:boolean;
  user_id_video:number;
  user_name_video:string;

  image:string;
  musicXml:string;
  musicXmlModified:string;
  videoId:string;

  tempo:number;
  style:string;
  instruments:string;

  tonality:string;

  creationDate:number;

  picture_number:number;

  constructor(
    id: number, 
    name: string, 
    user_id:number, 
    user_name:string,
    visible_origin:boolean,
    user_id_origin:number,
    user_name_origin:string,
    visible_modified:boolean,
    user_id_modified:number,
    user_name_modified:string,
    visible_video:boolean,
    user_id_video:number,
    user_name_video:string,
    image: string, 
    description: string, 
    musicXml:string, 
    musicXmlModified:string, 
    videoId:string, 
    tempo:number, 
    style:string, 
    instruments:string, 
    tonality:string, 
    picture_number:number, 
    creationDate:number){

      this.id = id;
      this.name = name;
      this.user_id = user_id;
      this.user_name = user_name;
      this.visible_origin = visible_origin,
      this.user_id_origin = user_id_origin,
      this.user_name_origin = user_name_origin,
      this.visible_modified = visible_modified,
      this.user_id_modified = user_id_modified,
      this.user_name_modified = user_name_modified,
      this.visible_video = visible_video,
      this.user_id_video = user_id_video,
      this.user_name_video = user_name_video,
      this.image = image;
      this.description = description;
      this.musicXml = musicXml;
      this.musicXmlModified = musicXmlModified;
      this.videoId = videoId;
      this.tempo = tempo;
      this.style = style;
      this.instruments = instruments;
      this.tonality = tonality;
      this.picture_number = picture_number;
      this.creationDate = creationDate;
  }
}