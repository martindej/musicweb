import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { AlertService } from '../alert.service';
import { MessageAlert } from '../messageAlert';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  @ViewChild('username') el:ElementRef;
  statuslogin:any;
  focusin: boolean = true;
  rForm: FormGroup;
  post:any;  
  usernameAlert:string="Please fill username";
  passwordAlert:string="Please fill password";
  loginAlert:string;
  loginError:boolean=false;
  returnUrl: string;
  loading:boolean;
  noEmail:boolean = false;
  model: any = {};
  userForm: FormGroup;
  submitAttempt:boolean;


  constructor(
      private route: ActivatedRoute,
      private authenticationService:AuthenticationService,    
      public router: Router,
      private fBuilder:FormBuilder,
      private alertService: AlertService
    ) {
      this.userForm = fBuilder.group({
        username: fBuilder.control(null, [Validators.required]),
        password: fBuilder.control(null, Validators.required)
      });
  }
  
  ngOnInit() {
    window.scroll(0, 0);
    this.authenticationService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
  } 

  login() {

    this.loading = true;

    if(!this.userForm.valid){
      // this.signupSlider.slideTo(0);
    }
    else {
      this.authenticationService.token(this.userForm.value.username, this.userForm.value.password)
      .subscribe(
        data => {

          if(data.token !== "email") {
            this.authenticationService.login(this.userForm.value.username)
            .subscribe(
              data => {
                this.router.navigateByUrl(this.returnUrl);
              },
              error => {
                let message:MessageAlert = new MessageAlert("incorrect username and/or password", 5000, true, 'danger');
                this.alertService.error(message);
                this.loading = false;
              });
          }
          else {
            this.noEmail = true;
            this.loading = false;
          }
          
        },
        error => {
          console.log(error);
          let message:MessageAlert = new MessageAlert("incorrect username and/or password", 5000, true, 'danger');
          this.alertService.error(message);
          this.loading = false;
        });
    }
  }

}