export class Card {
  id: number;
  title: string;
  image: string;
  text: string;
  resume: string;
}
