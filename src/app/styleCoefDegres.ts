export class StyleCoefDegres {

    id: number;

	c11: number;
	c12: number;
	c13: number;
	c14: number;
	c15: number;
	c16: number;
	c17: number;

	c21: number;
	c22: number;
	c23: number;
	c24: number;
	c25: number;
	c26: number;
	c27: number;

	c31: number;
	c32: number;
	c33: number;
	c34: number;
	c35: number;
	c36: number;
	c37: number;

	c41: number;
	c42: number;
	c43: number;
	c44: number;
	c45: number;
	c46: number;
	c47: number;

	c51: number;
	c52: number;
	c53: number;
	c54: number;
	c55: number;
	c56: number;
	c57: number;

	c61: number;
	c62: number;
	c63: number;
	c64: number;
	c65: number;
	c66: number;
	c67: number;

	c71: number;
	c72: number;
	c73: number;
	c74: number;
	c75: number;
	c76: number;
	c77: number;

	styleId: number;
    coefDegre: string;

    constructor(id: number, c11: number, c12: number, c13: number, c14: number, c15: number, c16: number, c17: number, c21: number, c22: number, c23: number,
        c24: number, c25: number, c26: number, c27: number, c31: number, c32: number, c33: number, c34: number, c35: number, c36: number, c37: number, c41: number,
        c42: number, c43: number, c44: number, c45: number, c46: number, c47: number, c51: number, c52: number, c53: number, c54: number, c55: number, c56: number,
        c57: number, c61: number, c62: number, c63: number, c64: number, c65: number, c66: number, c67: number, c71: number, c72: number, c73: number, c74: number,
        c75: number, c76: number, c77: number, styleId: number, coefDegre: string) {

        this.id = id;
        this.c11 = c11;
        this.c12 = c12;
        this.c13 = c13;
        this.c14 = c14;
        this.c15 = c15;
        this.c16 = c16;
        this.c17 = c17;
        this.c21 = c21;
        this.c22 = c22;
        this.c23 = c23;
        this.c24 = c24;
        this.c25 = c25;
        this.c26 = c26;
        this.c27 = c27;
        this.c31 = c31;
        this.c32 = c32;
        this.c33 = c33;
        this.c34 = c34;
        this.c35 = c35;
        this.c36 = c36;
        this.c37 = c37;
        this.c41 = c41;
        this.c42 = c42;
        this.c43 = c43;
        this.c44 = c44;
        this.c45 = c45;
        this.c46 = c46;
        this.c47 = c47;
        this.c51 = c51;
        this.c52 = c52;
        this.c53 = c53;
        this.c54 = c54;
        this.c55 = c55;
        this.c56 = c56;
        this.c57 = c57;
        this.c61 = c61;
        this.c62 = c62;
        this.c63 = c63;
        this.c64 = c64;
        this.c65 = c65;
        this.c66 = c66;
        this.c67 = c67;
        this.c71 = c71;
        this.c72 = c72;
        this.c73 = c73;
        this.c74 = c74;
        this.c75 = c75;
        this.c76 = c76;
        this.c77 = c77;
        this.styleId = styleId;
        this.coefDegre = coefDegre;
    }
}
