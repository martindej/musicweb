import { Component, OnInit, OnChanges, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { User } from "../user";
import { AuthenticationService } from '../authentication.service';
import { SubscriptionService } from '../subscription.service';
import { AvatarDirective } from '../avatar.directive';
import { SubscriptionOption } from '../subscriptionOption';
import { Booster } from '../booster';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit, OnDestroy {

  currentUser: User = new User(0,"","","",false,null, false, false,"",0,false, true);
  private subscription:Subscription;

  listUsersFollowed: User[] = [];
  private subscriptionUsersFollowed:Subscription;

  listBooster:Booster[] = [];
  private subscriptionListBooster:Subscription;

  listSubscriptionOption: SubscriptionOption[] = [];
  private subscriptionListSubscriptionOption: Subscription;

  subOptionId: number;
  private subscriptionSubOptionId: Subscription;

  numberMusicsSubOption: number;
  private subscriptionNumberMusicsSubOption: Subscription;

  orderId: string;
  // booster: Booster;
  // private subscriptionBooster: Subscription;
  
  numberMusicsBooster: number;
  private subscriptionNumberMusicsBooster: Subscription;


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private subscriptionService: SubscriptionService) { }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
    if(this.subscriptionUsersFollowed) {
      this.subscriptionUsersFollowed.unsubscribe();
    }
    if(this.subscriptionListSubscriptionOption) {
      this.subscriptionListSubscriptionOption.unsubscribe();
    }

    this.subscriptionSubOptionId.unsubscribe();
    this.subscriptionNumberMusicsSubOption.unsubscribe();
    this.subscriptionNumberMusicsBooster.unsubscribe();
    this.subscriptionListBooster.unsubscribe();
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.currentUser = this.authenticationService.currentUser;
    this.subscription = this.authenticationService.userChange.subscribe((value) => { 
      this.currentUser = value; 
    });

    this.getOrderId();
  }

  getSubscriptionsOption() {
    this.subscriptionListSubscriptionOption = this.subscriptionService.getAll().subscribe(
      (subscriptionOptionList) => { 
        for(let m of subscriptionOptionList) {
          let sub:SubscriptionOption = m;
          if(sub.pricePerMonth !== 0) {
            this.listSubscriptionOption.push(sub);
          }
        }
        this.sortSubscriptionOptions();
      });
  }

  getAllBooster() {
    this.subscriptionListBooster = this.subscriptionService.getAllBooster().subscribe(
      data => { 
        for(let m of data) {
          let boo:Booster = m;
          if(boo.price > 0) {
            this.listBooster.push(boo);
          }
        }
        this.sortBooster();
      });
  }

  sortBooster() {
    this.listBooster.sort((a, b) => {
      if (a.price < b.price) return -1;
      else if (a.price > b.price) return 1;
      else return 0;
    });
  }

  getOrderId() {
    this.orderId = "";
    this.subscriptionService.getOrderIdIncremented().subscribe(
      data => {
        this.orderId = ("00000" + data).slice(-6);
        this.getSubscriptionsOption();
        this.getAllBooster();
        this.getNumberMusicsBooster();
        this.getSubOption();
      });
  }

  sortSubscriptionOptions() {
    this.listSubscriptionOption.sort((a, b) => {
      if (a.pricePerYear < b.pricePerYear) return -1;
      else if (a.pricePerYear > b.pricePerYear) return 1;
      else return 0;
    });
  }

  getSubOption() {
    this.subscriptionSubOptionId = this.subscriptionService.getSubscriptionOptionByUserId(this.currentUser.id).subscribe(
      sub => { 
        this.subOptionId = sub;
        this.getNumberMusicsSubscription(this.subOptionId);
      });
  }

  getNumberMusicsSubscription(sub_id:number) {
    this.subscriptionNumberMusicsSubOption = this.subscriptionService.getNumberMusicsSubscription(this.currentUser.id).subscribe(
      sub => { 
        let data:any = sub;
        if(data) {
          this.numberMusicsSubOption = data;
        }
        
      });
  }

  // getBooster() {
  //   this.subscriptionBooster = this.subscriptionService.getBooster(this.currentUser.id).subscribe(
  //     boo => { 
  //       this.booster = boo;
  //     });
  // }

  getNumberMusicsBooster() {
    this.subscriptionNumberMusicsBooster = this.subscriptionService.getNumberMusicsBooster(this.currentUser.id).subscribe(
      boo => { 
        let data:any = boo;
        if(data) {
          this.numberMusicsBooster = data;
        }
      });
  }

}
