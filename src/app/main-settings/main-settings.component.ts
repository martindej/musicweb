import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';

import { StyleMusicFull } from '../styleMusicFull';
import { MusicService } from '../music.service';
import { AlertService } from '../alert.service';
import { ComposeService } from '../compose.service';
import { MessageAlert } from '../messageAlert';
import { StyleService } from '../style.service';
import { MusicParameters } from '../musicParameters';

@Component({
  selector: 'app-main-settings',
  templateUrl: './main-settings.component.html',
  styleUrls: ['./main-settings.component.css']
})
export class MainSettingsComponent implements OnInit, OnDestroy {

  stylesMusic: StyleMusicFull[] = [];
  private subscriptionStyleMusic;

  style: number;
  private subscriptionStyle;

  musicParameters: MusicParameters;
  private subscriptionMusicParameters;

  loading: boolean;

  private subscriptionRandomRefresh;

  constructor(
    private musicService: MusicService,
    private alertService: AlertService,
    private composeService: ComposeService,
    private styleService: StyleService
  ) {}

  ngOnInit() {

    this.loading = false;

    this.stylesMusic = this.styleService.listStyles;
    if (this.stylesMusic.length === 0) {
      this.getListStyles();
    }

    this.style = this.composeService.currentStyle;
    this.subscriptionStyle = this.composeService.style.subscribe((value) => {
      this.style = value;
    });

    this.subscriptionStyleMusic = this.styleService.listStyleChange.subscribe((value) => {
      this.stylesMusic = value;
      this.chooseStyle(this.stylesMusic[0].id);
    });

    this.subscriptionMusicParameters = this.composeService.currentMusicParameters.subscribe(
      musicParameters => {
        this.musicParameters = musicParameters;
    });

    this.subscriptionRandomRefresh = this.composeService.randomRefreshCompose.subscribe(
      random => {
        this.chooseStyle(this.style);
    });
  }

  ngOnDestroy() {
    this.subscriptionMusicParameters.unsubscribe();
    this.subscriptionRandomRefresh.unsubscribe();
    this.subscriptionStyleMusic.unsubscribe();
    this.subscriptionStyle.unsubscribe();
  }

  getListStyles() {

    this.styleService.getStylesMusic()
        .subscribe(
          data => {
            this.styleService.setAllStyles(data);
            // this.stylesMusic = data;
          },
          error => {
            const message: MessageAlert = new MessageAlert('Error fetching list of styles', 5000, true, 'danger');
            this.alertService.success(message, true);
        });
  }

  chooseStyle(styleId: number) {

    if (!this.loading) {
      this.loading = true;
      this.composeService.getMusicParameters(styleId).subscribe(
        data => {
          this.composeService.setCurrentStyle(styleId);
          this.composeService.setMusicParameters(data);
          this.loading = false;
        },
        error => {
          const message: MessageAlert = new MessageAlert('Error fetching list of styles', 5000, true, 'danger');
          this.alertService.success(message, true);
          this.loading = false;
        });
    }
  }

}
