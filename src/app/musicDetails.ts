export class MusicDetails {

    title: string;
    description: string;
    user_id: number;
    music_id:number;
  
    constructor(title:string, description:string, user_id:number, music_id:number){
      this.description = description;
      this.title = title;
      this.user_id = user_id;
      this.music_id = music_id;
    }
  }