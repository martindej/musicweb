export class MessageAlert {

    text: string;
    timeout: number;
    dismissible: boolean;
    type:string;
  
    constructor(text: string, timeout: number, dismissible: boolean, type:string){
      this.text = text;
      this.timeout = timeout;
      this.dismissible = dismissible;
      this.type = type; 
    }
}