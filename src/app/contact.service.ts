import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Message } from "./message";
import { Http, Response, RequestOptions, Headers } from "@angular/http";



@Injectable()
export class ContactService {

  private apiUrl = '/api/contact';

  constructor(private httpClient: HttpClient,private http: Http) { }

  send(message: Message): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.apiUrl + '/send', message, options);
  }

}

