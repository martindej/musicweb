import { Component, OnInit, Input, ElementRef} from '@angular/core';
import { Card } from '../card';
import { slideIn } from '../animation';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  animations: [
    slideIn
  ]
})

export class CardComponent implements OnInit {

  @Input() card: Card;

  // state = 'hide';
  // visibility:string = 'visibleRed';

  constructor(public el: ElementRef) { }

  ngOnInit() {
  }

  // changeStyle($event){
  //   this.visibility = $event.type == 'mouseover' ? 'visibleWhite' : 'visibleRed';
  // }

}
