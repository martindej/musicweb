import {map, shareReplay, tap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { User } from "./user";
import { Http, Headers } from "@angular/http";

import * as moment from 'moment';
 
@Injectable()
export class AuthenticationService {

  private apiUrl = '/api/users';
  currentUser:User = null;
  userChange: Subject<User> = new Subject<User>();

  constructor(private httpClient: HttpClient, private http: Http, private router:Router) { }

  pushFileToStorage(file: File): Observable<any> {

    const token = localStorage.getItem("auth_token");
    var headers = new Headers();
    headers.append('Authorization', 'Bearer ' + token);
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    const url = this.apiUrl + '/upload/' + this.currentUser.username + '/' + this.currentUser.id; 
    return this.http.post(url, formdata, {headers:headers}); // {headers}
  }

  initialize() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  setSession(authResult) {

    if(authResult.token !== "email") {
      localStorage.setItem('auth_token', authResult.token);
      localStorage.setItem('auth_token_refresh', authResult.refresh_token);
      localStorage.setItem('expiration_date', authResult.expiration_date);
    }
    
  }

  deleteUser(id): Observable<any>  {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/deleteUserById/' + id, {headers: headers})
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUser = null;
    this.userChange.next(this.currentUser);
    localStorage.removeItem("expiration_date");
    localStorage.removeItem('auth_token');
    localStorage.removeItem('auth_token_refresh');
  }

  backToMainPage() {
    this.router.navigate(['/mainPage']);
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem("expiration_date");
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }  
  
  refreshToken(): Observable<any> {

    const refreshtoken = localStorage.getItem("auth_token_refresh");
    const token = localStorage.getItem("auth_token");
    const expiration = localStorage.getItem("expiration_date");
    
    let authToken = {
      token:token,
      refresh_token:refreshtoken,
      expiration_date:expiration
    }

    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = this.apiUrl + '/refreshtoken'; 
    return this.httpClient.post(url, authToken, {headers:headers}); // {headers}
  }

  token(username:String, password:String): Observable<any> {

    let userLogin = {
      username:username,
      password:password
    }
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient
      .post(this.apiUrl + '/token', userLogin, {headers: headers}).pipe(
      tap(
        data => {
          this.setSession(data);
        }
      ),
      shareReplay(),);

    // We are calling shareReplay to prevent the receiver of this Observable from accidentally triggering multiple POST requests due to multiple subscriptions.
  }

  login(username:string) {
    const getLoginUrl = this.apiUrl + '/login/' + username;
    return this.httpClient
      .get(getLoginUrl, {}).pipe(
      map( res => {

        let data:any = res;
        this.currentUser = data;
        this.userChange.next(this.currentUser);
        localStorage.setItem('currentUser', JSON.stringify(data));
        return data;
      },
      err => {
        return err;
      }
      ))
  }

  

  toggleFollowUser(id:number): Observable<any> {
    const followUrl = this.apiUrl + '/toggleFollow/' + this.currentUser.id +'/'+ id;
    return this.httpClient.post(followUrl, {});
  }

  getNumberFollowers(id:number):Observable<any> {
    const followUrl = this.apiUrl + '/numberFollowers/'+ id;
    return this.httpClient.get(followUrl, {});
  }

  getNumberLikes(id:number):Observable<any> {
    const followUrl = this.apiUrl + '/numberLikes/'+ id;
    return this.httpClient.get(followUrl, {});
  }

  getNumberCompositions(id:number):Observable<any> {
    const followUrl = this.apiUrl + '/numberCompositions/'+ id;
    return this.httpClient.get(followUrl, {});
  }

  following(id:number): Observable<any> {
    const followUrl = this.apiUrl + '/following/' + this.currentUser.id +'/'+ id;
    return this.httpClient.get(followUrl, {});
  }

  getListUsersFollowed(id:number): Observable<any> {
    const followUrl = this.apiUrl + '/listUsersFollowed/' + id;
    return this.httpClient.get(followUrl, {});
  }

  getUserById(id:number): Observable<any> {
    const getLoginUrl = this.apiUrl + '/getUserById/' + id;
    return this.httpClient.get(getLoginUrl, {});
  }

  confirmEmail(username:String, identifier:String):Observable<any> {
    const getLoginUrl = this.apiUrl + '/confirmEmail/' + username + '/' + identifier;
    return this.httpClient.post(getLoginUrl, {})
  }

  create(user: User): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/create', user, {headers});
  }

  resetPassword(email: string): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/resetPassword/', email, {headers});
  }

  updateUser(user: User): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/updateUser', user, {headers});
  }

  changePassword(id:number, password:String, newPassword:String) {
    let userLogin = {
      username:this.currentUser.username,
      password:password,
      newPassword:newPassword
    }

    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient
      .post(this.apiUrl + '/changePassword', userLogin, {headers}).pipe(
      shareReplay());
  }

  validateEmail(email:string): Observable<any> {  
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.apiUrl + '/validateEmail/',email, {headers});
  }

  validateUsername(username:string): Observable<any> {
    return this.httpClient.get(this.apiUrl + '/validateUsername/' + username);
  }

  getAllUsers(): Observable<any>  {
    return this.httpClient.get(this.apiUrl + '/all/' + this.currentUser.id);
  }
}
