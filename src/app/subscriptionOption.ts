export class SubscriptionOption {

  id: number;
  name: string;
  musicsPerDay: number;
  pricePerMonth: number;
  pricePerYear: number;
  generalSettings: boolean;
  advancedSettings: boolean;
  melodySettings: boolean;
  durationInDays: number;

  constructor(id: number, name: string, musicsPerDay: number, pricePerMonth: number, pricePerYear:number, generalSettings: boolean, advancedSettings: boolean, melodySettings: boolean, durationInDays:number){
    this.id = id;
    this.name = name;
    this.musicsPerDay = musicsPerDay;
    this.pricePerMonth = pricePerMonth;
    this.pricePerYear = pricePerYear;
    this.generalSettings = generalSettings;
    this.advancedSettings = advancedSettings;
    this.melodySettings = melodySettings;
    this.durationInDays = durationInDays;
  }
}
