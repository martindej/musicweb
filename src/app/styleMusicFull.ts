import { StyleGroupInstrumentsFull } from './styleGroupInstruments';
import { StyleGroupDrumsFull } from './styleGroupDrums';
import { MesureTempsFort } from './mesureTempsFort';
import { StyleCoefDegres } from './styleCoefDegres';

export class StyleMusicFull {

  id: number;
  styleName: string;
  availableTon: number[];
  rythmeMax: number;
  rythmeMin: number;
  nInstrumentMin: number;
  nInstrumentMax: number;
  nPercussionMin: number;
  nPercussionMax: number;
  complexityChordMin: number;
  complexityChordMax: number;
  numberChordMin: number;
  numberChordMax: number;
  soundFont: string;
  mesureTempsForts: MesureTempsFort[];
  styleCoefDegres: StyleCoefDegres[];
  styleGroupInstruments: StyleGroupInstrumentsFull[];
  styleGroupDrums: StyleGroupDrumsFull[];

  publish: boolean;

  constructor(id: number,
      styleName: string,
      availableTon: number[],
      rythmeMax: number,
      rythmeMin: number,
      nInstrumentMin: number,
      nInstrumentMax: number,
      nPercussionMin: number,
      nPercussionMax: number,
      complexityChordMin: number,
      complexityChordMax: number,
      numberChordMin: number,
      numberChordMax: number,
      soundFont: string,
      publish: boolean) {

    this.id = id;
    this.styleName = styleName;
    this.availableTon = availableTon;
    this.rythmeMax = rythmeMax;
    this.rythmeMin = rythmeMin;
    this.nInstrumentMin = nInstrumentMin;
    this.nInstrumentMax = nInstrumentMax;
    this.nPercussionMin = nPercussionMin;
    this.nPercussionMax = nPercussionMax;
    this.complexityChordMin = complexityChordMin;
    this.complexityChordMax = complexityChordMax;
    this.numberChordMin = numberChordMin;
    this.numberChordMax = numberChordMax;
    this.publish = publish;
    this.soundFont = soundFont;
    this.styleCoefDegres = [];
    this.mesureTempsForts = [];
    this.styleGroupInstruments = [];
    this.styleGroupDrums = [];
  }
}
