export class StyleMusic {
    id: number;
    title: string;
    icon: string;
    image: string;
    text: string;
    link: string;

    constructor(id: number, title: string, icon: string, image: string, text: string, link: string){
      this.id = id;
      this.title = title;
      this.icon = icon;
      this.image = image;
      this.text = text;
      this.link = link;
    }
  }
  