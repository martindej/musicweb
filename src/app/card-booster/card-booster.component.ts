import { Component, OnInit, Input } from '@angular/core';

import { Booster } from '../booster';

@Component({
  selector: 'app-card-booster',
  templateUrl: './card-booster.component.html',
  styleUrls: ['./card-booster.component.css']
})
export class CardBoosterComponent implements OnInit {

  @Input() booster:Booster;
  @Input() selected:boolean;
  @Input() numberMusics:number;
  @Input() account:boolean;

  constructor() { }

  ngOnInit() {
  }

}
