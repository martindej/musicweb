import { Component, OnInit, OnDestroy } from '@angular/core';
import { MidiInstrument } from '../midiInstrument';
import { MIDIINSTRUMENTS } from '../list-midiInstrument';
import { ComposeService } from '../compose.service';
import { TonalityChoice } from '../tonalityChoice';
import { TONALITYCHOICES } from '../listTonalityChoice';
import { MusicParameters } from '../musicParameters';

@Component({
  selector: 'app-instruments-settings',
  templateUrl: './instruments-settings.component.html',
  styleUrls: ['./instruments-settings.component.css']
})
export class InstrumentsSettingsComponent implements OnInit, OnDestroy {

  selectedInstrument: MidiInstrument = {id: -1, bassPart: false, name: '-- Choose in the list --'};
  defaultInstrument: MidiInstrument = {id: -1, bassPart: false, name: '-- Choose in the list --'};
  listInstruments = MIDIINSTRUMENTS;
  listSelectedInstruments: Array<MidiInstrument> = [];
  private subscriptionListInstruments;
  openBoolean = false;

  previousSlideNumber = 0;
  currentSlideNumber: number;
  forwardSlide = true;

  musicParameters: MusicParameters;
  private subscriptionMusicParameters;

  tonalityList: TonalityChoice[] = TONALITYCHOICES;
  // majorMinor = 'minor';
  isChecked: boolean;
  percussion = 'No';

  private subscriptionRandomRefresh;

  constructor(
    private composeService: ComposeService) { }

  ngOnInit() {

    this.openBoolean = false;

    this.subscriptionListInstruments = this.composeService.currentListInstruments.subscribe(
      listSelectedInstruments => {
        this.listSelectedInstruments = listSelectedInstruments;
    });

    this.subscriptionMusicParameters = this.composeService.currentMusicParameters.subscribe(
      musicParameters => {
        this.musicParameters = musicParameters;
        if (musicParameters.tonality.indexOf('major') !== -1) {
          this.isChecked = true;
        }
        else {
          this.isChecked = false;
        }
    });

    // this.subscriptionRandomRefresh = this.composeService.randomRefreshCompose.subscribe(
    //   random => {
    //     this.chooseStyle();
    // });
  }

  ngOnDestroy() {
    this.subscriptionMusicParameters.unsubscribe();
    // this.subscriptionRandomRefresh.unsubscribe();
    this.subscriptionListInstruments.unsubscribe();
  }

  nextOrPrevious(event: number) {

    this.previousSlideNumber = this.currentSlideNumber;
    this.currentSlideNumber = event;

    this.forwardSlide = (this.currentSlideNumber > this.previousSlideNumber) || (this.previousSlideNumber - this.currentSlideNumber > 2);
  }

  addToList(instrument: MidiInstrument): void {
    let numberIns = 0;

    for (const ins of this.listSelectedInstruments) {
      if (ins.bassPart) {
        numberIns = numberIns + 2;
      }
      else {
        numberIns++;
      }
    }

    if (numberIns < 7 && instrument.bassPart === false || numberIns < 6 && instrument.bassPart) {
      this.composeService.addToCurrentListInstruments(instrument);
    }

    this.selectedInstrument = {id: -1, bassPart: false, name: '-- Choose in the list --'};
  }

  getInstrumentById(id: number) {
    for (const entry of this.listInstruments) {
      if (entry.id === id) {
        return entry;
      }
    }
  }

  deleteFromList(instrument: MidiInstrument): void {
    this.composeService.removeFromCurrentListInstruments(instrument);
  }

  compareFn(c1: MidiInstrument, c2: MidiInstrument): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  chooseTonality(tonality) {
    this.composeService.setTonality(tonality);
  }

  setPercussion() {
    this.composeService.setPercussion(this.musicParameters.percussion);
  }

  // chooseStyle() {
  //   this.composeService.getMusicParameters(0).subscribe(
  //     data => {
  //       this.composeService.setMusicParameters(data);
  //     });
  // }

  getPrevious(ton: TonalityChoice): TonalityChoice {

    const size = this.tonalityList.length;
    const indexOfTon = this.tonalityList.indexOf(ton);

    if (indexOfTon === 0) {
      return this.tonalityList[size - 1];
    }
    else {
      return this.tonalityList[indexOfTon - 1];
    }
  }

  getNext(ton: TonalityChoice): TonalityChoice {

    const size = this.tonalityList.length;
    const indexOfTon = this.tonalityList.indexOf(ton);

    if (indexOfTon === size - 1) {
      return this.tonalityList[0];
    }
    else {
      return this.tonalityList[indexOfTon + 1];
    }
  }

}
