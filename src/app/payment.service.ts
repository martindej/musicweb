import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, Subscription, Subject } from 'rxjs';


@Injectable()
export class PaymentService {

	private apiUrl = '/api/payment';

  constructor(private httpClient:HttpClient) { }

  pay(paymentInfos) {
  	// return this.httpClient.post(this.apiUrl + "/pay",paymentInfos);

  	let url = "https://paiement.systempay.fr/vads-payment/"; +
  				"&" + "vads_action_mode" + "="+paymentInfos.vads_action_mode
  				"&" + "vads_amount" + "="+paymentInfos.vads_amount
  				"&" + "vads_ctx_mode" + "="+paymentInfos.vads_ctx_mode
  				"&" + "vads_currency" + "="+paymentInfos.vads_currency
  				"&" + "vads_cust_email" + "="+paymentInfos.vads_cust_email
  				"&" + "vads_cust_id" + "="+paymentInfos.vads_cust_id
  				"&" + "vads_order_id" + "="+paymentInfos.vads_order_id
  				"&" + "vads_page_action" + "="+paymentInfos.vads_page_action
  				"&" + "vads_payment_config" + "="+paymentInfos.vads_payment_config
  				"&" + "vads_site_id" + "="+paymentInfos.vads_site_id
  				"&" + "vads_trans_date" + "="+paymentInfos.vads_trans_date
  				"&" + "vads_trans_id" + "="+paymentInfos.vads_trans_id
  				"&" + "vads_version" + "="+paymentInfos.vads_version
  				"&" + "signature" + "="+paymentInfos.signature;

  	document.location.href = url;
  	// return this.httpClient.post("https://paiement.systempay.fr/vads-payment/",paymentInfos);
  }

  
}
