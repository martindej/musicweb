import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleFactoryComponent } from './style-factory.component';

describe('StyleFactoryComponent', () => {
  let component: StyleFactoryComponent;
  let fixture: ComponentFixture<StyleFactoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleFactoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleFactoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
