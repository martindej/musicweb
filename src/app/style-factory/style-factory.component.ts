import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';

import { StyleMusicFull } from '../styleMusicFull';
import { StyleService } from '../style.service';
import { TONALITYCHOICES } from '../listTonalityChoice';
import { AlertService } from '../alert.service';
import { MessageAlert } from '../messageAlert';
import { StyleGroupInstrumentsFull } from '../styleGroupInstruments';
import { StyleGroupDrumsFull } from '../styleGroupDrums';
import { MesureTempsFort } from '../mesureTempsFort';
import { StyleCoefDegres } from '../styleCoefDegres';


@Component({
  selector: 'app-style-factory',
  templateUrl: './style-factory.component.html',
  styleUrls: ['./style-factory.component.css']
})
export class StyleFactoryComponent implements OnInit, OnDestroy {

  listStyles: StyleMusicFull[] = [];
  private subscriptionStyles: Subscription;

  listTonalities = TONALITYCHOICES;

  listSoundFont: string[];

  submitAttempt: boolean;
  loading: boolean;

  random: number;
  loaded: boolean;
  numberInsGroup = 1;

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };

  styleForm: FormGroup;
  selectedStyleMusic: StyleMusicFull = new StyleMusicFull(0, 'new', [8, 6], 200, 50, 1, 8, 0, 1, 1, 3, 3, 8, 'Timbres Of Heaven GM_GS_XG_SFX V 3.4 Final.sf2', false);

  constructor(
    private fBuilder: FormBuilder,
    private styleService: StyleService,
    private alertService: AlertService) {
    this.createForm();

    this.subscriptionStyles = this.styleService.listStyleChange.subscribe((value) => {
      this.listStyles = value;
    });

    this.getStyles();
    this.getSoundFont();
  }

  addGroupIns() {
    const styleGroupIns: StyleGroupInstrumentsFull = new StyleGroupInstrumentsFull(0, this.selectedStyleMusic.id, 'group instrument', 0, [0], 0, 100, 0, 100, 0, 100, 0, 9, 3, 6, 0, 100, 300, 40, 0, 100, 0, 100, 0, 100, 0, 100, 0, 100, 0, 100, 0, 100, 0, 100, 1, 4, 1, 100, 3, 6, 1, 6,0,
    100,
    80,
    100,
    50,
    80,
    0,
    100);
    this.selectedStyleMusic.styleGroupInstruments.push(styleGroupIns);
  }

  addGroupDrums() {
    const styleGroupDrums: StyleGroupDrumsFull = new StyleGroupDrumsFull(0,this.selectedStyleMusic.id,"style group drums",0,0,100,0,100,0,100,0,100,0,100,0,100);
    this.selectedStyleMusic.styleGroupDrums.push(styleGroupDrums);
  }

  addMesureTempsFort() {
    const mesureTempsFort: MesureTempsFort = new MesureTempsFort(0, this.selectedStyleMusic.id, 2, [1], true);
    this.selectedStyleMusic.mesureTempsForts.push(mesureTempsFort);
  }

  addStyleCoefDegres() {
    const styleCoefDegres: StyleCoefDegres = new StyleCoefDegres(0,1,1,1,3,3,1,1,0,0,0,1,2,0,2,0,1,0,3,1,3,0,3,1,0,0,3,1,1,4,0,0,1,0,1,0,0,1,0,1,1,0,0,1,0,1,0,0,0,0,this.selectedStyleMusic.id,'');
    this.selectedStyleMusic.styleCoefDegres.push(styleCoefDegres);
  }

  setSelectedStyle(styleMusic: StyleMusicFull) {

    if (styleMusic && styleMusic != null) {
      this.selectedStyleMusic = styleMusic;
      this.setFormValue(this.selectedStyleMusic);
    } else {
      this.selectedStyleMusic = new StyleMusicFull(0, 'new', [8, 6], 200, 50, 1, 8, 0, 1, 1, 3, 3, 8, 'Timbres Of Heaven GM_GS_XG_SFX V 3.4 Final.sf2', false);
      this.setFormValue(this.selectedStyleMusic);
    }

  }

  delete(name: string) {
    this.styleService.deleteByName(name)
      .subscribe(
        data => {
          this.getStyles();
        },
        error => {
          const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
          this.alertService.error(message);
        });
  }

  createForm() {

    this.styleForm = this.fBuilder.group({
      styleName: this.fBuilder.control(this.selectedStyleMusic.styleName, [Validators.required]),
      publish: this.fBuilder.control(this.selectedStyleMusic.publish, [Validators.required]),
      availableTon: this.fBuilder.control(this.selectedStyleMusic.availableTon, [Validators.required]),
      rythmeMax: this.fBuilder.control(this.selectedStyleMusic.rythmeMax, [Validators.required]),
      rythmeMin: this.fBuilder.control(this.selectedStyleMusic.rythmeMin, [Validators.required]),
      nInstrumentMin: this.fBuilder.control(this.selectedStyleMusic.nInstrumentMin, [Validators.required]),
      nInstrumentMax: this.fBuilder.control(this.selectedStyleMusic.nInstrumentMax, [Validators.required]),
      nPercussionMin: this.fBuilder.control(this.selectedStyleMusic.nPercussionMin, [Validators.required]),
      nPercussionMax: this.fBuilder.control(this.selectedStyleMusic.nPercussionMax, [Validators.required]),
      complexityChordMin: this.fBuilder.control(this.selectedStyleMusic.complexityChordMin, [Validators.required]),
      complexityChordMax: this.fBuilder.control(this.selectedStyleMusic.complexityChordMax, [Validators.required]),
      numberChordMin: this.fBuilder.control(this.selectedStyleMusic.numberChordMin, [Validators.required]),
      numberChordMax: this.fBuilder.control(this.selectedStyleMusic.numberChordMax, [Validators.required]),
      soundFont: this.fBuilder.control(this.selectedStyleMusic.soundFont, [Validators.required])
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptionStyles.unsubscribe();
  }

  setFormValue(styleMusic: StyleMusicFull) {

    this.styleForm.setValue({
      styleName: styleMusic.styleName,
      availableTon: styleMusic.availableTon,
      rythmeMax: styleMusic.rythmeMax,
      rythmeMin: styleMusic.rythmeMin,
      nInstrumentMin: styleMusic.nInstrumentMin,
      nInstrumentMax: styleMusic.nInstrumentMax,
      nPercussionMin: styleMusic.nPercussionMin,
      nPercussionMax: styleMusic.nPercussionMax,
      complexityChordMin: styleMusic.complexityChordMin,
      complexityChordMax: styleMusic.complexityChordMax,
      numberChordMin: styleMusic.numberChordMin,
      numberChordMax: styleMusic.numberChordMax,
      soundFont: styleMusic.soundFont,
      publish: styleMusic.publish
    });
  }

  setModelValue() {

    const model: StyleMusicFull = new StyleMusicFull(
      this.selectedStyleMusic.id,
      this.styleForm.value.styleName,
      this.styleForm.value.availableTon,
      this.styleForm.value.rythmeMax,
      this.styleForm.value.rythmeMin,
      this.styleForm.value.nInstrumentMin,
      this.styleForm.value.nInstrumentMax,
      this.styleForm.value.nPercussionMin,
      this.styleForm.value.nPercussionMax,
      this.styleForm.value.complexityChordMin,
      this.styleForm.value.complexityChordMax,
      this.styleForm.value.numberChordMin,
      this.styleForm.value.numberChordMax,
      this.styleForm.value.soundFont,
      this.styleForm.value.publish);

    return model;
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;

    if (!this.styleForm.valid) {
      // this.signupSlider.slideTo(0);
    } else {
      const model: StyleMusicFull = this.setModelValue();
      console.log(model);
      this.styleService.addNew(model)
        .subscribe(
          data => {
            this.getStyles();
          },
          error => {
            const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
            this.alertService.error(message);
            this.loading = false;
          });
    }
  }

  getStyles(): void {
    this.loading = true;
    this.styleService.getStylesMusic().subscribe(
      data => {
        this.styleService.setAllStyles(data);
        this.loading = false;
      }
    );
  }

  getSoundFont(): void {
    this.loading = true;
    this.styleService.getSoundFont().subscribe(
      data => {
        this.listSoundFont = data;
      }
    );
  }

  selectFile(event) {
    const file = event.target.files.item(0);

    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
      this.upload();
    } else {
      alert('invalid format!');
    }
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.styleService.pushFileToStorage(this.currentFileUpload, this.selectedStyleMusic.id).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!');
      }
      this.random = Math.random();
    });

    this.selectedFiles = undefined;
  }

}
