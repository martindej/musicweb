import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ComposeComponent }      from './compose/compose.component';
import { MyMusicComponent }      from './my-music/my-music.component';
import { MyMusicListComponent }      from './my-music-list/my-music-list.component';
import { SharedMusicsListComponent }      from './shared-musics-list/shared-musics-list.component';
import { MainPageComponent }      from './main-page/main-page.component';
import { MainSettingsComponent }      from './main-settings/main-settings.component';
import { InstrumentsSettingsComponent }      from './instruments-settings/instruments-settings.component';
import { MusicDetailsComponent }      from './music-details/music-details.component';
import { PlayerComponent }      from './player/player.component';
import { AuthguardService }   from './authguard.service';
import { LoginComponent }   from'./login/login.component';
import { RegisterComponent }  from'./register/register.component';
import { ConfirmationComponent }  from'./confirmation/confirmation.component';
import { AccountComponent }  from'./account/account.component';
import { MainScoreComponent } from './main-score/main-score.component';
import { ModifiedScoreComponent } from './modified-score/modified-score.component';
import { MusicParametersComponent } from './music-parameters/music-parameters.component';
import { MusicDetailsInfoComponent } from './music-details-info/music-details-info.component';
import { PublicAccountComponent } from './public-account/public-account.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { AdministrationComponent } from './administration/administration.component';
import { RecoverComponent }  from'./recover/recover.component';
import { PrivacyComponent }  from'./privacy/privacy.component';
import { TermsComponent }  from'./terms/terms.component';
import { AdminGuardService } from './admin-guard.service';


const routes: Routes = [
  { path: '', redirectTo: 'mainPage', pathMatch: 'full' },
  { path: 'administration', component: AdministrationComponent, canActivate: [AuthguardService, AdminGuardService] },
  { path: 'mainPage', component: MainPageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'recover', component: RecoverComponent },
  { path: 'account', component: AccountComponent, canActivate: [AuthguardService] },
  { path: 'publicAccount/:id', component: PublicAccountComponent, canActivate: [AuthguardService] },
  { path: 'register', component: RegisterComponent },
  { path: 'confirmation/:username/:identifier', component: ConfirmationComponent },
  { path: 'subscribe', component: SubscribeComponent, canActivate: [AuthguardService] },
  { path: 'privacyPolicy', component: PrivacyComponent},
  { path: 'termsAndConditions', component: TermsComponent},
  { path: 'musicDetails/:id',
    component: MusicDetailsComponent,
    canActivate: [AuthguardService],
    children: [
      {
        path: 'infoScore',
        component: MusicDetailsInfoComponent
      },
      {
        path: 'mainScore',
        component: MainScoreComponent
      },
      {
        path: 'modifiedScore',
        component: ModifiedScoreComponent
      }
    ]
  },
  { path: 'myMusic',
    component: MyMusicComponent,
    canActivate: [AuthguardService],
    children: [
      {
        path: 'myMusicsList',
        component: MyMusicListComponent
      },
      {
        path: 'SharedMusicsList',
        component: SharedMusicsListComponent },
      {
        path: 'player',
        component: PlayerComponent,
        outlet: 'aux'
      },
      {
        path: 'compose',
        component: ComposeComponent,
        outlet: 'aux',
        children: [
          {
            path: 'mainSettings',
            component: MainSettingsComponent,
            outlet: 'main'
          },
          {
            path: 'InstrumentsSettings',
            component: InstrumentsSettingsComponent,
            outlet: 'advanced'
          },
        ]
      },

    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
