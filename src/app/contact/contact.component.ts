import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../contact.service';
import { AlertService} from '../alert.service';
import { Autosize } from 'ng-autosize/src/autosize.directive';

import { Message } from '../message'
import { MessageAlert } from '../messageAlert';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  submitAttempt:boolean = false;
  loading:boolean = false;

  constructor(
    private fBuilder:FormBuilder,
    private contactService: ContactService,
    private alertService: AlertService) {
    this.contactForm = fBuilder.group({
      name: fBuilder.control(null, [Validators.required]),
      email: fBuilder.control(null, [Validators.required, Validators.email]),
      message: fBuilder.control(null, Validators.required),
    });
   }

  ngOnInit() {
  }

  send() {
    this.submitAttempt = true;
    this.loading = true;

    let mail:Message = new Message(this.contactForm.value.name, this.contactForm.value.email, this.contactForm.value.message);
 
    if(!this.contactForm.valid){
        // this.signupSlider.slideTo(0);
    }
    else {
        // User user = new User(0,model.username, model.password, )
        this.contactService.send(mail)
          .subscribe(
            data => {
              let message:MessageAlert = new MessageAlert("Message sent successfully", 5000, true, 'success');
              this.alertService.success(message, true);
              this.loading = false;
            },
            error => {
              let message:MessageAlert = new MessageAlert("A problem occured sending the message, please try again later", 5000, true, 'danger');
              this.alertService.success(message, true);
              this.loading = false;
            });
    }
  }

}
