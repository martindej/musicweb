import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { StyleService } from '../style.service';
import { MIDIINSTRUMENTS } from '../list-midiInstrument';
import { PERCUSSIONMIDIINSTRUMENTS } from '../list-midiPercussionInstruments';
import { AlertService} from '../alert.service';
import { MessageAlert } from '../messageAlert';
import { StyleGroupInstrumentsFull } from '../styleGroupInstruments';

@Component({
  selector: 'app-style-group-instruments',
  templateUrl: './style-group-instruments.component.html',
  styleUrls: ['./style-group-instruments.component.css']
})
export class StyleGroupInstrumentsComponent implements OnInit {

  @Input() styleGroupIns: StyleGroupInstrumentsFull;
  @Input() listStyleGroupIns: StyleGroupInstrumentsFull[];

  listInstruments = MIDIINSTRUMENTS;
  listPercussionInstruments = PERCUSSIONMIDIINSTRUMENTS;

  submitAttempt: boolean;
  loading: boolean;

  random: number;
  loaded: boolean;

  styleGroupInstrumentsForm: FormGroup;

  constructor(
    private fBuilder: FormBuilder,
    private styleService: StyleService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {

    this.styleGroupInstrumentsForm = this.fBuilder.group({
      name: this.fBuilder.control(this.styleGroupIns.name, [Validators.required]),
      motherId: this.fBuilder.control(this.styleGroupIns.motherId, [Validators.required]),
      instruments: this.fBuilder.control(this.styleGroupIns.instruments, [Validators.required]),

      pourcentagePlayingMin: this.fBuilder.control(this.styleGroupIns.pourcentagePlayingMin, [Validators.required]),
      pourcentagePlayingMax: this.fBuilder.control(this.styleGroupIns.pourcentagePlayingMax, [Validators.required]),
      balanceBassMelodyMin: this.fBuilder.control(this.styleGroupIns.balanceBassMelodyMin, [Validators.required]),
      balanceBassMelodyMax: this.fBuilder.control(this.styleGroupIns.balanceBassMelodyMax, [Validators.required]),
      balanceMelodyMelodyBisMin: this.fBuilder.control(this.styleGroupIns.balanceMelodyMelodyBisMin, [Validators.required]),
      balanceMelodyMelodyBisMax: this.fBuilder.control(this.styleGroupIns.balanceMelodyMelodyBisMax, [Validators.required]),
      gBetweenNotesMin: this.fBuilder.control(this.styleGroupIns.gBetweenNotesMin, [Validators.required]),
      gBetweenNotesMax: this.fBuilder.control(this.styleGroupIns.gBetweenNotesMax, [Validators.required]),
      dispersionNotesMin: this.fBuilder.control(this.styleGroupIns.dispersionNotesMin, [Validators.required]),
      dispersionNotesMax: this.fBuilder.control(this.styleGroupIns.dispersionNotesMax, [Validators.required]),
      pourcentageSilMin: this.fBuilder.control(this.styleGroupIns.pourcentageSilMin, [Validators.required]),
      pourcentageSilMax: this.fBuilder.control(this.styleGroupIns.pourcentageSilMax, [Validators.required]),
      speedMax: this.fBuilder.control(this.styleGroupIns.speedMax, [Validators.required]),
      speedMin: this.fBuilder.control(this.styleGroupIns.speedMin, [Validators.required]),
      pSyncopesMin: this.fBuilder.control(this.styleGroupIns.pSyncopesMin, [Validators.required]),
      pSyncopesMax: this.fBuilder.control(this.styleGroupIns.pSyncopesMax, [Validators.required]),
      pSyncopettesMin: this.fBuilder.control(this.styleGroupIns.pSyncopettesMin, [Validators.required]),
      pSyncopettesMax: this.fBuilder.control(this.styleGroupIns.pSyncopettesMax, [Validators.required]),
      pSyncopesTempsFortsMin: this.fBuilder.control(this.styleGroupIns.pSyncopesTempsFortsMin, [Validators.required]),
      pSyncopesTempsFortsMax: this.fBuilder.control(this.styleGroupIns.pSyncopesTempsFortsMax, [Validators.required]),
      pContreTempsMin: this.fBuilder.control(this.styleGroupIns.pContreTempsMin, [Validators.required]),
      pContreTempsMax: this.fBuilder.control(this.styleGroupIns.pContreTempsMax, [Validators.required]),
      pDissonanceMin: this.fBuilder.control(this.styleGroupIns.pDissonanceMin, [Validators.required]),
      pDissonanceMax: this.fBuilder.control(this.styleGroupIns.pDissonanceMax, [Validators.required]),
      pIrregularitesMin: this.fBuilder.control(this.styleGroupIns.pIrregularitesMin, [Validators.required]),
      pIrregularitesMax: this.fBuilder.control(this.styleGroupIns.pIrregularitesMax, [Validators.required]),
      bBinaireTernaireMin: this.fBuilder.control(this.styleGroupIns.bBinaireTernaireMin, [Validators.required]),
      bBinaireTernaireMax: this.fBuilder.control(this.styleGroupIns.bBinaireTernaireMax, [Validators.required]),
      pChanceSupSameRythmMin: this.fBuilder.control(this.styleGroupIns.pChanceSupSameRythmMin, [Validators.required]),
      pChanceSupSameRythmMax: this.fBuilder.control(this.styleGroupIns.pChanceSupSameRythmMax, [Validators.required]),
      numberNotesSimultaneouslyMin: this.fBuilder.control(this.styleGroupIns.numberNotesSimultaneouslyMin, [Validators.required]),
      numberNotesSimultaneouslyMax: this.fBuilder.control(this.styleGroupIns.numberNotesSimultaneouslyMax, [Validators.required]),
      probabilityRythmMin: this.fBuilder.control(this.styleGroupIns.probabilityRythmMin, [Validators.required]),
      probabilityRythmMax: this.fBuilder.control(this.styleGroupIns.probabilityRythmMax, [Validators.required]),
      numberRythmMin: this.fBuilder.control(this.styleGroupIns.numberRythmMin, [Validators.required]),
      numberRythmMax: this.fBuilder.control(this.styleGroupIns.numberRythmMax, [Validators.required]),
      weightRythmMin: this.fBuilder.control(this.styleGroupIns.weightRythmMin, [Validators.required]),
      weightRythmMax: this.fBuilder.control(this.styleGroupIns.weightRythmMax, [Validators.required]),
      pChanceSameNoteInMotifMin: this.fBuilder.control(this.styleGroupIns.pChanceSameNoteInMotifMin, [Validators.required]),
      pChanceSameNoteInMotifMax: this.fBuilder.control(this.styleGroupIns.pChanceSameNoteInMotifMax, [Validators.required]),
      pChanceStartByTonaleAsBassMin: this.fBuilder.control(this.styleGroupIns.pChanceStartByTonaleAsBassMin, [Validators.required]),
      pChanceStartByTonaleAsBassMax: this.fBuilder.control(this.styleGroupIns.pChanceStartByTonaleAsBassMax, [Validators.required]),
      pChanceStayAroundNoteRefMin: this.fBuilder.control(this.styleGroupIns.pChanceStayAroundNoteRefMin, [Validators.required]),
      pChanceStayAroundNoteRefMax: this.fBuilder.control(this.styleGroupIns.pChanceStayAroundNoteRefMax, [Validators.required]),
      pChancePlayChordMin: this.fBuilder.control(this.styleGroupIns.pChancePlayChordMin, [Validators.required]),
      pChancePlayChordMax: this.fBuilder.control(this.styleGroupIns.pChancePlayChordMax, [Validators.required])
    });
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;

    if (!this.styleGroupInstrumentsForm.valid) {
        // this.signupSlider.slideTo(0);
    } else {
        const model: StyleGroupInstrumentsFull = this.styleGroupInstrumentsForm.value;
        model.id = this.styleGroupIns.id;
        model.styleId = this.styleGroupIns.styleId;

        this.styleService.addNewGroupInstruments(model)
          .subscribe(
            data => {
              this.getStyles();
            },
            error => {
              const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
              this.alertService.error(message);
              this.loading = false;
            });
    }
  }

  delete() {
    this.styleService.deleteGroupInstruments(this.styleGroupIns.id, this.styleGroupIns.styleId)
      .subscribe(
        data => {
          this.getStyles();
        },
        error => {
          const message: MessageAlert = new MessageAlert('a problem occured, please try again later', 5000, true, 'danger');
          this.alertService.error(message);
        });
  }

  getStyles(): void {
    this.loading = true;
    this.styleService.getStylesMusic().subscribe(
      data => {
        this.styleService.setAllStyles(data);
        this.loading = false;
      }
    );
  }

}
