import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleGroupInstrumentsComponent } from './style-group-instruments.component';

describe('StyleGroupInstrumentsComponent', () => {
  let component: StyleGroupInstrumentsComponent;
  let fixture: ComponentFixture<StyleGroupInstrumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleGroupInstrumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleGroupInstrumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
