import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
 
import { AlertService} from '../alert.service';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user';
import { UsernameValidator } from  '../validators/username';
import { EmailValidator } from  '../validators/email';
import { PasswordValidator } from '../validators/password';
import { MessageAlert } from '../messageAlert';
 
@Component({
  moduleId: module.id,
  styleUrls: ['./register.component.css'],
  templateUrl: 'register.component.html'
})
 
export class RegisterComponent implements OnInit  {

  @ViewChild('username') usernameElement: ElementRef;
  @ViewChild('email') emailElement: ElementRef;

  loading:boolean = false;
  submitAttempt:boolean = false;

  userForm: FormGroup;
  password = 'password';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private fBuilder:FormBuilder,
    public usernameValidator: UsernameValidator,
    public emailValidator: EmailValidator) {

      this.userForm = fBuilder.group({
        username: fBuilder.control(null, [Validators.required, Validators.pattern('[a-zA-Z]*')], usernameValidator.checkUsername.bind(usernameValidator)),
        email: fBuilder.control(null, [Validators.required, Validators.email], emailValidator.checkEmail.bind(emailValidator)),
        password: fBuilder.control(null, Validators.required),
        repeatPassword: fBuilder.control(null, [Validators.required, PasswordValidator(this.password)]),
        validationTerms:  fBuilder.control(null, Validators.required) 
      });
  }

  ngOnInit() {
    window.scroll(0, 0);
  }

  ngAfterViewInit() {
    // Observable.fromEvent(this.usernameElement.nativeElement, 'input')
    //   .map((event: Event) => (<HTMLInputElement>event.target).value)
    //   .debounceTime(1000)
    //   .distinctUntilChanged()
    //   .subscribe(data => this.notTypingUsername = true);
  }

  save() {
    this.submitAttempt = true;
    this.loading = true;
 
    if(!this.userForm.valid || !this.userForm.value.validationTerms){
        // this.signupSlider.slideTo(0);
    }
    else {
        let model: User = new User(0,this.userForm.value.username,this.userForm.value.password,this.userForm.value.email, false, null, false, false,"",0,false, true);
        this.authenticationService.create(model)
          .subscribe(
            data => {
              // set success message and pass true paramater to persist the message after redirecting to the login page
              let message:MessageAlert = new MessageAlert("User created successfully", 5000, true, 'success');
              this.alertService.success(message, true);
              this.router.navigate(['/login']);
              this.loading = false;
            },
            error => {
              let message:MessageAlert = new MessageAlert("A problem occured creating the user", 5000, true, 'danger');
              this.alertService.success(message, true);
              this.loading = false;
            });
    }
  }
}