
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders, HttpParams} from '@angular/common/http';
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import { Observable, Subscription, Subject } from 'rxjs';


declare var gapi : any;

@Injectable()
export class YoutubeVideoService {

	// uploadVideo:UploadVideo;
	private apiUrl = "/api/youtube";
	private authResult;

	APIKEY = "AIzaSyDvtuOPSrAzN6acIZqqv8_2aN-KuRk7Y28";
	OAUTH2_CLIENT_ID = '781542012544-b2jjp4tq8hcds6ds7mbnpq1m6qrflunp.apps.googleusercontent.com';
	OAUTH2_SCOPES = [
	  'https://www.googleapis.com/auth/youtube'
	];

	authenticated:boolean = false;
	accessToken:string;

	constructor(
		private httpClient: HttpClient,
		private http: Http) { 
	}

	initClient() {

		let options = {
			apiKey:this.APIKEY,
			client_id: this.OAUTH2_CLIENT_ID,
			scope:this.authResult.scope
		};

		gapi.client.init(options)
		.then(_ => {
			this.handleAuthResult();
		})
		.catch(error => {
			console.log('authenticationError', {error, note: 'Error on GAPI init.'})
		})
	}

	getToken(): Observable<any> {
	    return this.httpClient
	    		.get(this.apiUrl + '/getToken', {}).pipe(
	    		map( data => {
	    			gapi.load('client', () => {this.initClient()});
	    			this.authResult = data;
			        // this.handleAuthResult(data);
			        return data;
			    },
			      err => {
			        return err;
			    }));
	}


	// Handle the result of a gapi.auth.authorize() call.
	handleAuthResult() {
		console.log(this.authResult);
		gapi.client.setToken({access_token:this.authResult.access_token})
	  	if (this.authResult && !this.authResult.error) {
		    // Authorization was successful. Hide authorization prompts and show
		    // content that should be visible after authorization succeeds.
		    this.loadAPIClientInterfaces();
	  	}
	}

	// Load the client interfaces for the YouTube Analytics and Data APIs, which
	// are required to use the Google APIs JS client. More info is available at
	// https://developers.google.com/api-client-library/javascript/dev/dev_jscript#loading-the-client-library-and-the-api
	loadAPIClientInterfaces() {
	  gapi.client.load('youtube', 'v3', () => {
	    this.ready()
	  });
	}

	ready () {
	  	this.accessToken = this.authResult.access_token;
	  	this.authenticated = true;

	  	gapi.client.request({
		    path: '/youtube/v3/channels',
		    params: {
		      part: 'snippet',
		      mine: true
		    }
		})
	  	.then(
	  		data => {
	      	if (data.error) {
	        	console.log(data.error.message);
	      	} else {
		       // console.log("ca marche");
	      	}
	    })
	};

	updateFile (id:string, title:string, description:string, categoryId:string, tags:string, privacyStatus:string ) : Observable<any> {

		var headers = new HttpHeaders();
	    headers = headers.append('Authorization', 'Bearer ' + this.accessToken);
	    headers = headers.append('Content-type', 'application/json');

	    let metadata = {
			id : id,
	    	snippet: {
	      		title: title,
	      		description: description,
	      		tags: tags,
	      		categoryId: categoryId
	    	},
	    	status: {
	      		privacyStatus: privacyStatus
	    	}
	  	};


		let params = '?part=snippet,status,id';

		const url = 'https://www.googleapis.com/youtube/v3/videos'+params; 
	    return this.httpClient.put(url, metadata, {headers:headers});
	}


	uploadFile (file): Observable<any> {

	    var headers = new HttpHeaders();
	    headers = headers.append('Authorization', 'Bearer ' + this.accessToken);

	   	let params = '?part=snippet,status,id';

	    const url = 'https://www.googleapis.com/upload/youtube/v3/videos'+params; 
	    return this.httpClient.post(url, file, {headers:headers, reportProgress:true});
  	}

  	getChannels()  {

	    gapi.client.request({
		    path: '/youtube/v3/channels',
		    params: {
		      part: 'snippet,contentDetails,statistics',
		      mine: true
		    }
		})
		.then( 
			data => {
				//console.log(data);
			})
  	}

  	getPlaylist()  {

	    gapi.client.request({
		    path: '/youtube/v3/playlists',
		    params: {
		      part: 'snippet,contentDetails',
		      // channelId: 'UC9KUH7R43De_6ZfP4UkvWHg'
		      mine:true
		    }
		})
		.then( 
			data => {
				console.log(data);
			})
  	}

}
