import { Pipe, PipeTransform } from '@angular/core';
import { MusicFile } from './MusicFile';

@Pipe({
  name: 'musicListFilter',
  pure: false 
})
export class MusicListFilterPipe implements PipeTransform {

  transform(items: MusicFile[], filter: {style:string[],instruments:string[]}): any {
    if (!items || !filter) {  
      return items;  
    }  
    return items.filter((item: MusicFile) => this.applyFilter(item, filter));
  }

  applyFilter(item: MusicFile, filter: {style:string[],instruments:string[]}): boolean {

    if(( filter.style.length == 0 || filter.style.indexOf(item.style.toLowerCase()) !== -1) && filter.instruments.length == 0 ) {
      return true;
    }
    else if( filter.style.length == 0 || filter.style.indexOf(item.style.toLowerCase()) !== -1) {
      for(let ins in filter.instruments) {
        if( item.instruments.toLowerCase().indexOf(filter.instruments[ins]) !== -1) {
          return true;
        }
      }
      return false;
    }
    else {
      return false;
    }
  }

}