import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, ChangeDetectorRef } from '@angular/core';
import { Observable, Subscription, Subject } from 'rxjs';
import WaveSurfer from 'wavesurfer.js';
import Microphone from 'wavesurfer.js/dist/plugin/wavesurfer.microphone.min.js';

import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit, OnDestroy {
  
  @ViewChild('audioPlayer') audioPlayer: ElementRef;
  @ViewChild('waveform') waveform: ElementRef;

  @ViewChild('seek') seek: ElementRef;

  private subscriptionMusic;
  bars;
  duration:number;
  playPercent:number = 0;
  playVisible:boolean = true;
  musicFile:MusicFile = new MusicFile(0, "", 0, "", false,0,"",false,0,"",false,0,"", "https://picsum.photos/640/640?random", "","","","", 0, "", "", "", 0,0);
  audio;
  wavesurfer:WaveSurfer;
  loading:boolean = true;
  isMute:boolean;
  innerWidth: any;
  // playNow:boolean;

  // repoUrl = '';

  constructor(private mainService: MainService, private musicService: MusicService, private changeDetectorRef:ChangeDetectorRef) {}

  ngOnDestroy() {
    this.wavesurfer.destroy();
    this.subscriptionMusic.unsubscribe();
  }

  ngOnInit() {

    this.isMute = false;
    this.innerWidth = window.innerWidth;
    this.generateLoadingBar();
    // this.initValues();
    this.initAudio();

    this.subscriptionMusic = this.mainService.currentMusicFile.subscribe(
      data => {
        // window.scroll(0, 0);
        // if( && (data.musicFile.videoId === null || data.musicFile.videoId === "")) {
        //   this.playAudio();
        // }
        if(data.id !== this.musicFile.id) {
          this.loading = true;
          this.musicFile = data;
          window.scroll(0, 0);
          this.loadAudio();
        }

        // this.repoUrl = 'https://murmure.com/musicDetails/'+ this.musicFile.id +'/infoScore';
    });

    // this.musicService.checkLike(this.musicFile.id).subscribe(
    //   res => {
    //     this.like = res;
    //   }
    // );
  }

  @HostListener('window:resize', ['$event'])
      onresize(event) {
        this.innerWidth = window.innerWidth;
  }

  // checkWaveFormEmpty() {
  //   if(this.waveform.nativeElement.children[0].childNodes.length > 2) {
  //     return false;
  //   }
  //   else {
  //     return true;
  //   }
  // }

  generateLoadingBar() {
    let bar;
    this.bars = [];

    for(let i=0;i<this.innerWidth/4;i++) {
      bar = {
        left : (i*4 + 1) + 'px',
        duration : Math.floor(Math.random() * 500) + 500 + 'ms'
      }
      this.bars.push(bar);
    }

  }

  initAudio() {
    this.wavesurfer = WaveSurfer.create({
      // Use the id or class-name of the element you created, as a selector
      container: '#waveform',
      // The color can be either a simple CSS color or a Canvas gradient
      waveColor: '#fff',
      progressColor: 'hsla(200, 100%, 30%, 0.5)',
      cursorColor: '#fff',
      cursorWidth:0,
      fillParent:true,
      // This parameter makes the waveform look like SoundCloud's player
      barWidth: 3
    });

    this.wavesurfer.on('loading', percents => {
      this.setPercent(percents);
    });

    this.wavesurfer.on('finish', () => {
      this.wavesurfer.stop();
      this.playVisible = true;
      this.changeDetectorRef.detectChanges();
    });

    this.wavesurfer.on('pause', () => {
      this.playVisible = true;
    });

    this.wavesurfer.on('play', () => {
      this.playVisible = false;
    });

    this.wavesurfer.on('ready', () => {
      this.loading = false;
      this.changeDetectorRef.detectChanges();
    });
  }

  setPercent(percents:number) {
    this.playPercent = percents;
  }

  loadAudio():void {
    if(this.musicFile.id > 0) {

      let audio = new Audio();

      this.musicService.loadMusic(this.musicFile.id).subscribe(
        res => {
          this.wavesurfer.loadBlob(res);
        }
      );

    }
  }

  muteAudio():void {
    this.isMute = !this.isMute;
    this.wavesurfer.toggleMute();
  }

  playAudio():void {
    this.wavesurfer.playPause();
  }

  // likeMusic():void {
  //   this.musicService.likeMusic(this.musicFile.id, this.musicFile.user_id).subscribe(
  //     res => {
  //       if(res == true) {
  //         this.like = true;
  //       }
  //     }
  //   );
  // }

  // dislikeMusic():void {
  //   this.musicService.dislikeMusic(this.musicFile.id).subscribe(
  //     res => {
  //       if(res == true) {
  //         this.like = false;
  //       }
  //     }
  //   );
  // }

  // toggleVisible():void {
  //   let visible = this.musicFile.visible_origin || this.musicFile.visible_modified || this.musicFile.visible_video;
  //   this.musicService.toggleVisible(this.musicFile.id, !visible).subscribe(
  //     res => {
  //       if(res == true) {
  //         this.musicFile.visible_origin = !visible;
  //         this.musicFile.visible_modified = !visible;
  //         this.musicFile.visible_video = !visible;
  //       }
  //     }
  //   );
  // }

}
