import { Rhythm } from './rhythm';

export const RHYTHMS: Rhythm[] = [
  { id: 1, name: 'ronde'},
  { id: 3, name: 'blanche'},
  { id: 5, name: 'noire'},
  { id: 7, name: 'noire triolet'},
  { id: 8, name: 'croche'},
  { id: 9, name: 'croche triolet'},
  { id: 10, name: 'double croche'},
  { id: 11, name: 'double croche triolet'}
];
