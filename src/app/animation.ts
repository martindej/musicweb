import { trigger, state, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

export const slideIn = trigger('slideIn', [

    // state('hide', style({
    //     opacity: 0,
    // })),
    // state('show',   style({
    //     opacity: 1
    // })),
    // //   transition('transparent => opaque', animate('700ms ease-out')),
    // //   transition('opaque => transparent', animate('700ms ease-in')),

    transition('* => *', [
      // this hides everything right away
      query(':enter', style({ opacity: 0, bottom: '50px'}), { optional: true }),

      // starts to animate things with a stagger in between
      query(':enter', stagger('500ms', [
        animate('1s', style({ opacity: 1, bottom: '0px'}))
      ]), { optional: true })
    ])
]);

export const slideInRight = trigger('slideInRight', [

  transition('* => *', [
    // this hides everything right away
    query(':enter', style({ opacity: 0, left: '300px'}), { optional: true }),

    // starts to animate things with a stagger in between
    query(':enter', stagger('500ms', [
      animate('1s', style({ opacity: 1, left: '0px'}))
    ]), { optional: true })
  ])
]);

export const slideInLeft = trigger('slideInLeft', [

  transition('* => *', [
    // this hides everything right away
    query(':enter', style({ opacity: 0, right: '300px'}), { optional: true }),

    // starts to animate things with a stagger in between
    query(':enter', stagger('500ms', [
      animate('1s', style({ opacity: 1, right: '0px'}))
    ]), { optional: true })
  ])
]);
