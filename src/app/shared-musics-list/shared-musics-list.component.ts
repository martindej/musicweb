import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { MainService } from '../main.service';
import { MusicService } from '../music.service';
import { MusicFile } from '../MusicFile';

@Component({
  selector: 'app-shared-musics-list',
  templateUrl: './shared-musics-list.component.html',
  styleUrls: ['./shared-musics-list.component.css']
})
export class SharedMusicsListComponent implements OnInit, OnDestroy {

  musicFiles:MusicFile[] = [];
  loading:boolean;
  page:number;
  moreVisible:boolean;


  filter:{style:string[],instruments:string[]} = {"style":[],"instruments":[]};
  private subscriptionFilter:Subscription;

  constructor(private mainService: MainService, private musicService: MusicService) { 

    this.filter = musicService.filter;
    this.subscriptionFilter = musicService.filterChange.subscribe((value) => { 
      this.page = 0;
      this.moreVisible = true;
      this.filter = value; 
    });
  }

  ngOnInit() {
    this.page = 0;
    this.moreVisible = true;
    this.getPublicMusics();
  }

  ngOnDestroy() {
    this.subscriptionFilter.unsubscribe();
  }

  addMore(): void {
    this.loading = true;
    this.musicService.getPublicMusics(this.page).subscribe(
      data => {
        if(data.length >= 20) {
          this.page++;
        }
        else {
          this.moreVisible = false;
        }
        this.addAllMusics(data);
        this.loading = false;
      }
    );
  }

  getPublicMusics(): void {
    this.loading = true;
    this.musicService.getPublicMusics(this.page)
      .subscribe(
        data => {
          if(data.length >= 20) {
            this.page++;
          }
          else {
            this.moreVisible = false;
          }
          this.setAllMusics(data);
          this.loading = false;
        }
      );
  }

  setAllMusics(data) {
    this.musicFiles = [];
    for(let m of data) {

      let music:MusicFile = new MusicFile(
        m.id, m.name, 
        m.user_id, 
        m.user_name, 
        m.visible_origin,
        m.user_id_origin,
        m.user_name_origin,
        m.visible_modified,
        m.user_id_modified,
        m.user_name_modified,
        m.visible_video,
        m.user_id_video,
        m.user_name_video, 
        "https://picsum.photos/640/640?image="+m.pictureNumber, 
        m.description,
        m.musicXmlFile,
        m.musicXmlFileModified,
        m.videoId, 
        m.tempo, 
        m.style, 
        m.instruments, 
        m.tonality, 
        m.pictureNumber,
        m.creationDate);
      this.musicFiles.push(music);
    }
  }
  
  addAllMusics(data) {
    for(let m of data) {
      let music:MusicFile = new MusicFile(
        m.id, m.name, 
        m.user_id, 
        m.user_name, 
        m.visible_origin,
        m.user_id_origin,
        m.user_name_origin,
        m.visible_modified,
        m.user_id_modified,
        m.user_name_modified,
        m.visible_video,
        m.user_id_video,
        m.user_name_video, 
        "https://picsum.photos/640/640?image="+m.pictureNumber, 
        m.description,
        m.musicXmlFile,
        m.musicXmlFileModified,
        m.videoId, 
        m.tempo, 
        m.style, 
        m.instruments, 
        m.tonality, 
        m.pictureNumber,
        m.creationDate);
      this.musicFiles.push(music);
    }
  }

}