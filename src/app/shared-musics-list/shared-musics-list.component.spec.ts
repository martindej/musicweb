import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedMusicsListComponent } from './shared-musics-list.component';

describe('SharedMusicsListComponent', () => {
  let component: SharedMusicsListComponent;
  let fixture: ComponentFixture<SharedMusicsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedMusicsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedMusicsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
