import { StyleMusic } from './styleMusic';

export const STYLEMUSIC: StyleMusic[] = [
  { id: 1, title: 'Random', icon:'assets/chef_string_pink.png', image:'assets/random.jpg', text:'Generate, listen and find your perfect melody', link:'composition' },
  { id: 2, title: 'Jazz', icon:'assets/radio_string_pink.png', image:'assets/jazz.jpg', text:'Choose your style and listen unique melodies', link:'radio' },
  { id: 3, title: 'Rock', icon:'assets/trompette_string_pink.png', image:'assets/rock.jpg', text:'Choose your instrument and generate a sheet to pratice', link:'study' },
  { id: 4, title: 'Piano classic', icon:'assets/awards_string_pink.png', image:'assets/piano.jpg', text:'One new song per week, show your skills in a video' , link:'ideo' },
  { id: 5, title: 'Blues', icon:'assets/chef_string_pink.png', image:'assets/blues.jpg', text:'Generate, listen and find your perfect melody', link:'composition' },
  { id: 6, title: 'Classique', icon:'assets/radio_string_pink.png', image:'assets/classical.jpg', text:'Choose your style and listen unique melodies', link:'radio' },
  { id: 7, title: 'Pop', icon:'assets/trompette_string_pink.png', image:'assets/pop.jpg', text:'Choose your instrument and generate a sheet to pratice', link:'study' },
  { id: 8, title: 'Reggae', icon:'assets/awards_string_pink.png', image:'assets/reggae.jpg', text:'One new song per week, show your skills in a video' , link:'ideo' }
];