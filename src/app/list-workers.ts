import { Worker } from './worker';

export const WORKERS: Worker[] = [
  { id: 1,
    name: 'Martin DEJAX',
    job: 'Founder',
    text: 'Enthusiast music instrument player, decent developer and terrible composer.',
    textResume: 'I had to do something about it',
    image: 'assets/card1.png',
    linkedInLink: 'https://www.linkedin.com/in/martindejax'
  },

  { id: 2,
    name: 'Bali',
    job: 'Pillow',
    text: 'Enthusiast bone eater, decent couch tester and terrible sheepdog.',
    textResume: 'I\'ll never change',
    image: 'assets/card2.png',
    linkedInLink: ''
  }
];
